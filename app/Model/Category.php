<?php

class Category extends AppModel {

    public $hasMany = array(
        'RetailerCategory',
        
        'SubCategory'=>array(
            'conditions'=>array('SubCategory.isDeleted'=>0)
        ),
    );

}
