<?php
class SubCategory extends AppModel
{
    //public $belongTo = 'Category';
    public $belongTo = array(
        'Category' => array(
            'className' => 'Category',
            'foreignKey' => 'id'
        )
    );
    
    
    public $validate = array(
        'name' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'The Name field is required'
            )            
        ),
        'category_id' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'The Category field is required'
            )            
        ),
    );
}

