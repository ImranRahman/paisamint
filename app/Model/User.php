<?php

class User extends AppModel {
	public $hasMany = array(
		'SocialProfile' => array(
			'className' => 'SocialProfile',
		)
	);

    public $validate = array(
        'name' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'The Name field is required'
            ),
            'name' => array(
                'rule' => array('minLength', '4'),
                'message' => 'The name must be 4 characters long.'
            )
        ),
        'username' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'The username field is required'
            )            
        ),
        'email' => array(
            'required' => array(
                'rule' => array('email'),
                'message' => 'The Email field is required and a valid email.'
            ),
            'email' => array(
                'rule' => 'isUnique',
                'message' => 'This email has already been taken.'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'The Password field is required'
            ),
            'password' => array(
                'rule' => array('minLength', '6'),
                'message' => 'The Password field must be minimum 6 characters.'
            )
        ),
        'confpassword' => array(
            'compare' => array(
                'rule' => array('validate_passwords'),
                'message' => 'The passwords you entered do not match.',
            )
        ),
        'mob' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'The Mobile field is required'
            ),
            'mob' => array(
                'rule' => '/^[0-9]{10}$/i',
                'message' => 'Mobile number is 10 digit numeric field'
            )
        ),
        'term' => array(
            //'notEmpty' => array(
                'rule' => array('comparison', '!=', 0),
                'required' => true,
                'message' => 'You must agree term and conditions.',
                'on' => 'create'
            //)
        ),
        'currpassword' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'The current password field is required.',
            )
        ),
    );

    //------ Password hashing --------------
    public function beforeSave($options = array()) {

        if (isset($this->data[$this->alias]['password'])) {

            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
        return true;
    }

//    public function beforeSave($options = array()) {
//        if (isset($this->data[$this->alias]['password'])) {
//            $passwordHasher = new BlowfishPasswordHasher();
//            $this->data[$this->alias]['password'] = $passwordHasher->hash(
//                    $this->data[$this->alias]['password']
//            );
//        }
//        return true;
//    }

    public function validate_passwords() {
        return $this->data[$this->alias]['password'] === $this->data[$this->alias]['confpassword'];
    }

/*
 * First time social login 
 */
 
    public function createFromSocialProfile($incomingProfile){
 
		// check to ensure that we are not using an email that already exists
		$existingUser = $this->find('first', array(
			'conditions' => array('email' => $incomingProfile['SocialProfile']['email'])));
		 
		if($existingUser){
			// this email address is already associated to a member
			return $existingUser;
		}
		//pr($incomingProfile);die;
		// brand new user
		$socialUser['User']['email'] = $incomingProfile['SocialProfile']['email'];
		$socialUser['User']['username'] = str_replace(' ', '_',$incomingProfile['SocialProfile']['display_name']);
		$socialUser['User']['name'] = $incomingProfile['SocialProfile']['first_name'].' '.$incomingProfile['SocialProfile']['last_name'];
		//$socialUser['User']['role'] = 'bishop'; // by default all social logins will have a role of bishop
		$socialUser['User']['password'] = date('Y-m-d h:i:s'); // although it technically means nothing, we still need a password for social. setting it to something random like the current time..
		$socialUser['User']['created'] = date('Y-m-d h:i:s');
		$socialUser['User']['modified'] = date('Y-m-d h:i:s');
		$socialUser['User']['term'] = 1;
		 
		// save and store our ID
		$this->save($socialUser);
		$socialUser['User']['id'] = $this->id;
		 
		return $socialUser;
		 
	 }
	 
}
