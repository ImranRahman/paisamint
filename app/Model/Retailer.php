<?php
class Retailer extends AppModel
{
    public $belongTo = array('RetailerCategory');
    public $hasMany = array('RetailerParam');
    
    public $validate = array(
        'retailer_type_id' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'The Retailer Type field is required'
            )            
        ),
        'category_id' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'The Category field is required'
            )            
        ),
        'name' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'The Name field is required'
            )            
        ),
        'logo_image' => array(
            'type' => array(
                'rule' => array('extension',  array('gif', 'jpeg', 'png', 'jpg','bmp')),
                'message' => 'Please supply a valid image.',
                'on' => 'create'
            ),
            'logo_image' => array(
                'rule'    => array('validateImageSize'),
                'message' => 'Minimum image dimension is 145x75.',
                'on' => 'create'
            ),
            
        ),
        'description' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'The Name field is required'
            )            
        ),        
        'phone' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'The Phone number field is required'
            ),
            'phone' => array(
                'rule' => '/^[0-9]{10}$/i',
                'message' => 'Phone number is 10 digit numeric field'
            )           
        ),
        'url' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'The Url field is required'
            ),
            'url' => array(
                'rule' => 'url',
                'message' => 'Please put a valid url'
            )
        ),
    );
    
    
    
    public function validateImageSize($field) {
       $tempFile = $field['logo_image']['tmp_name'];
       $sizes = getimagesize($tempFile); 
       if($sizes[0] < 145 || $sizes[1] < 75){
           return false;
       }else{
           return true;
       }
    }
    
}
 
