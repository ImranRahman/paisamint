<?php
class Coupon extends AppModel
{
    
    public $validate = array(      
        'retailer_id' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'The Retailer field is required'
            ) 
        ),
        'title' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'The Title field is required'
            ) 
        ),
        'description' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'The Description field is required'
            ) 
        ),
        'cashback' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'The Cashback field is required'
            ),
            'cashback' => array(
                'rule' => array('decimal'),
                'message' => 'Only decimal is allowed for Cashback'
            )
        ),
        'code' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'The Code field is required'
            ) 
        ),
        'activate_id' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'The activate Id field is required'
            ),
            'activate_id' => array(
                'rule' => 'numeric',
                'message' => 'Only number is allowed'
            )
        ),        
    );
}
