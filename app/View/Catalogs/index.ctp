<?php ?>
<style>
    .gecko-x-small {
    font-size: 1.6rem;
    font-weight: 700;
}
.f2 h2{font-size:28px;  padding:10px; color:red;}
.f2 p{padding:10px; margin-top:10px; line-height:1.5; font-size:18px;}
    </style>
<section class="Latest_Deals" style="margin-top: 30px;">
    <div class="container">
        <div class="row">
            <div class="latest search-container">
                <div class="box_heading">
                    <h3>Search results for: <?php echo isset($search)?$search:'';?><span><i class="fa fa-shopping-cart"></i></span></h3>
                </div>
                <div class="carousel slide"  id="myCarousel1" style="min-height:320px;">                    
                    <div class="carousel-inner">
                        <?php if(empty($coupons)){?>

                        <div class="col-md-8 col-md-offset-2 f2">
                                
                            <h2>Oh no! We can't find any search results for "<?php echo isset($search)?$search:'';?>"</h2>
                            <p>To continue your search, please double check that your search words are spelt correctly, or try broadening your search with more generic words. You can also try browsing our different categories above to find the retailer or product you're after.</p>
                           
                        </div> 

                        <?php }else{ ?>
                        <span class="count-results"><?php echo count($coupons);?> Results Found</span>
                        <div class="item active">
                            <?php 
                            
                            //echo count($coupons);
                            foreach($coupons as $coupon){
                                $couponID = $coupon['Coupon']['id'];
                            ?>
                            <div class="col-md-2 col-xs-3  offer_box"> 
                                <div class="icon-info">
                                    <i class="fa fa-info-circle new"></i>
                                </div>
                                <?php 
                                if($coupon['Retailer']['logo']!='' && file_exists(RETAILER_THUMB.$coupon['Retailer']['logo'])){
                                    echo $this->Html->image(RETAILER_THUMB_URL.$coupon['Retailer']['logo'], array("alt" => "Logo Here",
        'url' => array('controller' => 'retailers', 'action' => '/', $this->SeoUrl->seoUrl($coupon['Retailer']['name']))));
                                }else{
                                    echo $this->Html->image('noimage.jpg', array("alt" => "Logo Here",
        'url' => array('controller' => 'retailers', 'action' => '/', $this->SeoUrl->seoUrl($coupon['Retailer']['name']))));
                                }
                                ?>

                                <p><?php echo $coupon['Coupon']['title']; ?></p>
                                <?php                                 
                                    $now = time(); // or your date as well
                                    $your_date = strtotime($coupon['Coupon']['expire']);
                                    $datediff = $your_date - $now;
                                    $expiry = ceil($datediff/(60*60*24));
                                    $day = ($expiry > 1)?' days':' day'; 
                                    if($expiry == 0){
                                        $expiry = 'today'; $day='';
                                    }                                                                       
                                ?>
                                <span><i class="fa fa-clock-o"></i>Expires in <?php echo $expiry.$day;?></span>
                                <h5>+ Cashback upto <?php echo $coupon['Coupon']['cashback'].'%';?></h5>
                                <button type="button" class="get" onclick="couponPopup(<?php echo $couponID;?>);">Get this Deal</button>
                            </div>
                            <?php } ?>
                        </div>

                        <?php } ?>
                    </div>
                    <!--                    <div class="more_button">
                                            <button id="getMoreCoupon" type="button" class="btn btn-more" value="20">More Hot Deal</button>
                                            <div id="loader-coupon" style="display: none;"><i class="fa fa-spinner fa-pulse"></i></div>
                                        </div>-->
<?php // echo $this->element('sql_dump');?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo $this->element('coupon_popup'); ?>