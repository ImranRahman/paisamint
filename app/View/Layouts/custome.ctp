<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
    <head>
	<?php echo $this->Html->charset(); ?>
        <meta content="True" name="HandheldFriendly"> <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
		<?php //echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
        </title>
	<?php
            //echo $this->Html->meta('icon');

            //echo $this->Html->css('cake.generic');
            echo $this->Html->css(array('bootstrap.min', 'style', 'responsive','custome'));
            echo $this->Html->css('/font-awesome/css/font-awesome.min');
            echo $this->Html->css(array('tab/tabcss','tab/reset.css'));

        ?>

        <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>        
<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>

        <?php

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
                
                echo $this->Html->script(array('jquery.min','custome'));
                echo $this->Html->script('tab/modernizr');
	?>

    </head>
    <body>
        <input type="hidden" id="base-url" value="<?php echo BASE_URL;?>" />
        <input type="hidden" id="user_session" value="<?php echo $this->Session->read('user_data.id');?>" />
        <header>
            <?php echo $this->element('top_header'); ?>
            <?php echo $this->element('main_nav'); ?>
            <div class="clearfix"></div>
        </header>

        <?php //echo $this->Session->flash(); ?>

        <?php echo $this->fetch('content'); ?>


        <?php echo $this->element('footer'); ?>


        <p id="back-top"><a href="#top"><i class="icon-arrow-up"></i></a></p>

        <?php 
        echo $this->html->script(array('tab/jquery-2.1.1','tab/main'));
        echo $this->Html->script(array('bootstrap.min')); 
        ?>


	<?php //echo $this->element('sql_dump'); ?>
    </body>
</html>
