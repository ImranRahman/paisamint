<?php

App::uses('AppHelper', 'View/Helper');

class SeoUrlHelper extends AppHelper {

    public function seoUrl($url = null) {
        return strtolower(str_replace(' ', '-', $url));
    }

    public function friendlyUrl($url = null) {
        return strtolower(str_replace('-', ' ', $url));
    }
    
    public function cateUrl($url = null) {
        $url = str_replace(' ', '-', $url);
        return strtolower(str_replace('-', ' ', $url));
    }

    public function wordWrap($str, $len) {
        $line = $str;
        if(strlen($line) <= $len){
            return $line;
        }else{
            
            if (preg_match("/^.{1,$len}\b/s", $str, $match)) {
                $line = $match[0];
            }
            return $line.' ';
        }
        
    }

}
