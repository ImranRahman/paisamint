<?php
//pr($this->params);
?>
<style>
    #rech-form input{ text-align: l
;}
    .spiner{ width: 24px; height: 24px; display: none; vertical-align: bottom; margin-left: 10px;}
    .redeem-tab{ padding: 10px 0;}
    .redeem-tab a{ text-decoration: none;}
</style>

<section class="admin_edit">
    <div class="container">
        <div class="main_admin">
            
            <div class="admin-p">
                
                <?php echo $this->Session->flash(); ?>
                
                <div class="ad-bg-c">
                    <div class="row">
                        <div class="admin-profile">
                            <div class="col-md-2 col-sm-6 col-xs-6">
                                <div class="admin-img">
                                    <!--<img class="ad-img" src="images/lol.png">-->
                                    <?php echo $this->Html->image("/images/lol.png");?>
                                </div>
                            </div>
                            <div class="col-md-9 col-sm-6 col-xs-6">
                                <div class="ad-text">
                                    <h3><span class="ad-name"><?php echo ucwords($user['User']['name']);?></span></h3>
<!--                                    <div class="img-border">
                                        <p><i class="fa fa-map-marker"></i>From Delhi NCR</p>
                                    </div>
                                    <p><i class="glyphicon-time"></i><span class="ad-icon-watch">
                                            Joined  <?php                                       
//                                        echo CakeTime::timeAgoInWords($user['User']['created'], array(
//                                            'accuracy' => array('month' => 'month'),
//                                            'end' => '1 year'
//                                        ));
                                       
                                        ?>
                                        </span></p>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="cd-tabs">
                <nav>
                    <ul class="cd-tabs-navigation">
                        <li><a data-content="inbox" class="selected" href="#0"><i class="fa fa-plus"></i>Overview</a></li>
                        <li><a data-content="new" href="#0"><i class="fa fa-edit"></i>Cashback Summary</a></li>                          <li><a data-content="gallery" href="#0"><i class="fa fa-users"></i>Activity</a></li>
                        <li><a id="redeem_a" data-content="redeem" href="#0"><i class="fa fa-edit"></i>Redeem</a></li>
                        <li><a data-content="store" href="#0"><i class="fa fa-users"></i>Referral</a></li>
                        <li><a data-content="settings" href="#0"><i class=""> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span></i>Settings</a></li>
                    </ul>
                </nav>



                <ul class="cd-tabs-content">

                    <!-- ##### OVERVIEW TAB ###### -->
                    <li data-content="inbox" class="selected ">
                        <div class="row">
                            <div class="overview col-md-9 col-lg-9 col-xs-12"> <a href="#"><i class="fa  fa-caret-right"></i>Accoun Overview</a>
                                <p>Hey <?php echo ucwords($user['User']['name']);?>,<br>
                                    You can find below a list of your recent transactions including the current cashback status, as well the<br>
                                    total confirmed cashback and the amount paid out to you so far.</p>
                                <ul class="cashback-report">
                                    <li class="col-md-4 co-lg-4 col-xs-12 av_cash">
                                        <h4><i class="fa fa-exclamation"></i>Available Cashback</h4> <h3> <i class="fa fa-rupee"></i><?php echo $user['User']['balance'];?></h3></li>
                                    <li class="col-md-4 co-lg-4 col-xs-12 av_cash"> <h4><i class="fa fa-exclamation"></i>Pending Cashback</h4> <h3> <i class="fa fa-rupee"></i>00</h3></li>
                                    <li class="col-md-4 co-lg-4  col-xs-12 av_cash"> <h4><i class="fa fa-exclamation"></i>Lifetime Cashback</h4> <h3> <i class="fa fa-rupee"></i>00</h3></li>
                                </ul>
                                <div class="clearfix"></div>

                                <div class="table-responsive new_teble">
                                    <h3><i class="fa  fa-caret-right"></i>Recent Transactions</h3>
                                    <button class="btn-view" type="viewall" id="viewall_recent">View all</button>
                                    <table cellpadding="1" cellspacing="1" class="table">
                                        <thead>
                                            <tr class="table_recent">
                                                <th width="20%"><i class="fa fa-calendar"></i>Date</th>
                                                <th width="20%"><i class="fa fa-shopping-cart"></i>Marchant</th>
                                                <th width="20%"><i class="fa fa-rupee"></i>Amount</th>
                                                <th width="20%"><i class="fa fa-info-circle"></i>Status</th>
                                                <th width="20%">Ref.ID</th>
                                            </tr>
                                        </thead>
                                        <tbody class="recentFirst">
                                            <?php  if(empty($transRecent)){ ?>
                                            <tr><td colspan="5">There are none to display</td></tr>
                                            <?php }else{ ?>
                                            <tr>
                                                <td><?php echo date('m-d-Y', strtotime($transRecent['Transaction']['created']));?></td>
                                                <td><?php echo ucfirst($transRecent['Transaction']['type']);?></td>
                                                <td><?php echo $transRecent['Transaction']['amount'];?></td>
                                                <td><?php echo $transRecent['Transaction']['status'];?></td>
                                                <td><?php echo $transRecent['Transaction']['order_id'];?></td>
                                            </tr>
                                            <?php }  ?>
                                            
                                        </tbody>
                                        
                                        <tbody class="recentAll" style="display:none;">
                                            <?php  if(empty($transAll)){ ?>
                                            <tr><td colspan="5">There are none to display</td></tr>
                                            <?php }else{ 
                                            foreach($transAll as $trans){
                                                ?>
                                            <tr>
                                                <td><?php echo date('m-d-Y', strtotime($trans['Transaction']['created']));?></td>
                                                <td><?php echo ucfirst($transRecent['Transaction']['type']);?></td>
                                                <td><?php echo $trans['Transaction']['amount'];?></td>
                                                <td><?php echo $trans['Transaction']['status'];?></td>
                                                <td><?php echo $trans['Transaction']['order_id'];?></td>
                                            </tr>
                                            <?php }}  ?>
                                            
                                            <tr>
                                                <td colspan="5" id="paging">
                                                   <?php echo $this->paginator->first("<<"); ?> &nbsp;
                                                    <?php echo $this->paginator->prev('<'); ?> &nbsp;
                                                    <?php echo $this->paginator->numbers(array('separator'=>'&nbsp; ')); ?> &nbsp;			
                                                    <?php echo $this->paginator->next('>'); ?>&nbsp;		
                                                    <?php echo $this->paginator->last(">>"); ?>			 
<!--                                                </td>
                                                <td>-->
                                                    <?php echo $this->Html->image('spinner.gif', array('id'=>'spin','class'=>'spiner'));?>
                                                </td>
                                            </tr>
                                        </tbody>
                                        
                                    </table>
                                    
                                    
                                    
                                </div>
                                <div class="table-responsive new_teble">
                                    <h3><i class="fa  fa-caret-right"></i>Click history</h3>
                                    <button class="btn-view" type="view">View all</button>
                                    <table cellpadding="1" cellspacing="1" class="table">
                                        <thead>
                                            <tr class="table_recent">
                                                <th><i class="fa fa-calendar"></i>Date</th>
                                                <th><i class="fa fa-shopping-cart"></i>Marchant</th>
                                                <th>Ref.ID</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <td>***</td>
                                                <td></td>

                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
<!--                                <div class="table-responsive new_teble">
                                    <h3><i class="fa  fa-caret-right"></i>Missing Transtions</h3>
                                    <button class="btn-view" type="view">View all</button>
                                    <table cellpadding="1" cellspacing="1" class="table">
                                        <thead>
                                            <tr class="table_recent">
                                                <th><i class="fa fa-calendar"></i>Date</th>
                                                <th><i class="fa fa-shopping-cart"></i>Marchant</th>
                                                <th><i class="fa fa-rupee"></i>Amount</th>
                                                <th><i class="fa fa-info-circle"></i>Status</th>
                                                <th>Ref.ID</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>00</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>-->

                            </div>
                            <div class="balance col-md-3 col-lg-3 col-xs-12">
                                <h5>Your Account Balance<span><i class="fa fa-info-circle"></i></span></h5>
                                <h3> <i class="fa fa-rupee"></i>
                                <?php echo $user['User']['balance'];?>
                                </h3>
                                <h6>Approved Cashback</h6>
                                <button class="btn-withdraw" type="button">Redeem Now!</button>
                                <p>Pending Cashback <span>0.00</span></p>
                            </div>

                        </div>
                    </li>

                    <!-- ##### CASHBACK SUMMERY TAB ###### -->

                    <li data-content="new">
                        <div class="row">
                            <div class="overview col-md-10 col-lg-10 col-xs-12"> <a href="#"><i class="fa  fa-caret-right"></i>Cashback Summary</a>
                                <p>Hey <?php echo ucwords($user['User']['name']);?>,<br>
                                    You can find below a list of your recent transactions including the current cashback status, as well the<br>
                                    total confirmed cashback and the amount paid out to you so far.</p>
                            </div>
                            <div class="col-md-2 col-lg-2 col-xs-12 redeem">
                                <h4><button class="btn-withdraw" type="button" style="margin:0px; padding: 0px;">Redeem Now!</button></h4>
                                <h3><i class="fa fa-rupee"></i><?php echo $user['User']['balance'];?></h3>

                            </div> 
                            <ul class="cashback-report col-md-12">
                                <li class="col-md-3 co-lg-3 col-xs-12 av_cash new_a"> <h4><i class="fa fa-exclamation"></i>Available Cashback</h4> <h3> <i class="fa fa-rupee"></i><?php echo $user['User']['balance'];?></h3></li>
                                <li class="col-md-3 co-lg-3  col-xs-12 av_cash new_a"> <h4><i class="fa fa-exclamation"></i>Pending Cashback</h4> <h3> <i class="fa fa-rupee"></i>00</h3></li>
                                <li class="col-md-3 co-lg-3 col-xs-12 av_cash new_a"> <h4><i class="fa fa-exclamation"></i>Lifetime Cashback</h4> <h3> <i class="fa fa-rupee"></i>00</h3></li>
                                <li class="col-md-3 co-ld-3 col-xs-12 av_cash new_a"> <h4><i class="fa fa-exclamation"></i>Referral Amount</h4> <h3> <i class="fa fa-rupee"></i>00</h3></li>
                                
                                <p style="float:left; margin: -18px 0 0 10px;font-family:open Sans; font-size:14px; color: #666; line-height: 1.5; width: 90%;"><span style="color:#05B2D2;">Please Note: </span>Rewards can be redeemed via Mobile/DTH recharge & Shopping Vouchers, it can also be redeemed by Bank Transfer. Cashback statistics are not updated in realtime. There might be some delay in showing updated balance.</p>
                            </ul>
                            
                            <div class="table-responsive new_teble col-md-9 col-xs-12">
                                <h3><i class="fa  fa-caret-right"></i>Detailed Summery</h3>
                                <button class="btn-view" type="view">View all</button>
                                <table cellpadding="1" cellspacing="1" class="table">
                                    <thead>
                                        <tr class="table_recent">
                                            <th><i class="fa fa-calendar"></i>Date</th>
                                            <th><i class="fa fa-shopping-cart"></i>Marchant</th>
                                            <th><i class="fa fa-rupee"></i>Amount</th>
                                            <th><i class="fa fa-info-circle"></i>Status</th>
                                            <th>Ref.ID</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>00</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Custome Range</label>
                                    <div class="col-md-3">
                                        <div class="input-group input-daterange">
                                            <input type="text" class="form-control" name="start" placeholder="From">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group input-daterange">
                                            <input type="text" class="form-control" name="end" placeholder="To">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="button" class="get">Apply</button
                                    </div>
                                </div>
                            </div>

                            </div>
                        </div>
                    </li>

                    <!-- ##### ACTIVITY TAB ###### -->
                    <li data-content="gallery">

                        <div class="row">
                            <div class="overview col-md-9 col-lg-9 col-xs-12"> 
                                <a href="#"><i class="fa  fa-caret-right"></i>Activity</a>
                                <p>Hey <?php echo $user['User']['name'];?>,<br />
                                    you can find below a list of your recent transactions including the current cashback status, as well the<br />
                                    total confirmed cashback and the amount paid out to you so far.</p>
<!--                                <ul class="cashback-report">
                                    <li class="col-md-4 co-ld-4 av_cash">
                                        <h4><i class="fa fa-exclamation "></i>Available Cashback</h4> 
                                        <h3><i class="fa fa-rupee"></i><?php //echo $user['User']['balance'];?>
                                        </h3>
                                    </li>
                                    <li class="col-md-4 co-ld-4 av_cash"> 
                                        <h4><i class="fa fa-exclamation"></i>Available Cashback</h4> 
                                        <h3> <i class="fa fa-rupee"></i>00</h3>
                                    </li>
                                    <li class="col-md-4 co-ld-4 av_cash"> 
                                        <h4><i class="fa fa-exclamation"></i>Available Cashback</h4> 
                                        <h3> <i class="fa fa-rupee"></i>00</h3>
                                    </li>
                                </ul>-->
                                <div class="clearfix"></div>

                                <div class="table-responsive new_teble ">
                                    <h3><i class="fa fa-caret-right"></i>Recent Transactions</h3>
                                    <button class="btn-view" type="view">View all</button>
                                    <table cellpadding="1" cellspacing="1" class="table">
                                        <thead>
                                            <tr class="table_recent">
                                                <th><i class="fa fa-calendar"></i>Date</th>
                                                <th><i class="fa fa-shopping-cart"></i>Marchant</th>
                                                <th><i class="fa fa-rupee"></i>Amount</th>
                                                <th><i class="fa fa-info-circle"></i>Status</th>
                                                <th>Ref.ID</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>00</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="table-responsive new_teble">
                                    <h3><i class="fa  fa-caret-right"></i>Click history</h3>
                                    <button class="btn-view" type="view">View all</button>
                                    <table cellpadding="1" cellspacing="1" class="table">
                                        <thead>
                                            <tr class="table_recent">
                                                <th><i class="fa fa-calendar"></i>Date</th>
                                                <th><i class="fa fa-shopping-cart"></i>Marchant</th>
                                                <th>Ref.ID</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <td>***</td>
                                                <td></td>

                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
<!--                                <div class="table-responsive new_teble ">
                                    <h3><i class="fa  fa-caret-right"></i>Missing Transtions</h3>
                                    <button class="btn-view" type="view">View all</button>
                                    <table cellpadding="1" cellspacing="1" class="table">
                                        <thead>
                                            <tr class="table_recent">
                                                <th><i class="fa fa-calendar"></i>Date</th>
                                                <th><i class="fa fa-shopping-cart"></i>Marchant</th>
                                                <th><i class="fa fa-rupee"></i>Amount</th>
                                                <th><i class="fa fa-info-circle"></i>Status</th>
                                                <th>Ref.ID</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>00</td>
                                                <td>N/A</td>
                                                <td>/td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>-->
                            </div>
                            <div class="balance col-md-3 col-lg-3col-xs-12">
                                <h5>Your Account Balance<span><i class="fa fa-info-circle"></i></span></h5>
                                <h3> <i class="fa fa-rupee"></i><?php echo $user['User']['balance'];?></h3>
                                <h6>Approved Cashback</h6>
                                <button class="btn-withdraw" type="button">Redeem now!</button>
                                <p>Pending Cashback <span>0.00</span></p>
                            </div>
                        </div>                        
                    </li>
                    
                    <!--########## REDEEM TAB ######-->
                    <li data-content="redeem">
                        <div class="row">
                            <div class="redeem col-md-9 col-lg-9 col-xs-12">
                            
                            <div class="col-sm-12 white-well profile-steps text-center profile_edit">
                                
                                    <div class="col-sm-5 redeem-tab border active-step">
                                        <span class="fa fa-mobile"></span>
                                        <a href="#" class="recharge-now">Recharge</a>
                                    </div>
                                    <div class="col-sm-5 redeem-tab " >
                                        <i class="glyphicon glyphicon-user"></i>
                                        <a href="#">Online Transfer</a> 
                                    </div>
                                
                            </div>
                        </div>
                            <div class="balance col-md-3 col-lg-3 col-xs-12">
                                <h5>Your Account Balance<span><i class="fa fa-info-circle"></i></span></h5>
                                <h3> <i class="fa fa-rupee"></i>
                                <?php echo $user['User']['balance'];?>
                                </h3>
                                <input type="hidden" id="userBalance" value="<?php echo $user['User']['balance'];?>" />
                                <h6>Approved Cashback</h6>
                                <button class="btn-withdraw123" type="button"></button>
                                <p>Pending Cashback <span>0.00</span></p>
                            </div>
                                
                            </div>                        
                    </li>
                    <!--########## REDEEM ######-->
                    
                    <!-- ##### REFERRAL TAB ###### -->

                    <li data-content="store">
                        <div class="row ref-back">

                            <div class="refrel col-md-9 col-lg-9 col-xs-12"> <a href="#"><i class="fa  fa-caret-right"></i>Referral</a>
                                <div class="ref-back-smry">
                                    <p>Invite your friends and get <i class="fa fa-rupee"></i> 30 cashback into your account! Simply refer your friends to Paisamint.Note: Please ensure you are logged onto your Paisamint account before referring your friends.</p></div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <button class="ref-back-Redeem-btn">Start Reffering Now</button>
                            </div>
                            <div class="clearfix"></div>
                            <div class="referral-bg col-md-12">
                                <div class="first"><h4>Earn cashback every timeyou refer someone</h4>
                                </div>
                                <div class="second">  <h4><span>Help a friend save and earn <i class="fa fa-rupee"></i>30 </span></h4>
                                </div>


                                <div class="clearfix"></div>
                            </div>
                            <div class="row ref-f col-md-12 xol-lg-12">
                                <div class="col-md-4 col-xs-12">
                                    <div class="myimg">
                                        <?php echo $this->Html->image('/images/1st.png',array('class'=>'myimg1'));?>
                                    </div>
                                    <p>Refer Your Friends and family
                                        members to sign up paisamint
                                        using the above button
                                    </p>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="myimg">                                                                          <?php echo $this->Html->image('/images/iind.png',array('class'=>'myimg1'));?>
                                    </div>
                                    <p>Refer Your Friends and family
                                        members to sign up paisamint
                                        using the above button
                                    </p>
                                </div>
                                <div class="col-md-4 col-xs-12"> 
                                    <div class="myimg">
                                        <?php echo $this->Html->image('/images/iiird.png',array('class'=>'myimg1'));?>
                                    </div>
                                    <p>Refer Your Friends and family
                                        members to sign up paisamint
                                        using the above button
                                    </p>
                                </div>
                            </div> 
                            <div class="col-md-12 col-xs-12 click_earn">
                                <h2>
                                    <span class="mian_text">Click and share - 
                                        <span class="earn_t">Earn</span>
                                        <span class="cash">
                                            <i class="fa fa-rupee"></i>30
                                        </span>
                                    </span>
                                </h2>
                            </div>

                            <div class="soclal_share col-md-12 col-lg-12 col-xs-12">
                                <?php $surl = BASE_URL.'?r='.$shareId.'#register';?>
                                <div class="col-lg-4 col-xs-12 facbook_share">

                                    <div class="soc_1">
                                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $surl;?>" target="_blank">
                                            <i class="fa fa-facebook"></i>
                                            <span>Facebook</span>
                                        </a>
                                    </div>
                                    <p>Click to share your referral link on your Facebook wall.</p>
                                </div>
                                <div class="col-lg-4 col-xs-12 googel_share">
                                    <div class="soc_2">
                                        <a href="https://plus.google.com/share?url=<?php echo $surl;?>" target="_blank">
                                            <i class="fa fa-google-plus"></i>
                                            <span>Google+</span>
                                        </a>
                                    </div>
                                    <p> Click to share your referral link on your Google+ wall.</p>
                                </div>
                                <div class="col-lg-4 col-xs-12 twitter_share">
                                    <div class="soc_3">
                                        <a href="https://twitter.com/home?status=<?php echo $surl;?>" target="_blank">
                                            <i class="fa fa-twitter" ></i>
                                            <span>Twitter</span>
                                        </a>
                                    </div>
                                    <p>Click to tweet your referral link on your Twitter page.</p>
                                </div>
                            </div>

                            <div class=" col-md-8 col-sm-offset-2 news_letter">
                                <form id="ref-frnd-form" action="#" method="post">
                                    <div class="form-group">
                                        <input type="text" class="newaleter" id="referFriend" name="referFriend" placeholder="Invite friends to cashitback via email">
                                        <span class="glyphicon glyphicon-envelope form-control-feedback" onclick="send_invite();" style="pointer-events:all; cursor:pointer;"></span>
                                        <div class="spiner" id="refSpin" style="float:right;">
                                            <?php echo $this->Html->image('spinner.gif'); ?>
                                        </div>                                    
                                        <span class="" id="refError"></span>
                                    </div>                                
                                </form>
                            </div>
                            <div class="col-md-12 col-lg-12 tex p_text">
                                <p>
                                    To receive your Tell-a-Friend bonus, the person you have referred must sign up to Paisamint from your link and earn ₹ 30 payable cashback 
                                    or more. View the full Tell-a-Friend Terms and Conditions.
                                </p>
                            </div>
                        </div>
                    </li>

                    <!-- ##### SETTINGS TAB ###### -->

                    <li data-content="settings">
                        <div class="overview col-md-10 col-lg-10 col-xs-12"> <a href="javascript:void(0);"><i class="fa  fa-caret-right"></i>Settings</a></div>


                        

                        <div class=" col-md-6 col-md-offset-3 profile_edit">
                            <div class="col-sm-11 white-well profile-steps text-center">
                                <div class="row">
                                    <div class="col-sm-6 border active-step sett" id="basic-details">
                                        <span class="glyphicon glyphicon-user"></span>
                                        Personal Settings
                                    </div>
                                    <div class="col-sm-6 sett" id="full-profile-details">
                                        <i class="fa fa-search"></i>
                                        Change Password. 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="passwwordError" class="error"></div>
                        <div id="basic-details-div" class="formdiv">
                        <?php 
                        $frmOptions = array('action'=>'updateProfile','class'=>'form-horizontal', 'novalidate' => true);
                        echo $this->Form->create('User', $frmOptions); ?>
                            <div class="form-group">
                                <label for="fullname" class="col-sm-4 control-label">Full Name</label>
                                <div class="col-sm-6">    
                            <?php echo $this->Form->input('name', array('label'=>false, 'div'=>false,'placeholder'=>'Full Name*','class'=>'form-control','value'=>$user['User']['name'])); ?>
                                    <span class="error"><?php if(isset($nameError)){ echo $nameError;}?></span>    
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="emailid" class="col-sm-4 control-label">Email-ID</label>
                                <div class="col-sm-6">
                                <?php echo $this->Form->input('', array('label'=>false, 'div'=>false,'placeholder'=>'Email ID*','class'=>'form-control','value'=>$user['User']['email'], 'disabled'=>'disabled')); ?>
                                    <span class="error"><?php if(isset($emailError)){ echo $emailError;}?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="username" class="col-sm-4 control-label">User Name</label>
                                <div class="col-sm-6">
                                <?php echo $this->Form->input('username', array('label'=>false, 'div'=>false,'placeholder'=>'Username','class'=>'form-control','value'=>$user['User']['username'])); ?>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="mobile" class="col-sm-4 control-label">Mobile No.</label>
                                <div class="col-sm-6">
                                <?php echo $this->Form->input('', array('label'=>false, 'div'=>false,'placeholder'=>'Mobile No.','class'=>'form-control','value'=>$user['User']['mob'], 'disabled'=>'disabled')); ?>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-offset-5 col-md-4 last_btn" style="text-align:center;">
                        <?php 
                        $options = array('label' => 'Update', 'class' => 'btn-feat', 'id' => 'profileFormBtn');
                        echo $this->Form->end($options);
                        ?>
                                </div>
                            </div>

                        </div>
                        <div id="full-profile-details-div" class="formdiv" style="display: none;">

                        <?php 
                        $frmOptions = array('action'=>'updatePassword','class'=>'form-horizontal','novalidate' => true);
                        echo $this->Form->create('User', $frmOptions); ?>

                    
                            <div class="form-group">
                                <label for="fullname" class="col-sm-4 control-label">Current Password</label>
                                <div class="col-sm-6">    
                            <?php echo $this->Form->input('currpassword', array('label'=>false, 'div'=>false,'placeholder'=>'Current Password','class'=>'form-control','type'=>'password')); ?>
                                    <span class="error"><?php if(isset($currPassError)){ echo $currPassError;}?></span>   
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="emailid" class="col-sm-4 control-label">New Password</label>
                                <div class="col-sm-6">
                                <?php echo $this->Form->input('password', array('label'=>false, 'div'=>false,'placeholder'=>'New Password','class'=>'form-control')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="username" class="col-sm-4 control-label">Confirm Password</label>
                                <div class="col-sm-6">
                                <?php echo $this->Form->input('confpassword', array('label'=>false, 'div'=>false,'placeholder'=>'Confirm Password','class'=>'form-control','type'=>'password')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-5 col-md-4 last_btn" style="text-align:center;">
                        <?php 
                        $options = array('label' => 'Update', 'class' => 'btn-feat', 'id' => 'profileFormBtn');
                        echo $this->Form->end($options);
                        ?>
                        <div class="loader2"><i class="fa fa-spinner fa-pulse"></i></div>
                                </div>
                            </div>
                        </div>

                    </li>

                </ul>
            </div>
        </div>
    </div>
</section>

<?php //echo $this->element('sql_dump');?>

<?php echo $this->element('recharge_popup'); ?>



<script>
    $(document).ready(function () {
        $("#UserUpdatePasswordForm #profileFormBtn").click(function(e) {
            e.preventDefault();

            $.ajax({
                type: "POST",
                url: "<?php echo BASE_URL;?>users/updatePassword",
                data: $("#UserUpdatePasswordForm").serialize(),
                beforeSend: function (xy) {
                    $('.loader2').show();
                },
                success: function (res) {
                    $(".loader2").hide();
                    if (res != 'done') {
                        $("#passwwordError").html(res);
                    }

                }
            }); 
        });
    });

</script>
