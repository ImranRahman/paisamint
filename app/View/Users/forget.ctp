<?php

//pr($data); ?>
<section class="work-bg">
    <div style="padding: 50px; text-align: center;">
        <h3><?php echo $data['heading'];?></h3>
        <p style="margin-top: 20px;"><?php echo $data['message'];?></p>
    </div>

    <?php if($data['heading'] != "Error occured!"){ ?>
        <div class="row">
            <div class="co-md-12 col-lg-12 main login-form ">
                
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-sm-offset-3 share">
                      <div class="modal-content">
                          <h3>&nbsp;</h3>
    <?php echo $this->Form->create('User', array('action'=>'forget/'.$data['id'].'/'.$data['code'], 'class'=>'form login_form')); ?>
                        <div class="form-group">
        <?php echo $this->Form->input('password', array('label'=>false,'class'=>'form-control','placeholder'=>'Password*','required'=>false)); ?>
                        </div>
                        <div class="form-group">
        <?php echo $this->Form->input('confpassword', array('label'=>false,'class'=>'form-control','placeholder'=>'Confirm password*', 'type'=>'password','required'=>false)); ?>
                        </div>

    <?php $options = array('label' => 'Submit', 'class' => 'btn btn btn-join-free', 'id' => '');
                echo $this->Form->end($options); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</section>


<?php //echo $this->element('sql_dump'); ?>