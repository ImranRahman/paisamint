<?php ?>
<style>
 .prod-1{ margin: 0 0 3% 2.3%;}
.prod-1 img {
  background: none repeat scroll 0 0 #fff;
  border: 1px solid #a7a7a7;
  border-radius: 4px;
  box-shadow: 1px 3px 10px rgba(0, 0, 0, 0.14);
  margin-top: 15px;
  padding: 0px; 
  text-align: center;
  width: 228px;
  height: 107px;
}
.prod-1 ul li h5 {
  color: #1bbc9b;
  font-size: 23px;
  padding: 10px 0;
  height: 70px;
  overflow: hidden;
  margin-bottom: 15px;
  margin-top:0;
}
.my-p { margin-left: 15px; }
.mobile_offer{ margin-top: 0;}
.more-div{ display: none; padding: 10px; border-radius: 4px; width: 100%;}
.popover{ background-color:thistle;}
.table {
  position: relative;
  top: 0px;  
  left: 10px;  
 }
 .table tr td { border: none !important;}
</style>
<section class=""> 
    <!--Electronics & Appliances-->
<div class="container">
<!--<div class="top_heading col-lg-12 col-md-12">
<div class="co-lg-9 col-md-9 col-sm-9">
<ul>
<li>
    <a href="<?php echo BASE_URL;?>">Home<span><i class="fa fa-caret-right"></i></span></a>
    <a href="<?php echo BASE_URL;?>categories"><?php echo $model;?>
        <span><i class="fa fa-caret-right"></i></span>
    </a>
    <?php $params = $this->params; if(empty($params['pass'])){ ?>
    All
    <?php } else { ?>
    
        <?php echo ucwords($this->SeoUrl->friendlyUrl($params['pass'][0]));?>
        <span><i class="fa  fa-caret-right"></i></span>
    
    <?php if(isset($params['pass'][1])){ 
        echo ucwords($params['pass'][1]);
     } ?>
    
    <?php } ?>
    </li>

</ul>                                             
</div>
    
<div class="co-lg-3 col-md-3 col-sm-3 new_twit">
<ul class="tweet">
<li><a href="#"><i class="fa fa-twitter"> </i><span>Tweet</span></a></li>
<li class="googe-plus"> <i class="fa fa-google-plus"></i><span>1 </span><span>7</span></li>
</ul>                                                                                                                                          
</div>
</div>-->

<div class="col-lg-12 col-md-12 elec_cont">
<div class="col-lg-3 col-md-3 slidebar">
<div class="left_section">
<h4><?php echo $this->Html->image('/images/cat/bag.png');?><span>Browse By Categories</span></h4>
<?php //pr($categories);
$c = 1; $disp='style="display:none;"'; $urlCate='';
foreach($categories as $category){ //pr($category); die;
    $cateName = $category['Category']['name'];
    $cateId = $category['Category']['id'];
    
    if(isset($params['pass'][0])){
        $urlCate = $this->SeoUrl->friendlyUrl($params['pass'][0]);
        if(strcasecmp($urlCate, $cateName)==0){
            $disp = 'style="display:block;"';        
        }else{
            $disp='style="display:none;"';
        }
    }    
?>
<ul class="cate-container">
<h5 onclick="show(<?php echo $cateId;?>,this)" itemid="0">
    <i class="fa fa-caret-<?php echo (strcasecmp($urlCate, $cateName)==0)?'down':'right';?>"></i>
    <?php echo $cateName;?>
</h5>
    
<div id="<?php echo $cateId;?>" class="subcate-container" <?php echo $disp;?>>
<?php 
foreach($category['SubCategory'] as $subCate){  
?>
<li>
    <a href="<?php echo BASE_URL.'categories/'.$this->SeoUrl->seoUrl($cateName).'/'.$this->SeoUrl->seoUrl($subCate['name']);?>">
        <i class="fa fa-camera"></i><?php echo $subCate['name'];?>
    </a>
</li>

<?php } ?>
</div>
</ul>
<?php $c++; } ?>
</div>

<div class="left_section deal">
<h3><i class="fa fa-tag"></i><span>Deal Of the Day</span></h3>
<?php echo $this->Html->image('/images/cat/small-banner.png');?>
</div>
</div>

<!-- ########### RIGHT SECTION ####################### -->

<div class="col-lg-9 col-md-9 mobile_offer">
<div class="offer_heading">

<ul>
<li>
    <!--<h4>Top Mobile & Tablets Offer Offers </h4>-->
<!--<p>Coupon Type<span><select class="form-control type">
  <option>All</option>
  <option>2</option>
  <option>3</option>
  <option>4</option>
  <option>5</option>
  
</select></span></p>-->
</li>
</ul>

<?php  //echo $this->element('sql_dump'); die;
if(empty($coupons)){ ?>
    <div class="alert alert-info">No Data Found</div>  
 <?php 
}else{
foreach($coupons as $coupon) {
$cid = $coupon['Coupon']['id'];    
?>
<div class=" col-md-4 col-lg-4 col-xs-12 prod-1 my-p">
<ul style="min-height:340px; padding-bottom: 15px;">
<li> 
    <?php
    if($coupon['Retailer']['logo']!='' && file_exists(RETAILER_MID_THUMB.$coupon['Retailer']['logo'])){
        echo $this->Html->image(RETAILER_MID_THUMB_URL.$coupon['Retailer']['logo'], array("alt" => "Logo Here", 'url' => array('controller' => 'retailers', 'action' => '/', $this->SeoUrl->seoUrl($coupon['Retailer']['name']))));
    }else{
        echo $this->Html->image('noimage.jpg', array("alt" => "Logo Here", 'url' => array('controller' => 'retailers', 'action' => '/', $this->SeoUrl->seoUrl($coupon['Retailer']['name']))));
    }
    ?>
    <h5 title="<?php echo $coupon['Coupon']['title'];?>"><?php echo $coupon['Coupon']['title'];?></h5>
    <p><?php echo $desc = $this->SeoUrl->wordWrap($coupon['Coupon']['description'],50);?></p>
    <a id="more_<?php echo $cid;?>" class="more_btn" href="#">More</a>
    <div class="more-div popover bottom" id="div_more_<?php echo $cid;?>">
        <?php echo $coupon['Coupon']['description'];?>
    </div>
    <button type="button" class="btn btn-get" onclick="couponPopup(<?php echo $cid;?>);">Get Code</button>
    
    <span>Verified
    <?php 
    echo CakeTime::timeAgoInWords($coupon['Coupon']['created'], array(
        'accuracy' => array('month' => 'month'),
        'end' => '1 year'
    ));
    ?>
    </span>
</li>
</ul>
    <style>
        .like_box li a i{ padding: 5px 0;}
    </style>
<!--<ul class="like_box">
<?php $Clikes = $this->requestAction('categories/getCouponLikes/'.$cid); //pr($Clikes); ?>
<li class="col-md-5" style="padding:0;">
    <?php $likeClass = ($Clikes['userLike']==1 || $Clikes['userLike']==2)?'':'coup-likes'; ?>
    <a href="javascript:void(0);" class="col-md-6 <?php echo $likeClass;?>" id="like_<?php echo $cid;?>" style="padding:0;">
        <i class="fa fa-thumbs-o-up" style="<?php echo ($Clikes['userLike']==1)?'color:#0DE238;':'';?>"></i>
        <span><?php echo $Clikes['likes'];?></span>
    </a>
    <a href="javascript:void(0);" class="col-md-6 <?php echo $likeClass;?>" id="dislike_<?php echo $cid;?>" style="padding:0;">
        <i class="fa fa-thumbs-o-down" style="<?php echo ($Clikes['userLike']==2)?'color:red;':'';?>"></i>
        <span><?php echo $Clikes['disLikes'];?></span>
    </a>
</li>
<?php $Ccomment = $this->requestAction('comments/getCouponComment/'.$cid); //pr($Ccomment); ?>
<li class="col-md-5" style="padding:0 2px;">
    <?php $comstyle = ($Ccomment==1)?'color:#0DE238;':'';?>
    <a href="<?php echo BASE_URL.'comments/coupon/'.$cid;?>">        
        <i class="fa fa-comment-o" style="<?php echo $comstyle;?>"></i>
        <span style="font-size:14px;">Comments</span>
    </a>
</li>
<?php $Cfav = $this->requestAction('categories/getCouponFav/'.$cid); //pr($Cfav); ?>
<li class="col-md-2" style="padding:0;">
    <?php if($Cfav==1){ $fstyle='color:#0DE238;';}else{ $fstyle='';}?>
    <a href="javascript:void(0);" class="coup-fav"  id="fav_<?php echo $cid;?>">
        <i class="fa fa-heart-o" style="<?php echo $fstyle;?>"></i>
    </a>
</li>
</ul>-->
</div>
<?php } ?> 
    
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr>
        <td width="20%">
            Page <?php echo $this->paginator->counter(); ?>
        </td>
        <td width="80%">
            <?php echo $this->paginator->first("<<"); ?> &nbsp;&nbsp;
            <?php if($this->paginator->hasPrev()){ echo $this->paginator->prev('Previous'); } ?> &nbsp;&nbsp;
            <?php echo $this->paginator->numbers(array('separator'=>'&nbsp; &nbsp;')); ?> &nbsp;&nbsp;
            <?php if($this->paginator->hasNext()){ echo $this->paginator->next('Next'); } ?>&nbsp;&nbsp;			
            <?php echo $this->paginator->last(">>"); ?>            
            <!--This is mandatory-->
            
        </td>
        
    </tr>
</table>
<?php } ?>
</div>

</div>

</div>

</div>
</section>

<?php echo $this->element('coupon_popup'); ?>

<script>
function show(id, obj){
    $('.subcate-container').hide();    
    $('#'+id).show();
} 

$(function(){
   $('.more_btn').click(function(e){
      var ID = $(this).attr('id');
      var pos = $(this).position();
      $('#div_'+ID).css('top',(pos.top)+20);
      $('#div_'+ID).toggle();
      e.preventDefault();
   });    
});
</script>