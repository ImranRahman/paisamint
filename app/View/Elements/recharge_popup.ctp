<?php ?>
<div class="modal fade col-md-6 col-md-offset-3" id="popup-withdraw">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title">Recharge Mobile</h2>
            </div>
            <div class="modal-body">

                <form action="<?php echo BASE_URL;?>users/recharge" method="post" id="rech-form">
                    <input type="hidden" name="user" id="user_id" value="<?php echo $this->Session->read('user_data.id');?>" />
                    <div class="rechargeDivContainer">
                        <div class="form-group">
                            <label>Choose Operator:</label><br/>
                            <select name="operator" id="operator" class="form-control">
                                <option value="">Select Operator</option>
                                <option value="AT">Airtel</option> 
                                <option value="AL">Aircel</option> 
                                <option value="BS">BSNL</option> 
                                <option value="BSS">BSNL Special</option> 
                                <option value="IDX">Idea</option> 
                                <option value="VF">Vodafone</option>
                                <option value="TD">Docomo GSM</option> 
                                <option value="TDS">Docomo GSM Special</option> 
                                <option value="TI">Docomo CDMA (Indicom)</option> 
                                <option value="RG">Reliance GSM</option> 
                                <option value="RL">Reliance CDMA</option> 
                                <option value="MS">MTS</option> 
                                <option value="UN">Uninor</option> 
                                <option value="UNS">Uninor Special</option> 
                                <option value="VD">Videocon</option> 
                                <option value="VDS">Videocon Special</option> 
                                <option value="MTM">MTNL Mumbai</option> 
                                <option value="MTMS">MTNL Mumbai Special</option> 
                                <option value="MTD">MTNL Delhi</option> 
                                <option value="MTDS">MTNL Delhi Special</option> 
                                <option value="VG">Virgin GSM</option> 
                                <option value="VGS">Virgin GSM Special</option> 
                                <option value="VC">Virgin CDMA</option> 
                                <option value="T24">T24</option> 
                                <option value="T24S">T24 Special</option>
                            </select>
                            <span class="error" id="operator_error"></span>
                            <span class="error"><?php echo isset($operator_error)?$operator_error:'';?></span>
                        </div>

                        <div class="form-group">
                            <label>Enter Mobile Number:</label><br/>
                            <input type="text" id="servicenumber" name="servicenumber" maxlength="10" class="form-control" />
                            <span class="error" id="mobno_error"></span>
                            <span class="error"><?php echo isset($mobno_error)?$mobno_error:'';?></span>
                        </div>

                        <div class="form-group">
                            <label>Enter Amount:</label><br/>
                            <input type="text" id="amount" name="amount" class="form-control" />
                            <span class="error" id="amt_error"></span>
                            <span class="error"><?php echo isset($amt_error)?$amt_error:'';?></span>
                        </div>
                    </div>
                    <div class="otp-div-container">
                        <div class="form-group">
                            <div id="otpMsg" style="font-size: 15px; font-weight: bold; margin-bottom: 15px; color:green;">
                                An OTP is sent to your mobile no. Please confrim OTP.
                            </div>

                        </div>
                        <div class="form-group">
                            <label>Enter OTP sent to your mobile no.</label><br/>
                            <div class="control-group">
                                <input type="text" name="otp" id="otp" class="form-control" maxlength="6" />
                            </div>
                        </div>
                        <div class="form-group error" id="otpError"></div>

                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" id="req-otp" class="btn btn-info">Resend OTP</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" id="recharge-btn" class="btn btn-primary">Recharge</button>
                <button type="button" id="otp-btn" class="btn btn-primary">Submit</button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade col-md-6 col-md-offset-3" id="popup-balance">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title" style="font-weight: bold;">Alert</h2>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger" style="font-size: 22px; font-weight: bold;">You don't have sufficient balance!</div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->