<!---   Join  --->
<div class="modal fade" id="join-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="right_close">
        <div class="row">
          <div class="co-md-6 col-lg-6 main ">
            <h3>Signup to Paisamint</h3>
            <div class="row">
              <div class="col-xs-6 col-sm-6 col-md-6 share"> <a href="#" class="btn btn-lg btn-primary btn-block new-btn"><i class=" fa fa-facebook"></i>Sign With Facebook</a> </div>
              <div class="col-xs-6 col-sm-6 col-md-6 share"> <a href="#" class="btn btn-lg btn-info btn-block new-btn googel"><i class="fa fa-google-plus"></i>Sign With Google</a> </div>
            </div>
            <div class="login-or">
              <hr class="hr-or">
              <h4> Create New One </h4>
              <span class="span-or">or</span> </div>
<!--            <form class="form join_form">-->
<?php echo $this->Form->create('User', array('action'=>'register', 'class'=>'form login_form')); ?>
              <div class="form-group">
                <!--<input type="text" class="form-control" id="inputUsernameEmail join_input" placeholder="Full Name*">-->
                  <?php echo $this->Form->input('name', array('label'=>false,'class'=>'form-control','placeholder'=>'Full Name*')); ?>
              </div>
              <div class="form-group">
                <!--<input type="email" class="form-control " id="inputUsernameEmail join_input" placeholder="Email address*">-->
                  <?php echo $this->Form->input('email', array('label'=>false,'class'=>'form-control','placeholder'=>'Email address*')); ?>
              </div>
              <div class="form-group">
                <!--<input type="text" class="form-control " id="inputUsernameEmail join_input" placeholder="Mobile Number*">-->
                  <?php echo $this->Form->input('mob', array('label'=>false,'class'=>'form-control','placeholder'=>'Mobile Number*')); ?>
              </div>
              <div class="form-group">
                <!--<input type="password" class="form-control " id="inputPassword join_input" placeholder="password*">-->
                  <?php echo $this->Form->input('password', array('label'=>false,'class'=>'form-control','placeholder'=>'password*')); ?>
              </div>
              <div class="form-group">
                <!--<input type="password" class="form-control " id="inputPassword join_input" placeholder="Confirm password*">-->
                  <?php echo $this->Form->input('confPassword', array('label'=>false,'class'=>'form-control','placeholder'=>'Confirm password*', 'type'=>'password')); ?>
              </div>
            
              <!--<input type="checkbox">-->
                <?php echo $this->Form->checkbox('term', array('hiddenField' => false)); ?>
              <span>I agree to the Tearms and Conditions</span>
<!--              <button type="submit" class="btn btn btn-join-free"> Join Free Now </button>
            </form>-->
            <?php $options = array(
    'label' => 'Join Free Now',
    'class' => 'btn btn btn-join-free'
);
echo $this->Form->end($options); ?>
          </div>
          <div class="co-md-6 col-lg-6 join">
            <div class="row">
              <h3>Join India’s Top Cashback Website.</h3>
              <div class="why_signup">
                <ul>
                  <li><i class=" fa fa-caret-right"></i> Why Signup?</li>
                  <li><i class=" fa fa-caret-right"></i>Personalize your experience</li>
                  <li><i class=" fa fa-caret-right"></i>Get access to cashback offers</li>
                  <li><i class=" fa fa-caret-right"></i>Get alerts on trending discount coupons</li>
                  <li><i class=" fa fa-caret-right"></i>Favourite your brands</li>
                  <li><i class=" fa fa-caret-right"></i>Get access to contests and promotions</li>
                  <li><i class=" fa fa-caret-right"></i>Set newsletter preferences</li>
                </ul>
                <h6>Just Shop through us.Save your Money.
                  Happy Saving ! Happy Shopping..!!!</h6>
                </p>
                <h5>Save Money on TOP Brands:</h5>
                <div class="brand-follow"> 
                    <a href="#"><img src="img/flipkart.png"></a> 
                    <a href="#"><img src="img/mantra.png">
                    </a> <a href="#"><img src="img/yep.png"></a> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="login_form_bg" id="pop_form_mask" style="display: block;"> </div>
  </div>
</div>


<!---   Login  --->

<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog login">
    <div class="modal-content">
      <div class="row">
        <div class="co-md-12 col-lg-12 main login-form ">
          <h3>Signup to Paisamint</h3>
          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 share"> <a href="#" class="btn btn-lg btn-primary btn-block new-btn"><i class=" fa fa-facebook"></i>Sign With Facebook</a> </div>
            <div class="col-xs-6 col-sm-6 col-md-6 share"> <a href="#" class="btn btn-lg btn-info btn-block new-btn googel"><i class="fa fa-google-plus"></i>Sign With Google</a> </div>
          </div>
          <div class="login-or">
            <hr class="hr-or">
            <h4> Create New One </h4>
            <span class="span-or">or</span> </div>
          <!--<form class="form login_form" id="login-from" method="post" action="#">-->
          <?php echo $this->Form->create('User', array('action'=>'login', 'class'=>'form login_form')); ?>
            <div class="form-group">
              <!--<input type="email" class="form-control " id="inputUsernameEmail join_input" placeholder="Email address*">-->
                <?php echo $this->Form->input('email', array('label'=>false,'class'=>'form-control','placeholder'=>'Email address*')); ?>
            </div>
            <div class="form-group">
              <!--<input type="password" class="form-control " id="inputPassword join_input" placeholder="Confirm password*">-->
                <?php echo $this->Form->input('password', array('label'=>false,'class'=>'form-control','placeholder'=>'Password*')); ?>
            </div>
          
            <!--<span>I agree to the Tearms and Conditions</span>-->
<!--            <button type="submit" class="btn btn btn-join-free"> Login </button>
          </form>-->
          <?php //echo $this->button('Submit', array('class'=>'btn btn btn-join-free')); ?>
          <?php $options = array(
    'label' => 'Login',
    'class' => 'btn btn btn-join-free'
);
echo $this->Form->end($options); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="login_form_bg" id="pop_form_mask" style="display: block;"> </div>
</div>


<!---   Forget  --->

<div class="modal fade" id="forget-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog forget">
    <div class="modal-content">
      <div class="row">
        <div class="co-md-12 col-lg-12 main login-form ">
          <h3>Forget Password</h3>
       <form class="form login_form">
            <div class="form-group">
              <input type="email" class="form-control " id="inputUsernameEmail join_input" placeholder="Email address*">
            </div>
            
          </form>
          <form class="chek">
            <input type="checkbox">
            <span>I agree to the Tearms and Conditions</span>
            <button type="submit" class="btn btn btn-join-free"> Submit </button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="login_form_bg" id="pop_form_mask" style="display: block;"> </div>
</div>
