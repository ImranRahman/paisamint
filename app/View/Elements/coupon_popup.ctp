<?php ?>
<div class="modal fade" id="couponModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog get">
        <div class="modal-content">
            <div class="right_close">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="get_deal_pop">
                    <div class="top-pop-up">
                        <div class="top_pop">
                            <h4>Here is your coupon code</h4>
                        </div>
                        <div class="coupon-code-container">
                            <div class="coupon-code inline-block">100COUPOND</div>
                            <div class="btn btn-primary inline-block copy-code" data-clipboard-text="100COUPOND">Copy</div>
                        </div>
                        <div class="coupon-instruction">Go to <a target="_blank" href="#" class="external">eBay</a> and Avail this Offer</div>
                        <!--                        <div class="share-coupon">
                                                    <div class="inline-block new_inline"><p>Share this offer</p></div>
                                                    <div class="sharing inline-block">
                                                        <div class="sms"></div>
                                                        <div class="email"></div>
                        
                                                    </div>
                                                </div>-->
                    </div>
                    <div class="popop-offer">
                        <div class="inline-block coupon-title">
                            <h4>Exclusive: Upto 70% + Extra 15% Off</h4></div>
                        <div class="coupon-description">CouponDunia Exclusive: Upto 70% + extra 15% off sitewide. Not valid on feeding, nursing and diapering products. Maximum discount of Rs. 600 per order.</div>
                        <div class="separated-data">
<!--                            <div class="voting inline-block no-border ">
                                <div class="voteup " data-value="1"></div>
                                <div class="votedown " data-value="0"></div>
                            </div>-->
                            <!--<div class="coupon-success inline-block">85% Success</div>-->
                            <div class="coupon-verified inline-block ">Verified Today</div>
                            <div class="coupon-end inline-block no-border">Ends: 15th Jun</div>
                        </div>
                        <!--<div class="view-all"><a href="http://www.coupondunia.in/babyoye">View all Babyoye offers</a></div>-->

                    </div>

                </div>
            </div>
        </div>        


    </div>
</div>