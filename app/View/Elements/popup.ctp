<?php 
if($this->params->query){
    $refrral = isset($this->params->query['r'])?$this->params->query['r']:''; 
}else{
    $refrral = '';
}
?>
<style>
    .error{ color:red; }
    .success {
        background: #c3e2e2 none repeat scroll 0 0;
        border: 1px solid green;
        border-radius: 4px;
        color: green;
        margin: 5px;
        padding: 5px;
    }
    .fa.fa-spinner.fa-pulse {
        font-size: 35px;
        text-align: center;
    }
    .loader3, .loader1, .loader2{ display: none; text-align: center;}
    button.close{ padding: 0 20px;}
    .join{ padding-bottom:0; padding-top:0; text-align:left; }
    .login-form{ border-right: none;}
    .why_signup{ width: 100%;}
</style>

<!---Join-->
<div class="modal fade" id="join-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog forget">
        <div class="modal-content">
            <div class="right_close">
                <div class="row">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <div class="co-md-12 col-lg-12 main login-form ">
                        <h3>Signup to Paisamint</h3>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 share"> <a href="<?php echo $this->Html->url(array("controller" => "users", "action" => "social_login", "Facebook")); ?>" class="btn btn-lg btn-primary btn-block new-btn"><i class=" fa fa-facebook"></i>Sign With Facebook</a> </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 share"> <a href="<?php echo $this->Html->url(array("controller" => "users", "action" => "social_login", "Google")); ?>" class="btn btn-lg btn-info btn-block new-btn googel"><i class="fa fa-google-plus"></i>Sign With Google</a> </div>
                        </div>
                        <div class="login-or">
                            <hr class="hr-or">
                            <h4> Create New One </h4>
                            <span class="span-or">or</span> </div>
                        <div id="singSuccess" class="success" style="display:none;"></div>
                        <!--<form class="form join_form">-->
                        
                        <?php echo $this->Form->create('User', array('action'=>'register', 'class'=>'form login_form')); ?>
                        <?php echo $this->Form->hidden('referral',array('value'=>$refrral)); ?>
                        <div class="form-group">                          
                        <?php echo $this->Form->input('name', array('label'=>false,'class'=>'form-control','placeholder'=>'Full Name*')); ?>
                        </div>
                        <div class="form-group">
                          <?php echo $this->Form->input('email', array('label'=>false,'class'=>'form-control','placeholder'=>'Email address*')); ?>
                        </div>
                        <div class="form-group">
                          <?php echo $this->Form->input('mob', array('label'=>false,'class'=>'form-control','placeholder'=>'Mobile Number*')); ?>
                        </div>
                        <div class="form-group">
                          <?php echo $this->Form->input('password', array('label'=>false,'class'=>'form-control','placeholder'=>'password*')); ?>
                        </div>
                        <div class="form-group">
                          <?php echo $this->Form->input('confpassword', array('label'=>false,'class'=>'form-control','placeholder'=>'Confirm password*', 'type'=>'password')); ?>
                        </div>
  
                        <div id="UserTerm">
                        <?php echo $this->Form->checkbox('term', array('hiddenField' => false)); ?>
                            <span>I agree to the Tearms and Conditions</span>
                        </div>
                        
                        <?php 
                        $options = array(
                            'label' => 'Join Free Now',
                            'class' => 'btn btn btn-join-free',
                            'id' => 'signupBtn'
                        );
                        echo $this->Form->end($options); 
                        ?>
                        <div class="loader2"><i class="fa fa-spinner fa-pulse"></i></div>
                    </div>
<!--                    <div class="co-md-6 col-lg-6 join1">
                        <div class="row1">
                            <h3>Join India’s Top Cashback Website.</h3>
                            <div class="why_signup">
                                <ul>
                                    <li><i class=" fa fa-caret-right"></i> Why Signup?</li>
                                    <li><i class=" fa fa-caret-right"></i>Personalize your experience</li>
                                    <li><i class=" fa fa-caret-right"></i>Get access to cashback offers</li>
                                    <li><i class=" fa fa-caret-right"></i>Get alerts on trending discount coupons</li>
                                    <li><i class=" fa fa-caret-right"></i>Favourite your brands</li>
                                    <li><i class=" fa fa-caret-right"></i>Get access to contests and promotions</li>
                                    <li><i class=" fa fa-caret-right"></i>Set newsletter preferences</li>
                                </ul>
                                <h6>Just Shop through us.Save your Money.
                                    Happy Saving ! Happy Shopping..!!!</h6>
                                </p>
                                <h5>Save Money on TOP Brands:</h5>
                                <div class="brand-follow"> 
                                    <a href="#"><img src="img/flipkart.png"></a> 
                                    <a href="#"><img src="img/mantra.png"></a>
                                    <a href="#"><img src="img/yep.png"></a> 
                                </div>
                            </div>
                        </div>
                    </div>-->
                </div>
            </div>
        </div>
        <div class="login_form_bg" id="pop_form_mask" style="display: block;"> </div>
    </div>
</div>


<!--Login-->

<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog forget">
        <div class="modal-content">
            <div class="row">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <div class="co-md-12 col-lg-12 main login-form ">
                    <h3>Signin to Paisamint</h3>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 share"> <a href="<?php echo $this->Html->url(array("controller" => "users", "action" => "social_login", "Facebook")); ?>" class="btn btn-lg btn-primary btn-block new-btn"><i class=" fa fa-facebook"></i>Sign With Facebook</a> </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 share"> <a href="<?php echo $this->Html->url(array("controller" => "users", "action" => "social_login", "Google")); ?>" class="btn btn-lg btn-info btn-block new-btn googel"><i class="fa fa-google-plus"></i>Sign With Google</a> </div>
                    </div>
                    <div class="login-or">
                        <hr class="hr-or">
                        <h4>  </h4>
                        <span class="span-or">or</span> </div>
                        <?php echo $this->Form->create('User', array('action'=>'login', 'class'=>'form login_form')); ?>
                    <div class="form-group">
                      <?php echo $this->Form->input('email', array('label'=>false,'class'=>'form-control','placeholder'=>'Email address*')); ?>
                    </div>
                    <div class="form-group">
                      <?php echo $this->Form->input('password', array('label'=>false,'class'=>'form-control','placeholder'=>'Password*')); ?>
                    </div>

                    <span>
                        <a href="#" id="forgetPassword" data-toggle="modal" data-target="#forget-modal"> <i class="fa  fa-user"></i>Forget Password </a>
                    </span>
                    
                    <div id="loginError" class="error"></div>
          
                    <?php 
                    $options = array(
                        'label' => 'Login',
                        'class' => 'btn btn btn-join-free',
                        'id' => 'loginBtn'
                    );
                    echo $this->Form->end($options); 
                    ?>
                    <div class="loader1"><i class="fa fa-spinner fa-pulse"></i></div>
                </div>
            </div>
        </div>
    </div>
    <div class="login_form_bg" id="pop_form_mask" style="display: block;"> </div>
</div>


<!--Forget-->

<div class="modal fade" id="forget-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog forget">
        <div class="modal-content">
            <div class="row">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <div class="co-md-12 col-lg-12 main login-form ">
                    <h3>Forget Password</h3>
                    <!--<form class="form login_form">-->
                    <?php echo $this->Form->create('User', array('action'=>'forgetPassword', 'class'=>'form login_form')); ?>
                    <div class="form-group">
                        <?php echo $this->Form->input('email', array('label'=>false,'class'=>'form-control','placeholder'=>'Email address*', 'id'=>'forgetUserEmail')); ?>
                    </div>
                    <div class="" id="forgetError"></div>
                    <!--<button type="submit" class="btn btn btn-join-free"> Submit </button>-->
                    <?php $options = array('label' => 'Submit', 'class' => 'btn btn btn-join-free', 'id' => 'forgetBtn');
                    echo $this->Form->end($options); ?>
                    <div class="loader3"><i class="fa fa-spinner fa-pulse"></i></div>

                </div>
            </div>
        </div>
    </div>
    <div class="login_form_bg" id="pop_form_mask" style="display: block;"> </div>
</div>

<script>
    //###### LOGIN #########
    $(document).ready(function () {
        $('#modal-launcher').click(function () {
            $("#loginError").html('');
        });
        
        $('#loginBtn').click(function (e) {
            $(this).attr('disabled', 'disabled');
            $("#loginError").html('');
            var form = $('#UserLoginForm').serialize();
            $.ajax({
                type: "POST",
                url: "<?php echo BASE_URL;?>users/ajaxLogin",
                data: form,
                beforeSend: function (xy) {
                    $('.loader1').show();
                },
                success: function (res) {
                    $('.loader1').hide();
                    $('#loginBtn').removeAttr('disabled');
                    console.log(res);
                    if (res != 'Done') {
                        $("#loginError").html(res);
                    } else {
                        document.location.href = "<?php echo BASE_URL;?>";
                    }
                }
            });
            e.preventDefault();
            return false;
        });
    });
</script>

<script>
    //####### SIGN UP #########
    $(document).ready(function () {
        $('#modal-launcher-signup').click(function () {
            $("#UserRegisterForm").find(".error").remove();
            $(".success").html('');
            $('#singSuccess').hide();
            $('.form-control').val('');
        });

        $('#signupBtn').click(function (e) {
            $(this).attr('disabled', 'disabled');
            $("#UserRegisterForm").find(".error").remove();
            $(".success").html('');
            $('#singSuccess').hide();
            var form = $('#UserRegisterForm').serialize();
            $.ajax({
                type: "POST",
                url: "<?php echo BASE_URL;?>users/ajaxRegister",
                data: form,
                beforeSend: function (xy) {
                    $('.loader2').show();
                },
                success: function (res) {
                    $('#signupBtn').removeAttr('disabled');
                    $('.loader2').hide();
                    if (res != 'Done') {
                        var data = $.parseJSON(res);
                        $.each(data, function (i, item) {
                            //console.log(i+' '+item);
                            var field = toTitleCase(i);
                            //console.log(field+' '+item);
                            $("#User" + field).after('<span class="error">' + item + '</span>');
                        });

                    } else {
                        $('#singSuccess').html('<p>Congratulations!</p><p>You have Register successfull.</p>');
                        $('#singSuccess').show();
                        setTimeout(function(){
                            document.location.href="<?php echo BASE_URL;?>";
                        },2000);
                    }
                }
            });
            e.preventDefault();
            return false;
        });
    });

    function toTitleCase(str)
    {
        return str.replace(/\w\S*/g, function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    }
</script>

<script>

    $(document).ready(function () {
        $("#forgetPassword").click(function () {
//       $("#login-modal").hide();
            $('#forgetError').html('');
            $('.modal-backdrop').trigger('click');
        });

        $('#forgetBtn').click(function (e) {
            $('#forgetError').html('').removeClass();
            $(this).attr('disabled', 'disabled');
            var email = $('#forgetUserEmail').val();
            $.ajax({
                type: "POST",
                url: "<?php echo BASE_URL;?>users/forgetPassword",
                data: {'email': email},
                beforeSend: function (xy) {
                    $('.loader3').show();
                },
                success: function (res) {
                    $('.loader3').hide();
                    $('#forgetBtn').removeAttr('disabled');
                    //console.log(res);
                    if (res != 'Done') {
                        $("#forgetError").html(res).addClass('error');
                    } else {
                        $("#forgetError").html('An email is sent to your email address.<br> Please check and follow steps given.').addClass('success');
                    }
                }
            });
            e.preventDefault();
            return false;
        });
    });
</script>
