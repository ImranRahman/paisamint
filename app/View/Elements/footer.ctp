<?php ?>
<footer>
  <div class="container">
    <div class="row row-wrap">
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 box_1">
        <h4>About Us</h4>
        <ul class="list list-footer">
          <li> <a href="<?php echo BASE_URL;?>pages/who-we-are">Who we are ?</a> </li>
        </ul>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 box_1">
        <h4>Exclusive menu</h4>
        <ul class="list list-footer">
          <li><a href="<?php echo BASE_URL;?>pages/exclusive-coupon">Exclusive Coupons</a> </li>
          <li><a href="<?php echo BASE_URL;?>pages/top20-coupon">Top20 Coupons</a> </li>
          <li><a href="<?php echo BASE_URL;?>retailers">Store</a> </li>
          <?php 
          $cheklogged = $this->Session->read('user_data');
          if(empty($cheklogged)){
          ?>          
          <li><a href="#" onclick="$('#modal-launcher-signup').trigger('click'); return false;">Sign Up</a></li>
          <?php } ?>
          <li><a href="<?php echo BASE_URL;?>pages/coupon-offer"> Coupon Offer </a> </li>
        </ul>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 box_1">
        <h4>Help</h4>
        <ul class="list list-footer">
          <li><a href="<?php echo BASE_URL;?>pages/faqs">FAQ</a> </li>
          <li><a href="<?php echo BASE_URL;?>pages/contact-us">Contact Us</a> </li>
          <li><a href="<?php echo BASE_URL;?>pages/payment-options">Payment Options </a> </li>
          <li><a href="<?php echo BASE_URL;?>pages/how-it-works">How to use coupon?</a> </li>
        </ul>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 box_1">
        <h4>Social</h4>
        <div class=" social_icon">
          <ul class="list list-horizontal list-space">
            <li> <a href="https://www.facebook.com/"> <i class=" fa fa-facebook"></i><span> Join us on Facebook</span></a> </li>
            <li> <a href="https://twitter.com/"> <i class=" fa fa-twitter"></i> <span>Join us on Twitter </span></a></li>
            <li> <a href="https://plus.google.com/"> <i class=" fa fa-google-plus"></i> <span>Join us on Google+</span></a> </li>
          </ul>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="boottom_f">
        <p> All codes are selected by our editors and are valid at time of publication. ‘Exclusive’ coupon codes cannot be published elsewhere without written permission from <?php echo SITE_NAME;?>
          Copyright © <?php echo date('Y');?></p>
      </div>
    </div>
  </div>
 
</footer>
