<?php

$controller = $this->params['controller']; //echo $controller;
$action = $this->params['action']; //echo $action;
if($controller == 'homes' && $action == 'index'){
    $fixedClass = 'navbar-fixed-top add_bg';  
}else{
    $fixedClass = '';
}

?>
<style>
    ul.dropdown-menu li { padding: 0;}    
    .navbar-brand { height: auto; padding: 0px 0px 15px 15px;}
    ul.nav.navbar-nav{ font-size: 1em;}
    i.fa { padding: 0 5px; }
</style>
<header>


    <nav class="navbar navbar-default navbar-fixed-top add_bg">
        <div class="container-fluid container">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!--<a class="navbar-brand" href="#">Brand</a>-->
                <?php 
                  echo $this->Html->image("logo.jpg", array(
                      "alt" => "Logo Here",
                      'url' => array('controller' => '/', 'action' => 'index'),
                      'class'=>'navbar-brand'
                  ));
                ?>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo BASE_URL;?>retailers"><i class="fa fa-shopping-cart"></i>STORES</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i>CATEGORIES<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <?php foreach($catesMenu as $cates){?>
                            <li><a href="<?php echo BASE_URL.'categories/'.$this->SeoUrl->Seourl($cates['Category']['name']);?>"><?php echo ucwords($cates['Category']['name']);?></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                    <?php $logged = $this->Session->read('user_data');
                    if(!empty($logged)){ ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i>ACCOUNT<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo BASE_URL;?>users/account" > 
                                    <i class=" fa fa-suitcase"></i>My Account</a></a>
                            </li>
                            <li>
                                <a href="<?php echo BASE_URL;?>users/logout" > 
                                    <i class="fa fa-key"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                    <?php }else{ ?>

                    <li><a href="#" id="modal-launcher" data-toggle="modal" data-target="#login-modal"> <i class="fa  fa-user"></i>
                            SIGN IN
                        </a></li>
                    <li>
                        <button id='modal-launcher-signup' class="btn btn-join" data-target="#join-modal" data-toggle="modal">JOIN US</button></li>
                    <?php } ?>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->

    </nav>

</header>

<?php echo $this->element('popup'); ?>
