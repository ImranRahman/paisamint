<?php ?>
<style>
    .navbar-brand { height: auto; padding: 0px;}
</style>
<div class="container header">
    <nav class="navbar navbar-default">
        <div class="container-fluid col-md-12 col-xs-12">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header col-md-3 col-xs-12 col-sm-12">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!--<a class="navbar-brand" href="#">Brand</a>-->
                <?php  
                echo $this->Html->image("logo.jpg", array(
                    "alt" => "Logo Here",
                    'url' => array('controller' => '/', 'action' => 'index'),
                    'class'=>'navbar-brand'
                ));
                ?>
            </div>

            <div class="col-md-6 col-xs-12 col-sm-12">    
            <form  method="post" action="#" class="navbar-form navbar-left" role="search">
                <div class="input-group main_search form-group">
                    <input type="text" name="search" class="form-control input-lg serch_box srch-form" placeholder="Search Coupons (eg. Flipkart,etc...)" autocomplete="off">
                    <span class="input-group-btn">
                        <button class="btn btn-lg new_search_btn searchBtn" type="button"> 
                            <i class="fa fa-search"></i> 
                        </button>
                    </span>                        
                </div>
                <ul class="serach-result-ul">

                </ul>
            </form>
            </div>
            
            <div class="col-md-3 col-xs-12 col-sm-12">
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">

                <ul class="nav navbar-nav navbar-right">
                    <?php $logged = $this->Session->read('user_data'); if(!empty($logged)){ ?>
                    <li class="dropdown new_drop123">
                        <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-user"></i>
                            <span class="username">Account</span>
                            <b class="caret new_creat"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <li><a href="<?php echo BASE_URL;?>users/account">
                                    <i class=" fa fa-suitcase"></i> My Account</a></li>                                
                            <li><a href="<?php echo BASE_URL;?>users/logout">
                                    <i class="fa fa-key"></i> Log Out</a></li>
                        </ul>
                    </li>
                        <?php }else{ ?>
                    <li class="sign_btn">
                        <a href="#" id="modal-launcher" data-toggle="modal" data-target="#login-modal"> <i class="fa fa-user"></i>SIGN IN</a> 
                    </li>
                    <li class="join_btn">
                        <button id='modal-launcher-signup' class="btn btn-join" data-target="#join-modal" data-toggle="modal">JOIN US</button>
                    </li>
                        <?php } ?> 
                </ul>
            </div><!-- /.navbar-collapse -->
            </div>
        </div><!-- /.container-fluid -->
    </nav>
</div>

<?php echo $this->element('popup'); ?>
