<style>
    ul.dropdown-menu li {
        padding: 0;
    }
    ul.cate-dropdown li a {color: #1bbc9b; font-size:16px;}
</style>
<div class="co-lg-12 col-md-12 main_nav">
    <ul class="container">
        <li><a href="<?php echo BASE_URL;?>"><i class="fa fa-home"> </i><span>Home</span></a></li>
        <li><a href="<?php echo BASE_URL;?>retailers"><i class="fa fa-shopping-cart"> </i><span>STORES</span></a></li>
        <li class="dropdown">
            <a href="#" data-toggle="dropdown"><i class="fa  fa-bars"></i><span>CATEGORIES 
                <i class="caret"></i></span>
            </a>    
            <ul class="dropdown-menu cate-dropdown">
                <?php foreach($catesMenu as $cates){?>
                <li><a href="<?php echo BASE_URL.'categories/'.$this->SeoUrl->Seourl($cates['Category']['name']);?>"><?php echo ucwords($cates['Category']['name']);?></a></li>
                <?php } ?>
            </ul>                                    
        </li>
    </ul>
</div>
