<?php ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>PaisaMint</title>

        <style>
            *{margin:0; padding:0px; font-family:"open Sans"; font-weight:300;}

            .tb{width:700px; margin:auto; padding:24px;}
            .tb h2{text-align:right; font-family:"open Sans"; font-weight:300;}
            .hi{margin-top:15px;}
            .hi h1{margin-top:29px}
            .mytxt{margin-top:25px;}
            .mytxt ul{  margin: 15px 56px;}

            a.get {
                background: none repeat scroll 0 0 #1bbc9b;
                border-radius: 4px;
                color: #fff;
                height: 35px;
                margin: 0 0 20px 10px;
                padding: 8px;
                width: 140px;
            }
        </style>
    </head>

    <body>

        <table class="tb" width="700">
            <tr>
                <td width="300" align="left">
                    <a href="<?php echo BASE_URL;?>">
                        <img src="<?php echo BASE_URL;?>app/webroot/images/logo.jpg" alt="<?php echo SITE_NAME;?>" />
                    </a>
                </td>
                <td width="700" align="right"> 
                    <h2></h2>
                </td>
            </tr>
        </table>

        <table class="tb" width="700">
            <tr>
                <td class="hi"><h1>Hi <?php echo $toName;?></h1></td>
            </tr>
            <tr>
                <td>
                    <?php echo $fromName;?> has invited you to join <a href="<?php echo BASE_URL;?>"><?php echo SITE_NAME;?></a>
                </td>
            </tr>

            <tr><td>&nbsp;</td></tr>

            <tr>
                <td>
                    To Join now 
                    <a href="<?php echo BASE_URL.'?r='.$code.'#register'; ?>" style="background: #1bbc9b none repeat scroll 0 0;
    border-radius: 4px;
    color: white;
    font-size: 15px;
    font-weight: bold;
    padding: 7px 12px;
    text-decoration: none;">Accept</a>
                </td>
            </tr>

            <tr><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>

            <tr><td>With best regard,</td></tr>
            <tr>
                <td>
                    Team <a href="<?php echo BASE_URL;?>"><?php echo SITE_NAME;?></a>
                </td>
            </tr>

        </table>

    </body>
</html>
