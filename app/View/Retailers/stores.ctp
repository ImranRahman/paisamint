<?php ?>

<style>
    
    *{margin:0; padding:0; font-family:"open Sans";}
    .stores_ot h2{color:#f3ab00; font-size: 2em;}
    .stores_ot{width:80%; margin:auto;}
    /*.stores_search{margin-top:30px;}*/
    .form-control1{border-radius:0;}
    .store_img{text-align:center; position:relative; }
    .stores-bg{padding: 17px;
               margin-top: 19px;
               border: 1px solid#F2F1F1;}
    .img-stor{ 
        overflow: hidden;
        margin: 15px 0px;   padding: 26px 0px;
        border: 1px solid#E0E0E0;
        min-height:131px;
    }
    .img-stor:hover{/*background:rgba(0, 0, 0, .5);*/ cursor:pointer;
        border:1px solid#f3ab00;
        width: 100%;
        height: 100%;
    }
    
    .img-stor{ position:relative; }
    .title-hover {
        display: none;
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
        background:rgba(19, 185, 126, 0.8);

        padding:15px;
    }
    .img-stor:hover .title-hover {
        display:block;
    }
    .title-hover > h2{color:#fff; font-size: 2em;}
    .title-hover > p{
        color: #fff;
        font-size: 2em;
        /*margin-top: -12px;*/
    }
</style>
<div class="container" style="margin-top:45px;">
    <div class="row">    

        <div class="stores_ot clearfix">
            <div class="col-md-6">
                <h2>All Stores</h2>
            </div>
            <div class="col-md-6 stores_search">
                <div class=" table-filter">
                    <form class="form-inline" role="form">

                        <div class="form-group">
                            <input type="text" class="form-control form-control1" id="storesshows" placeholder="Search">
                        </div>

                        <div class="form-group">
                            <select class="form-control form-control1" id="categories" name="categories">
                                <option value="0">All Stores</option>
                                <?php 
                                foreach($catesMenu as $cates){ ?>
                                    <option value="<?php echo BASE_URL.'categories/'.$this->SeoUrl->Seourl($cates['Category']['name']);?>"><?php echo ucwords($cates['Category']['name']);?></option>
                                <?php }
                                ?>
                            </select>
                        </div>

                    </form>
                </div>
            </div>    
        </div>
        <div class="stores-bg clearfix">
            <?php foreach($retailers as $retailer){ //pr($retailer); ?>
            <div class="col-md-3 store_img ">
                <div class="img-stor">
                    <!--<img src="1433312297_amazon.jpg" />-->
                    <?php
                    if($retailer['Retailer']['logo']!='' && file_exists(RETAILER_THUMB.$retailer['Retailer']['logo'])){
                    echo $this->Html->image(RETAILER_THUMB_URL.$retailer['Retailer']['logo'], array('alt'=>'Logo'));
                    } else{
                        echo ucwords($retailer['Retailer']['name']);
                    }
                    ?>
                    <a href="<?php echo BASE_URL.'retailers/'.$this->SeoUrl->seourl($retailer['Retailer']['name']);?>" >
                        <div class="title-hover">                        
                            <h2><?php echo $retailer['Coupon'];?></h2>
                            <p>Coupons</p>                        
                        </div>
                    </a>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>         
</div>

