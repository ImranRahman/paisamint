<?php //pr($coupons);?>
<section class="slider-banner">
    <div class="co-md-12 col-lg-12">
        <div class="container">

            <ul class="banner-text">
                <li style="font-size: 14px;">
                    <?php echo $this->Html->link("Home", array('controller'=>'/','action'=>'index'));?> 
                    <span><i class="fa  fa-caret-right"></i> 
                        <?php echo $paramName;?> 
                    </span>
                </li>
                <li class="bold-text"><h2><?php echo $paramName;?> Coupons, Deals & Cash Back</h2>
                    <p>
                        <?php echo $retailer['Retailer']['description'];?>
                    </p>
                    <!--<span>Special Terms and Exclusions +</span>-->
                </li>
            </ul>
            <div class="mid-logo">
                <!--<img src="/images/cat/amzoan.jpg">-->
                <?php echo $this->Html->image(RETAILER_MID_THUMB_URL.$retailer['Retailer']['logo']); ?>
            </div>

        </div>
    </div>
    <div class="clearfix"></div>
</section>


<section class="payment-box">
    <div class="container">
        <div class="col-md-4 col-lg-4 col-xs-12 dilivery-option">
            <div class="left_bus">
                <!--<img src="images/cat/truck.png">-->
                <?php echo $this->Html->image("/images/cat/truck.png"); ?>
                <a href="#">Delivery Option</a>
            </div>
            <div class="clearfix"></div>

            <div class="offer">
                <ul>
                    <li><h2>5</h2></li>
                    <li><h3>%</h3><h4>Cash back</h4> 
                    <span><a href="#">See All Offer</a></span>
                    </li>
                </ul>

            </div>
            <button class="btn btn-sign-join" type="submit">Sign in or join to get cashback</button>
        </div>
        <div class="col-md-4 col-lg-4 col-xs-12 paymen_dat" style="line-height: 0.84;">
            <div class="payment_date">
                <h4>Estimated date for payment</h4>
                <!--<img src="images/cat/speed.png">-->
                <?php echo $this->Html->image("/images/cat/speed.png"); ?>
            </div>
            <ul class="trak">
                <li><h5>12<sup>th</sup>July</h5><span>Estimated payment</span></li>
                <li class="traking"><h5>95%</h5><span>Tracking reliability</span></li>
            </ul>


            <div class="clearfix"></div>
            <button class="btn btn-sign-join second" type="submit">See all cashback stats</button>
        </div>


        <div class="col-md-4 col-lg-4 col-xs-12 paymen_dat" style="line-height: 1.1875;">
            <div class="payment_date">
                <h4 style="color:#1bbc9b">Important things to know</h4>
                <ul class="payment-method">
                    <li><a href="#"><i class="fa fa-caret-right"></i>New customers only</a></li>
                    <li><a href="#"><i class="fa fa-caret-right"></i>Cashback only eligible on Paisa-mint <br>vertised vouchers.</a></li>
                    <li><a href="#"><i class="fa fa-caret-right"></i>Cashback is excluded on:<br> products/categories</a></li>
                    <li><a href="#"><i class="fa fa-caret-right"></i>Cashback not paid on VAT</a></li>
                    <li><a href="#"><i class="fa fa-caret-right"></i>Cashback paid</a></li>
                </ul>
                <button class="btn btn-sign-join second" type="submit">See all terms & conditions</button>
            </div>
        </div>

    </div>

</section>


<section class="retailer_left">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-3 col-xs-12 left_retailer">
                <h4>Similar Retailer</h4>
                <ul>
                    <?php foreach($relatedRetailers as $relRetailer){ ?>
                    <li>
                        <a href="<?php echo BASE_URL;?>retailers/<?php echo $this->SeoUrl->seoUrl($relRetailer['Retailer']['name']);?>"><i class="fa fa-caret-right"></i>
                            <span> <?php echo $relRetailer['Retailer']['name'];?></span>
                        </a>                        
                    </li>
                    <?php } ?>
                </ul>
            </div>

            <div class="col-lg-9 col-md-9 col-xs-12 voucher_offer">
                <h3>Voucher codes & offers</h3>
                <div class="clearfix"></div>
                <?php //pr($coupons); ?>
                <?php 
                foreach($coupons as $coupon){ 
                $cid = $coupon['Coupon']['id'];  
                    ?>
                <div class="offer_voucher col-lg-12 col-md-12 col-xs-12 ">
                    <div class="col-md-2 col-lg-2 col-xs-12 earn">
                        <p>Earn</p>
                        <div class="cl"></div>
                        <h4><?php echo $coupon['Coupon']['cashback'];?>%</h4>
                        <button type="button" class=" btn-cashback">Cashback</button>

                    </div>
                    <div class="col-md-8 col-lg-8 col-xs-12 ofer">
                        <p>Verified <?php //echo $coupon['Coupon']['created'];
                        echo CakeTime::timeAgoInWords($coupon['Coupon']['created'], array(
                            'accuracy' => array('month' => 'month'),
                            'end' => '1 year'
                        ));
//                        $now = time(); // or your date as well
//                        $your_date = strtotime($coupon['Coupon']['created']);
//                        $datediff = $now - $your_date;
//                        $expiry = floor($datediff/(60*60*24));
//                        $day = ($expiry > 1)?' days':' day'; 
                        //echo $expiry.$day;
                        ?>   •  16 People Used Today</p>
                        <h4><?php echo $coupon['Coupon']['title'];?></h4>
                        <h6><?php echo $this->SeoUrl->wordWrap($coupon['Coupon']['description'],100); if(strlen($coupon['Coupon']['description']) >100) { ?><a href="#">More[+]</a><?php } ?> </h6>
                        <h5>                            
                            <?php echo $this->Html->image('/images/cat/time.png');?>
                            
                            Expires in 
                            <?php 
                                //echo $coupon['Coupon']['expire']; 
                                $now = time(); // or your date as well
                                $your_date = strtotime($coupon['Coupon']['expire']);
                                $datediff = $your_date - $now;
                                $expiry = ceil($datediff/(60*60*24));
                                $day = ($expiry > 1)?' days':' day'; 
                                if($expiry == 0){
                                    $expiry = 'today'; $day='';
                                }
                                echo $expiry.$day; 
                            ?>
                        </h5>

                    </div>

                    <div class="col-md-2 col-lg-2 col-xs-12 btn-cash-get">
                        <button class=" btn btn-get-cash pull-right" type="button" onclick="couponPopup(<?php echo $cid;?>);">Get Code</button>
                    </div>

                </div>
                <?php } ?>

            </div>
        </div>
    </div>
</section>

<?php echo $this->element('coupon_popup'); ?>