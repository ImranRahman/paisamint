<?php ?>
<style>
    div.hor_1 img {
        width: 142px;
        height: 75px;
        border: 1px solid #c1c1c1;
    }
    .offer_box a img {
        padding: 0;
        border: 1px solid #c1c1c1;        
        margin: 5px 0 15px 10px;
    }
    #myCarousel1 .col-md-2{ max-height: 340px;}
</style>
<section class="banner">
    <div class="main_slider">
        <div class="container">
            <div class="row" >
                <div class="col-md-12  col-sm-12 col-xs-12 main-title"> 
                    <!-- Indicators -->

                    <div class=" col-lg-12 col-md-12 col-sm-12 banner_heading">
                        <h1>Search for Discounts,<span> ends here</span></h1>
                        <p>Save more,Shop more</p>
                    </div>

                    <!-- Left and right controls -->

                    <div class="col-md-12  col-sm-12 col-xs-12 search">
                        <form class="my_search_bar search-form-home" method="post" action="#">
                            <div class="input-group main_search">
                                <input name="search" type="text" class="form-control mysearch srch-form" placeholder="search for your favourite merchant, product or category..." autocomplete="off">
                                <span class="input-group-btn search-bar">
                                    <button class="btn find searchBtn" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span> 
                            </div>
                            <ul class="serach-result-ul">
                                <li>fsdfs fsdfdsf ds</li>
                                <li>ff34434234</li>
                                <li>fsdfs fsdfdsf ds</li>
                                <li>ff34434234</li>
                                <li>fsdfs fsdfdsf ds</li>
                                <li>ff34434234</li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--=========================== Retailer ==================-->
<?php
$icons = array(
    'fa-mobile-phone',
    'fa-user',
    'fa-plane',
    'fa-spoon',
);
?>
<section class="tab">
    <div class="container">
        <div id="parentHorizontalTab">
            <div class="retailer">
                <h3>Shop at all your favourite retailers</h3>
                <ul class="resp-tabs-list hor_1">
                    <li><a href="javascript:void(0)"><i class=" fa fa-hand-o-right"></i> Top Online Stores</a></li>
                    <?php $c=0; foreach($categories as $category){ 
                        echo '<li>';
                        echo '<a href="javascript:void(0)"><i class="fa '.$icons[$c].' "> </i> '.$category['Category']['name'].'</a>';
                        echo '</li>';
                    $c++; }
                    ?>
                </ul>
                <div class="resp-tabs-container hor_1">
                    <div>
                        <p> 
                            <!--vertical Tabs-->

                        <div id="ChildVerticalTab_1">
                            <ul>
                                <?php //echo count($retailers);
                                foreach($retailers as $retailer){ ?>
                                <li>
                                    <?php 
                                    if($retailer['Retailer']['logo']!='' && file_exists(RETAILER_THUMB.$retailer['Retailer']['logo'])){
                                        echo $this->Html->image(RETAILER_THUMB_URL.$retailer['Retailer']['logo'], array("alt" => "Logo Here", 'url' => array('controller' => 'retailers', 'action' => '/', $this->SeoUrl->seoUrl($retailer['Retailer']['name']))));
                                    }else{
                                        echo $this->Html->image('noimage.jpg', array("alt" => "Logo Here", 'url' => array('controller' => 'retailers', 'action' => '/', $this->SeoUrl->seoUrl($retailer['Retailer']['name']))));
                                    }
                                    ?>
                                </li>
                                <?php } //echo $this->element('sql_dump');?>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                        </p>
                    </div>

                    <?php foreach($retsCate as $retCate) { ?>
                    <div>
                        <ul class="ChildVerticalTab">
                        <?php 
                        foreach($retCate as $ret){
                            echo '<li>';
                            if($ret['Retailer']['logo']!='' && file_exists(RETAILER_THUMB.$ret['Retailer']['logo'])){
                                echo $this->Html->image(RETAILER_THUMB_URL.$ret['Retailer']['logo'], array("alt" => "Logo Here", 'url' => array('controller' => 'retailers', 'action' => '/', $this->SeoUrl->seoUrl($ret['Retailer']['name']))));
                            }else{
                                echo $this->Html->image('noimage.jpg', array("alt" => "Logo Here", 'url' => array('controller' => 'retailers', 'action' => '/', $this->SeoUrl->seoUrl($ret['Retailer']['name']))));
                            }
                            echo '</li>';
                        }
                           //pr($retCate);                    

                        ?>
                        <?php //echo $this->element('sql_dump'); ?>
                        </ul>    
                    </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</section>

<!--=========================== why paisa mint ==================-->
<section class="whypaisa">
    <div class="container">
        <div class="row" >
            <div class=" col-sm-12 col-xs-12 col-lg-12 why_paisa">
                <h2> Why Paisamint?</h2>
                <ul class="free_offer">
                    <li class="col-md-2 col-xs-4 "> <img src="images/1.png" alt="100%"><a href="#"><span>100% Free to Join<span> Paisamint</span></span></a></li>
                    <li class="col-md-2 col-xs-4 "> <img src="images/2.png" alt="100%"><a href="#"><span>100% Free to Join<span> Paisamint</span></span></a></li>
                    <li class="col-md-2 col-xs-4 "> <img src="images/3.png" alt="100%"><a href="#"><span>100% Free to Join<span> Paisamint</span></span></a></li>
                    <li class="col-md-2 col-xs-4 "> <img src="images/4.png" alt="100%"><a href="#"><span>100% Free to Join<span> Paisamint</span></span></a></li>
                    <li class="col-md-2 col-xs-4 "> <img src="images/5.png" alt="100%"><a href="#"><span>100% Free to Join<span> Paisamint</span></span></a></li>
                    <li class="col-md-2 col-xs-4 "> <img src="images/6.png" alt="100%"><a href="#"><span>100% Free to Join<span> Paisamint</span></span></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!--=========================== Coupon Section ==================-->
<section class="Latest_Deals">
    <div class="container">
        <div class="row">
            <div class="latest">
                <div class="box_heading">
                    <h3>Latest Deals & Coupons <span><i class="fa fa-shopping-cart"></i></span></h3>
                </div>
                <?php                      
                    //echo $this->element('sql_dump'); 
                ?>
                <div class="carousel slide"  id="myCarousel1">
                    <div class="carousel-inner">
                        <div class="item active">
                            <?php
                            //echo count($coupons);
                            foreach($coupons as $coupon){
                                $couponID = $coupon['Coupon']['id'];
                            ?>
                            <div class="col-md-2 col-xs-3  offer_box"> 
                                <div class="icon-info">
                                    <i class="fa fa-info-circle new"></i>
                                </div>
                                <?php 
                                if($coupon['Retailer']['logo']!='' && file_exists(RETAILER_THUMB.$coupon['Retailer']['logo'])){
                                    echo $this->Html->image(RETAILER_THUMB_URL.$coupon['Retailer']['logo'], array("alt" => "Logo Here",
        'url' => array('controller' => 'retailers', 'action' => '/', $this->SeoUrl->seoUrl($coupon['Retailer']['name']))));
                                }else{
                                    echo $this->Html->image('noimage.jpg', array("alt" => "Logo Here",
        'url' => array('controller' => 'retailers', 'action' => '/', $this->SeoUrl->seoUrl($coupon['Retailer']['name']))));
                                }
                                ?>

                                <p style="line-height:1.35;"><?php echo $coupon['Coupon']['title']; ?></p>
                                <?php                                 
                                    $now = time(); // or your date as well
                                    $your_date = strtotime($coupon['Coupon']['expire']);
                                    $datediff = $your_date - $now;
                                    $expiry = ceil($datediff/(60*60*24));
                                    $day = ($expiry > 1)?' days':' day';
                                    if($expiry == 0){
                                        $expiry = 'today'; $day='';
                                    }
                                ?>
                                <span><i class="fa fa-clock-o"></i>
                                    Expires in <?php //echo $coupon['Coupon']['expire'];
                                    echo $expiry.$day;                                    
                                    //echo $intval->format('%d days');
                                    ?>
                                </span>
                                <h5 style="line-height:1.25; height: 70px;">+ Cashback upto <br><span style="font-size:24px; color: #08c; font-weight: bold; font-family: arial;"><?php echo $coupon['Coupon']['cashback'].'%';?></span></h5>
                                
                                <button type="button" onclick="couponPopup(<?php echo $couponID;?>);" class="get">Get this Deal</button>
                            </div>
                            <?php } ?>
                        </div>

                    </div>
                    <div class="more_button">
                        <button id="getMoreCoupon" type="button" class="btn btn-more" value="10">More Hot Deals</button>
                        <div id="loader-coupon" style="display: none;"><i class="fa fa-spinner fa-pulse"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="newsletter">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="sub-warper pull-left">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="sub-icon sub-one"> <i class="fa fa-envelope-o"></i> </div>
                        <h3>Subscribe</h3>
                        <p>Never miss exciting coupons, stay up to date</p>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 sub-line">
                        <div class="sub-icon sub-two"> <i class="fa fa-heart"></i> </div>
                        <h3>Personalize Store</h3>
                        <p>Save Your Favourite Store</p>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 sub-line">
                        <div class="sub-icon sub-three"> <i class="fa fa-star"></i> </div>
                        <h3>Save Coupon</h3>
                        <p>Save Coupon For Later Use</p>
                    </div>
                </div>
                <div class=" col-md-9  col-xs-12 news-search">

                    <?php echo $this->Form->create('Home',array('action'=>'subscribe')); ?>
                    <div class="input-group input-group-lg">

                    <?php echo $this->Form->input('email', array('label'=>false,'class'=>'form-control sub','name'=>'EMAIL','id'=>'mce-EMAIL','placeholder'=>'Subscribe here','required'=>'false', 'div'=>false)); ?>

                        <span class="input-group-btn">
                        <?php 
                            $options = array(
                                'label' => 'Subscribe!',
                                'class' => 'btn btn-sub',
                                'id' => 'mc-embedded-subscribe',
                                'name'=>'subscribe',
                                'div'=>false
                            );
                        echo $this->Form->end($options); 
                        ?>

                        </span> 
                    </div>
                    <div class="" id="emailError"></div>
                    <div id="subscribe-loader"><?php echo $this->Html->image('spinner.gif');?></div>
                    <!-- /col-6--> 
                </div>
            </div>
            <!-- /row --> 
            <!-- /container --> 
        </div>
    </div>
</section>

<?php echo $this->element('coupon_popup'); ?>




<style>
    .rmt{ text-align:center; font-weight: bold; margin-top:15px; display: inline-block; width: 100%}
</style>

<script>
    $(document).ready(function () {
        $('#getMoreCoupon').click(function () {
            var val = $(this).val();
            $('.rmt').remove();
            $.ajax({
                type: "POST",
                url: "<?php echo BASE_URL;?>homes/moreCoupon",
                data: {'limit': val},
                beforeSend: function (x) {
                    $('#loader-coupon').show();
                },
                success: function (res) {
                    $('#loader-coupon').hide();
                    if (res == 'No Found') {
                        $('.carousel-inner').append('<div class="error rmt">No More Coupons!</div>');
                    } else {
                        $('.item').append(res);
                        $('#getMoreCoupon').val((eval(val) + 20));
                    }
                }
            });
        });
    });

</script>

<script>
    $(document).ready(function () {
        $('#subscribe-loader').hide();
        $('#mc-embedded-subscribe').click(function (e) {
            var email = $('#mce-EMAIL').val();
            $('#emailError').html('').removeClass();

            $.ajax({
                type: "POST",
                url: "<?php echo BASE_URL;?>homes/subscribe",
                data: {'email': email},
                beforeSend: function (xhr) {
                    $('#subscribe-loader').show();
                },
                success: function (res) {
                    $('#subscribe-loader').hide();
                    if (res == 'You are already Subscribed') {
                        $('#emailError').html(res).removeClass().addClass('alert alert-success');
                    } else if (res != 'Done') {
                        $('#emailError').html(res).removeClass().addClass('alert alert-danger');
                    } else {
                        $('#emailError').html('<strong>Congratulations!</strong><br> You are subscribed our newsletter successfully. Please Check your Email!').removeClass().addClass('alert alert-success');
                    }
                }
            });

            return false;

        });
    });
</script>
