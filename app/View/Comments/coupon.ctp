<?php

//pr($coupons);?>
<section class="slider-banner">
    <div class="co-md-12 col-lg-12">
        <div class="container">

            <ul class="banner-text">
                <li style="font-size: 14px;">
                    <?php echo $this->Html->link("Home", array('controller'=>'/','action'=>'index'));?> 
                    <span><i class="fa  fa-caret-right"></i> 
                        <?php echo $paramName;?> 
                    </span>
                </li>
                <li class="bold-text"><h2><?php echo $paramName;?> Coupons, Deals & Cash Back</h2>
                    <p>
                        <?php echo $retailer['Retailer']['description'];?>
                    </p>
                    <!--<span>Special Terms and Exclusions +</span>-->
                </li>
            </ul>
            <div class="mid-logo">
                <!--<img src="/images/cat/amzoan.jpg">-->
                <?php echo $this->Html->image(RETAILER_MID_THUMB_URL.$retailer['Retailer']['logo']); ?>
            </div>

        </div>
    </div>
    <div class="clearfix"></div>
</section>




<section class="retailer_left">
    <div class="container">
        <div class="row">

            <div class="col-lg-12 col-md-12 col-xs-12 voucher_offer">
                <!--<h3>Voucher codes & offers</h3>-->
                <div class="clearfix"></div>
                <?php //pr($coupons); ?>
                <?php 
                 
                $cid = $coupons['Coupon']['id'];  
                    ?>
                <div class="offer_voucher col-lg-12 col-md-12 col-xs-12 ">
                    <div class="col-md-2 col-lg-2 col-xs-12 earn">
                        <p>Earn</p>
                        <div class="cl"></div>
                        <h4><?php echo $coupons['Coupon']['cashback'];?>%</h4>
                        <button type="button" class=" btn-cashback" style="width: 100%;">Cashback</button>

                    </div>
                    <div class="col-md-8 col-lg-8 col-xs-12 ofer">
                        <p>Verified 
                            <?php 
                            //echo $coupon['Coupon']['created'];
                            echo CakeTime::timeAgoInWords($coupons['Coupon']['created'], array(
                                'accuracy' => array('month' => 'month'),
                                'end' => '1 year'
                            ));                        
                            ?>   •  16 People Used Today</p>
                        <h4><?php echo $coupons['Coupon']['title'];?></h4>
                        <h6>
                            <?php echo $this->SeoUrl->wordWrap($coupons['Coupon']['description'],100); 
                            if(strlen($coupons['Coupon']['description']) >100) { ?>
                            <a href="#">More[+]</a>
                            <?php } ?> 
                        </h6>
                        <h5>                            
                            <?php echo $this->Html->image('/images/cat/time.png');?>

                            Expires in 
                            <?php 
                                //echo $coupon['Coupon']['expire']; 
                                $now = time(); // or your date as well
                                $your_date = strtotime($coupons['Coupon']['expire']);
                                $datediff = $your_date - $now;
                                $expiry = ceil($datediff/(60*60*24));
                                $day = ($expiry > 1)?' days':' day'; 
                                if($expiry == 0){
                                    $expiry = 'today'; $day='';
                                }
                                echo $expiry.$day; 
                            ?>
                        </h5>

                    </div>

                    <div class="col-md-2 col-lg-2 col-xs-12 btn-cash-get">
                        <button class=" btn btn-get-cash pull-right" type="button" onclick="couponPopup(<?php echo $cid;?>);">Get Code</button>
                    </div>

                </div>


            </div>




        </div>
    </div>
</section>


<section class="retailer_left">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 offer_voucher2" style="width: 97.3%; margin-left: 15px;">
                <div class="col-md-8 col-md-offset-2">
                    
                <?php echo $this->Session->flash();?>    
                <?php foreach ($comments as $comment){ //pr($comment); ?>
                    <div class="col-md-12 offer_voucher" style="padding: 15px;">

                        <h2 style="margin-bottom: 15px; font-size:18px; font-weight: bold;">
                            <i class="fa fa-user"></i>
                            <?php echo ucwords($comment['User']['name']);?>,
                            on 
                            <?php echo date('F d Y', strtotime($comment['CouponComment']['created']));?>
                        </h2>
                        <p style=" line-height: 1.45; padding-left: 15px;">
                            <?php echo $comment['CouponComment']['comment'];?>
                        </p>
                    </div>


                <?php } ?>

                    <div class="col-md-12 offer_voucher" id="commnet-div" style="padding: 15px; display: none;">
                        <?php 
                        echo $this->Form->create('CouponComment', array('class'=>'form login_form'));
                        echo $this->Form->hidden('user_id', array('value'=>$this->Session->read('user_data.id')));
                        ?>
                        <div class="form-group">
                            <label for="comment">Comment</label>
                        <?php echo $this->Form->input('comment', array('label'=>false,'class'=>'form-control')); ?>
                        </div>
                        <div class="form-group">
                        <?php $options = array('class'=>'btn btn-get-cash'); ?>
                        </div>
                        <div class="form-group">
                          <?php  $options = array(
                            'label' => 'Post Comment',
                            'class' => ' btn btn-primary',
                            'id' => 'post-comment',
                              'div'=>false
                        );
                         echo $this->form->end($options); ?>
                            <button type="button" class="btn btn-danger" id="comm-cancel">Cancel</button>   
                        </div>

                    </div>
                    <p>
                        <button class=" btn btn-get-cash" id="your-comment" style="margin-top: 30px;">Post Your Comment</button>

                    </p>

<!--                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" style="margin-top: 15px;">
                        <tr>
                            <td width="100%">
                                <?php echo $this->paginator->first("<<"); ?> &nbsp;&nbsp;
                                <?php if($this->paginator->hasPrev()){ echo $this->paginator->prev('Previous'); } ?> &nbsp;&nbsp;
                                <?php echo $this->paginator->numbers(array('separator'=>'&nbsp; &nbsp;')); ?> &nbsp;&nbsp;
                                <?php if($this->paginator->hasNext()){ echo $this->paginator->next('Next'); } ?>&nbsp;&nbsp;			
                                <?php echo $this->paginator->last(">>"); ?>            
                            </td>
                        </tr>
                    </table>-->
                </div>
            </div> 

        </div>
    </div>
</section>


<?php echo $this->element('coupon_popup'); ?>


<script>
    $(document).ready(function () {
        $('#your-comment').click(function () {
            var user_sess = $('#user_session').val();
            if (user_sess) {
                $('#commnet-div').show();
                $('#your-comment').hide();
            } else {
                $('#modal-launcher').click();
            }
        });

        $('#post-comment').click(function () {
            $('.comm-err').remove();
            if ($('#CouponCommentComment').val() == '') {
                $('#CouponCommentComment').after('<span class="error comm-err">Please enter your comment!</span>');
                return false;
            }
        });

        $('#comm-cancel').click(function () {
            $('#CouponCommentComment').val('');
            $('#commnet-div').hide();
            $('#your-comment').show();
        });
    });
</script>