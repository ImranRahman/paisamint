<?php ?>
<style>
    *{font-family:"open Sans";}
    .cont-info{width:100%; margin:auto; }

    .span4.sidebar {
        padding: 15px;
        border: 1px solid#e7e7e7;

        background: #f6f6f6;
    }

    .btn-primary1 {
        color: #fff;
        background-color: #1bbc9b;
        border-color: #1bbc7b;
    }
    .my_contact h2{color:#ff9800;}
    .form-control{ text-align: left;}
</style>
<section>
    <div class="container">
        <div class="bg-text">
            <h3 style="width: auto; font-size: 24px;">Contact Us</h3>
        </div>    
        <div class="col-lg-12 col-md-12 elec_cont" style="margin-top: 40px; line-height: 2; min-height: 400px;">
            <div class="container cont-info">


                <div class="col-md-7 my_contact">

                    <form class="form-horizontal" role="form" method="post" action="index.php">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="name" name="name" placeholder="name" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="email" name="email" placeholder="email" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="subject" class="col-sm-2 control-label">Subject</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="subject" name="subject" placeholder="subject" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="message" class="col-sm-2 control-label">Message</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="4" name="message" placeholder="comments"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-10 col-sm-offset-2">
                                <input id="submit" name="submit" type="submit" value="Send" class="btn btn-primary1">
                            </div>
                        </div>

                    </form>
                </div>
                <div class="col-md-5">
                    <div class="span4 sidebar">

                        <div class="sidebox">
                            <h3 class="sidebox-title">Contact Information</h3>
                            <p>
                            </p><address><strong>Your Company, Inc.</strong><br>
                                Address<br>
                                City, State, Zip<br>
                                <abbr title="Phone">P:</abbr> (123) 456-7890</address> 
                            <address>  <strong>Email</strong><br>
                                <a href="mailto:#">first.last@gmail.com</a></address>  
                            <p></p>     


                        </div>



                    </div>


                </div>
            </div>
        </div>
    </div>
</section>
