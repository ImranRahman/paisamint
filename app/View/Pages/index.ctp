<?php ?>
<section class="work-bg" style="height: auto;">
    <div class="container">
        <div class="bg-text">
            <h3 style="width: auto;"><?php echo $pageTitle;?></h3>
        </div>    
        <div class="col-md-12" style="margin-top: 40px; line-height: 2; min-height: 300px;">
            <?php
            if(!empty($content)){
                echo nl2br($content['StaticPage']['content']);
            }else{
                echo '<p class="alert alert-warning">Page Not Found!</p>';
            }
            
            ?>
            
            <?php //echo $this->element('sql_dump');?>
        </div>
    </div>
</section>
