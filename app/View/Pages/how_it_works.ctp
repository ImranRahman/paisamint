<section class="work-bg">
    <div class="container">
        <div class="bg-text">
            <h3>Brands you know.</h3>
            <h2>Rewards you'll love...</h2>
            <p>Paisamint is the smart, simple and  secure way to earn 
                cashback every time you shop.</p>
            
            <?php $logged = $this->Session->read('user_data');
            if(empty($logged)){ ?>            
            <a href="#" class="btn-member">Become a Member</a>
            <h4>Already a member? <span class="btn-login">Login</span></h4>
            <?php } ?>
        </div>
    </div>
</section>

<section class="shoping-1">
    <div class="container">
        <div class="row">
            <div class="col-md-4 shoping-icon"><?php echo $this->Html->image('/images/shoping.png');?></div>
            <div class="col-md-8 shoping-text"><p><span class="no">1</span><span class="shoping-hd">Shop</span>
                </p>
                <h4>Browse and shop at your favorite stores</h4>
                <h5>
                    Using the codes below, text the retailers that you visit most often, and they will send you mobile coupons when they have them available. Most of the shops listed will text you about once a week, although some (such as Kohl's) send texts more often, and others (like Office Max) send texts sporadically.</h5>
            </div>
            <!--<div class="col-md-2">
            <ul>
            <li class="fa fa-circle"></li>
            <li class="fa fa-circle-o"></li>
            <li class="fa fa-circle-o"></li>
            </ul>
            
            </div>-->
        </div>
    </div>

</section>

<section class="rupee">
    <div class="container">
        <div class="row">
            <div class="col-md-8 cashback-c"><div class="case-head"><span class="no2">2</span><span class="cashback">Earn Cashback</span></div>
                <h4>Cashback gets added,Cashback gets Confirmed</h4>
                <h5>
                    Using the codes below, text the retailers that you visit most often, and they will send you mobile coupons when they have them available. Most of the shops listed will text you about once a week, although some (such as Kohl's) send texts more often, and others (like Office Max) send texts sporadically.</h5>
            </div>
            <div class="col-md-4 case-icon"><?php echo $this->Html->image('/images/rupee.png');?></div>

        </div>
    </div>

</section>


<section class="transfer">
    <div class="container">
        <div class="row">
            <div class="col-md-4 transfer-icon"><?php echo $this->Html->image('/images/hand.png');?></div>
            <div class="col-md-8 transfer-text"><p><span class="no3">3</span><span class="transfer-hd">Transfer Cashback  
                    </span>
                </p>
                <h4>NOW CASHBACK GETS ADDED TO YOUR CASHKARO ACCOUNT</h4>
                <h5>
                    Using the codes below, text the retailers that you visit most often, and they will send you mobile coupons when they have them available. Most of the shops listed will text you about once a week, although some (such as Kohl's) send texts more often, and others (like Office Max) send texts sporadically.</h5>
            </div>
        </div>
    </div>
</section>

<section class="slider-bg">
    <div class="container">
        <div class="find"><h2><?php echo $this->Html->image('/images/search.png');?>Find Over 4000’ Retailer</h2>
        </div>



        <div id="myCarousel" class="carousel slide outer-slide" data-ride="carousel">
            <!-- Indicators -->

            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>


            <!-- Wrapper for slides -->
            <div class="carousel-inner my-slid" role="listbox">
                <div class="item active">
                    <div class="col-lg-2">
                        <?php echo $this->Html->image('/images/amazon.png', array('alt'=>'amazon','width'=>133,'height'=>62));?>
                        <!--<img src="images/amazon.png" alt="Chania" width="133" height="62">-->
                    </div>
                    <div class="col-lg-2">
                        <!--<img src="images/amazon.png" alt="Chania" width="133" height="62">-->
                        <?php echo $this->Html->image('/images/amazon.png', array('alt'=>'amazon','width'=>133,'height'=>62));?>
                    </div>
                    <div class="col-lg-2">
                        <!--<img src="images/amazon.png" alt="Chania" width="133" height="62">-->
                        <?php echo $this->Html->image('/images/amazon.png', array('alt'=>'amazon','width'=>133,'height'=>62));?>
                    </div>
                    <div class="col-lg-2">
                    <?php echo $this->Html->image('/images/amazon.png', array('alt'=>'amazon','width'=>133,'height'=>62));?>    
                    </div>
                   <div class="col-lg-2">
                    <?php echo $this->Html->image('/images/amazon.png', array('alt'=>'amazon','width'=>133,'height'=>62));?>    
                    </div>
                    <div class="col-lg-2">
                    <?php echo $this->Html->image('/images/amazon.png', array('alt'=>'amazon','width'=>133,'height'=>62));?>    
                    </div>
                </div>

                <div class="item">
                    <div class="col-md-2">                        
                        <?php echo $this->Html->image('/images/sanpdeal1.jpg', array('alt'=>'Chania','width'=>133,'height'=>62));?>
                    </div>
                    <div class="col-md-2">                        
                        <?php echo $this->Html->image('/images/sanpdeal1.jpg', array('alt'=>'Chania','width'=>133,'height'=>62));?>
                    </div>
                    <div class="col-md-2">                        
                        <?php echo $this->Html->image('/images/sanpdeal1.jpg', array('alt'=>'Chania','width'=>133,'height'=>62));?>
                    </div>
                    <div class="col-md-2">                        
                        <?php echo $this->Html->image('/images/sanpdeal1.jpg', array('alt'=>'Chania','width'=>133,'height'=>62));?>
                    </div>
                    <div class="col-md-2">                        
                        <?php echo $this->Html->image('/images/sanpdeal1.jpg', array('alt'=>'Chania','width'=>133,'height'=>62));?>
                    </div>
                    <div class="col-md-2">                        
                        <?php echo $this->Html->image('/images/sanpdeal1.jpg', array('alt'=>'Chania','width'=>133,'height'=>62));?>
                    </div>
                </div>

                <div class="item">
                    <div class="col-md-2">                        
                        <?php echo $this->Html->image('/images/ebay.png', array('alt'=>'Chania','width'=>133,'height'=>62));?>
                    </div>
                    <div class="col-md-2">                        
                        <?php echo $this->Html->image('/images/ebay.png', array('alt'=>'Chania','width'=>133,'height'=>62));?>
                    </div>
                    <div class="col-md-2">                        
                        <?php echo $this->Html->image('/images/ebay.png', array('alt'=>'Chania','width'=>133,'height'=>62));?>
                    </div>
                    <div class="col-md-2">                        
                        <?php echo $this->Html->image('/images/ebay.png', array('alt'=>'Chania','width'=>133,'height'=>62));?>
                    </div>
                    <div class="col-md-2">                        
                        <?php echo $this->Html->image('/images/ebay.png', array('alt'=>'Chania','width'=>133,'height'=>62));?>
                    </div>
                    <div class="col-md-2">                        
                        <?php echo $this->Html->image('/images/ebay.png', array('alt'=>'Chania','width'=>133,'height'=>62));?>
                    </div>
                </div>

                <div class="item">
                    <div class="col-md-2">                        
                        <?php echo $this->Html->image('/images/flipkart.png', array('alt'=>'Chania','width'=>133,'height'=>62));?>
                    </div>
                    <div class="col-md-2">                        
                        <?php echo $this->Html->image('/images/flipkart.png', array('alt'=>'Chania','width'=>133,'height'=>62));?>
                    </div>
                    <div class="col-md-2">                        
                        <?php echo $this->Html->image('/images/flipkart.png', array('alt'=>'Chania','width'=>133,'height'=>62));?>
                    </div>
                    <div class="col-md-2">                        
                        <?php echo $this->Html->image('/images/flipkart.png', array('alt'=>'Chania','width'=>133,'height'=>62));?>
                    </div>
                    <div class="col-md-2">                        
                        <?php echo $this->Html->image('/images/flipkart.png', array('alt'=>'Chania','width'=>133,'height'=>62));?>
                    </div>
                    <div class="col-md-2">                        
                        <?php echo $this->Html->image('/images/flipkart.png', array('alt'=>'Chania','width'=>133,'height'=>62));?>
                    </div>
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control slid" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left sd" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right sd" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

    </div>
</section>


<section class="newsletter">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="sub-warper pull-left">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="sub-icon sub-one"> <i class="fa fa-envelope-o"></i> </div>
                        <h3>Subscribe</h3>
                        <p>Never miss Exitings coupons stay up to date</p>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 sub-line">
                        <div class="sub-icon sub-two"> <i class="fa fa-heart"></i> </div>
                        <h3>Personalize Store</h3>
                        <p>Save Your Favourite Store</p>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 sub-line">
                        <div class="sub-icon sub-three"> <i class="fa fa-star"></i> </div>
                        <h3>Save Coupon</h3>
                        <p>Save Coupon For Later Use</p>
                    </div>
                </div>
                <div class=" col-md-9  col-xs-12 news-search">
                    <form>
                        <div class="input-group input-group-lg">
                            <input type="email" name="EMAIL" class="form-control sub" id="mce-EMAIL" placeholder="Subscribe here">
                            <span class="input-group-btn">
                                <button type="submit" name="subscribe" id="mc-embedded-subscribe" class="btn btn-sub">Subscribe!</button>
                            </span> </div>
                        <div class="cl"></div>
                    </form>
                    <!-- /col-6--> 
                </div>
            </div>
            <!-- /row --> 
            <!-- /container --> 
        </div>
    </div>
</section>