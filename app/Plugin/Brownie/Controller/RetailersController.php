<?php

class RetailersController extends BrownieAppController {

    public $components = array('Paginator','SimpleImage');
    public $helpers = array('Brownie.i18n');
    public $Model;
    public $uses = array('Brownie.Content');

    public function beforeFilter() {
        parent::beforeFilter();

        $model = 'Retailer';
        $this->Model = ClassRegistry::init($model);
        $this->loadModel($model);

        $this->loadModel('Category');
        $this->loadModel('RetailerType');

        $this->set('model', $model);
    }

    public function index() {

        $this->set('categories', $this->Category->find('all', array(
                    'fields' => array('Category.id', 'Category.name')
        )));

        $this->set('RetailerTypes', $this->RetailerType->find('all'));

        $joins = array(
            array(
                'table' => 'categories',
                'alias' => 'Category',
                'conditions' => array('Category.id = Retailer.category_id'),
                'type' => 'LEFT'
            ),
            array(
                'table' => 'retailer_types',
                'alias' => 'RetailerType',
                'conditions' => array('RetailerType.id = Retailer.retailer_type_id'),
                'type' => 'LEFT'
            ),
        );

        $subcategories = $this->paginate = array(
            'fields' => array('Category.id', 'Category.name', 'RetailerType.name', 'Retailer.*'),
            'joins' => $joins,
            'order' => array('Retailer.id' => 'desc'),
            'conditions'=>array('Retailer.isDeleted'=>'0')
        );

        $data = $this->paginate($this->Model); //pr($data);die;
        $this->set('datas', $data);
    }

    // ########## ADD #######################
    public function add() {
        $this->set('categories', $this->Category->find('all', array(
                    'fields' => array('Category.id', 'Category.name')
        )));

        $this->set('RetailerTypes', $this->RetailerType->find('all'));

        $Model = $this->Model->name; //echo ($Model);die; 
        if ($this->request->is('post')) {
            //pr($this->request->data);  die;
                                      
            $this->request->data['Retailer']['logo'] = $this->request->data['Retailer']['logo_image']['name'];
            $categories = $this->request->data['Retailer']['category_id'];
            unset($this->request->data['Retailer']['category_id']);
          
            /*
             * ....Retailer url parametes
             */
            $Params = $this->request->data['RetailerParam'];
//            pr($retailerParams['name']); die;
           
            
            if ($this->Retailer->save($this->request->data)) {
                
                $lastId = $this->Retailer->getLastInsertID();
                
                $fileName = $this->SimpleImage->upload($this->request->data['Retailer']['logo_image'], RETAILER_IMG);
                if($fileName != 'error'){
                    /*
                     * ..Creating midThumb
                     */              
                    $this->SimpleImage->load(RETAILER_IMG.$fileName);
                    $this->SimpleImage->resize(200,160);
                    $this->SimpleImage->save(RETAILER_MID_THUMB.$fileName);
                    /*
                     * ..Creating Thumb
                     */              
                    $this->SimpleImage->load(RETAILER_MID_THUMB.$fileName);
                    $this->SimpleImage->resize(143,75);
                    $this->SimpleImage->save(RETAILER_THUMB.$fileName);
                    
                    $this->Retailer->query("UPDATE retailers SET logo = '$fileName' WHERE id = '$lastId'");
                }
                
                /*
                 * ... Inserting Category
                 */
                $this->loadModel('RetailerCategory');
                $cateArray = array();
                foreach($categories as $category){
                    $this->RetailerCategory->create();
                    $cateArray['RetailerCategory']['retailer_id'] = $lastId;
                    $cateArray['RetailerCategory']['category_id'] = $category;
                    $this->RetailerCategory->save($cateArray);                
                }
                
                /*
                 * .... Inserting url parameters
                 */
                $this->loadModel('RetailerParam');
                $len = count($Params['name']); 
                $paramArray = array();
                for($i=0; $i<$len; $i++){
                    echo $Params['name'][$i];
                    if(!empty($Params['name'][$i])){
                        $this->RetailerParam->create();
                        $paramArray['RetailerParam']['retailer_id'] = $lastId;
                        $paramArray['RetailerParam']['name'] = $Params['name'][$i];
                        $paramArray['RetailerParam']['value'] = $Params['value'][$i];
                        $this->RetailerParam->save($paramArray);  
                    }
                }

                $msg = ($this->Model->brwConfig['names']['gender'] == 1) ?
                        __d('brownie', 'The %s has been saved [male]', __($this->Model->brwConfig['names']['singular'])) :
                        __d('brownie', 'The %s has been saved [female]', __($this->Model->brwConfig['names']['singular']));

                $this->Session->setFlash($msg, 'flash_success');

                if (!empty($this->request->data[$this->Model->name]['after_save'])) {
                    $this->_afterSaveRedirect();
                }
            }

            $msg = ($this->Model->brwConfig['names']['gender'] == 1) ?
                    __d('brownie', 'The %s could not be saved. Please, check the error messages.[male]', __($this->Model->brwConfig['names']['singular'])) :
                    __d('brownie', 'The %s could not be saved. Please, check the error messages.[female]', __($this->Model->brwConfig['names']['singular']));

            $this->Session->setFlash($msg, 'flash_error');
        }
    }
    
    
    //############ EDIT ###############
    public function edit($id = null) {
        $this->loadModel('RetailerCategory');
        
        $this->set('categories', $this->Category->find('all', array(
                    'fields' => array('Category.id', 'Category.name')
        )));

        $this->set('RetailerTypes', $this->RetailerType->find('all'));
        
        $this->set('retailerCates', $this->RetailerCategory->find('all', array(
            'fields'=>array('RetailerCategory.retailer_id','RetailerCategory.category_id'),
            'conditions'=>array('RetailerCategory.retailer_id'=>$id),
        )));

        $data = $this->Retailer->find('first', array(
            'conditions' => array('Retailer.id' => $id,'Retailer.isDeleted'=>0),
        ));
        
        $Model = $this->Model->name; //echo ($Model);die; 
        
        
        
        if ($this->request->is(array('post', 'put'))) {
            //pr($this->request->data);  die; 
                      
            $categories = $this->request->data[$Model]['category_id'];
                //pr($categories);die;
            if(!empty($categories)){  
                unset($this->request->data['Retailer']['category_id']); 
                $this->RetailerCategory->deleteAll(array('RetailerCategory.retailer_id'=>$id));
                foreach($categories as $category){
                    $this->RetailerCategory->create();
                    $cateArray['RetailerCategory']['retailer_id'] = $id;
                    $cateArray['RetailerCategory']['category_id'] = $category;
                    $this->RetailerCategory->save($cateArray);                
                }
            }
            
            /*
            * .... Inserting url parameters
            */
            if(isset($this->request->data['RetailerParam'])){
                $Params = $this->request->data['RetailerParam'];
            
                $this->loadModel('RetailerParam');
                $this->RetailerParam->deleteAll(array('RetailerParam.retailer_id'=>$id));
                $len = count($Params['name']); 
                $paramArray = array();
                for($i=0; $i<$len; $i++){
                    echo $Params['name'][$i];
                    if(!empty($Params['name'][$i])){
                        $this->RetailerParam->create();
                        $paramArray['RetailerParam']['retailer_id'] = $id;
                        $paramArray['RetailerParam']['name'] = $Params['name'][$i];
                        $paramArray['RetailerParam']['value'] = $Params['value'][$i];
                        $this->RetailerParam->save($paramArray);  
                    }
                }
            }
                
            $this->Retailer->id = $id;
            
            if ($this->Retailer->save($this->request->data)) {
                
                if($this->request->data['Retailer']['logo_image'] != ''){
                    $fileName = $this->SimpleImage->upload($this->request->data['Retailer']['logo_image'], RETAILER_IMG);
                    if($fileName != 'error'){
                        
                        $this->SimpleImage->load(RETAILER_IMG.$fileName);
                        $this->SimpleImage->resize(200,160);
                        $this->SimpleImage->save(RETAILER_MID_THUMB.$fileName);
                                      
                        $this->SimpleImage->load(RETAILER_MID_THUMB.$fileName);
                        $this->SimpleImage->resize(143,75);
                        $this->SimpleImage->save(RETAILER_THUMB.$fileName);
                        
                        @unlink(RETAILER_IMG.$data['User']['logo']);
                        @unlink(RETAILER_MID_THUMB.$data['User']['logo']);
                        @unlink(RETAILER_THUMB.$data['User']['logo']);

                        $this->Retailer->query("UPDATE retailers SET logo = '$fileName' WHERE id = '$id'");
                    }
                }
                
                

                $msg = ($this->Model->brwConfig['names']['gender'] == 1) ?
                        __d('brownie', 'The %s has been saved [male]', __($this->Model->brwConfig['names']['singular'])) :
                        __d('brownie', 'The %s has been saved [female]', __($this->Model->brwConfig['names']['singular']));

                $this->Session->setFlash($msg, 'flash_success');

                if (!empty($this->request->data[$this->Model->name]['after_save'])) {
                    $this->_afterSaveRedirect();
                }
            }

            $msg = ($this->Model->brwConfig['names']['gender'] == 1) ?
                    __d('brownie', 'The %s could not be saved. Please, check the error messages.[male]', __($this->Model->brwConfig['names']['singular'])) :
                    __d('brownie', 'The %s could not be saved. Please, check the error messages.[female]', __($this->Model->brwConfig['names']['singular']));

            $this->Session->setFlash($msg, 'flash_error');
        }

        if (!$this->request->data) {
            $this->request->data = $data;
        }
        $this->set('data',$data);
    }
    

    //########## VIEW #######################
    public function view($id = null) {
        $model = $this->Model->name;
        $this->loadModel('RetailerCategory');

        $neighbors = $this->Model->find(
                'neighbors', array('field' => 'id', 'value' => $id, 'conditions'=>array('isDeleted'=>0))
        );
        $this->set('neighbors', $neighbors);
        
        $this->set('RetailerTypes', $this->RetailerType->find('all'));
        
        $joins = array(
            array(
                'table' => 'retailer_types',
                'alias' => 'RetailerType',
                'conditions' => array('RetailerType.id = '.$model.'.retailer_type_id'),
                'type' => 'INNER'
            )
        );
                
        $data = $this->Model->find('first', array(
            'fields' => array('RetailerType.name', $model.'.*'),
            'joins' => $joins,
            'conditions' => array($model.'.id'=>$id, $model.'.isDeleted'=>0)
        ));
        $this->set('datas', $data);
        
        $join2 = array(            
            array(
                'table' => 'categories',
                'alias' => 'Category',
                'conditions' => array('Category.id = RetailerCategory.category_id'),
                'type' => 'LEFT'
            ),
        );
        
        $this->set('retailerCate', $this->RetailerCategory->find('all', array(
            'fields' => array('Category.name'),
            'conditions'=>array('RetailerCategory.retailer_id'=>$id),
            'joins'=>$join2,
        )));
    }

    

    public function delete($id=null, $model=null) {
        $record = $this->Model->findById($id);
        if (empty($record)) {
            throw new NotFoundException('Record does not exists');
        }
        $home = array('plugin' => 'brownie', 'controller' => 'brownie', 'action' => 'index', 'brw' => false);
        $redirect = $this->referer($home);
        //$deleted = $this->Content->remove($this->Model, $id);
        //die(pr($this->Model->name));
        $modelName = $this->Model->name;
        $this->Model->id = $id;
        $this->request->data[$modelName]['isDeleted'] = 1;

        //if (!$deleted) {
        if (!$this->Model->save($this->request->data)) {
            $this->Session->setFlash(__d('brownie', 'Unable to delete'), 'flash_error');
            $this->redirect($redirect);
        } else {
            $this->Session->setFlash(__d('brownie', 'Successful delete'), 'flash_success');
            $afterDelete = empty($this->params['named']['after_delete']) ? null : $this->params['named']['after_delete'];
            if ($afterDelete == 'parent') {
                $parentModel = $this->Model->brwConfig['parent'];
                if (!$parentModel) {
                    $afterDelete = 'index';
                } else {
                    $foreignKey = $this->Model->belongsTo[$parentModel]['foreignKey'];
                    $redirect = array(
                        'plugin' => 'brownie', 'controller' => 'contents',
                        'action' => 'view', $parentModel, $record[$model][$foreignKey]
                    );
                }
            }
            if ($afterDelete == 'index') {
                if ($this->Model->brwConfig['actions']['index']) {
                    $redirect = array(
                        'plugin' => 'brownie', 'controller' => 'contents',
                        'action' => 'index', $model
                    );
                } else {
                    $redirect = $home;
                }
            }
            $this->redirect($redirect);
        }
    }

    public function delete_multiple($model = null) {
        $model = $this->Model->name;
        $plural = $this->Model->brwConfig['names']['plural'];
        if (empty($this->request->data[$model]['id'])) {
            $msg = __d('brownie', 'No %s selected to delete', $plural);
            $this->Session->setFlash($msg, 'flash_notice');
        } else {
            $deleted = $no_deleted = 0;
            $dataArray = $this->request->data[$model]['id'];
            foreach ($dataArray as $id) {

                $this->Model->id = $id;
                $this->request->data[$model]['isDeleted'] = 1;

                if ($this->Model->save($this->request->data)) {
                    //if ($this->Content->remove($this->Model, $id)) {
                    $deleted++;
                } else {
                    $no_deleted++;
                }
            }
            $msg_deleted = $msg_no_deleted = '';
            if ($deleted) {
                $msg_deleted = __d('brownie', '%d %s deleted.', $deleted, $plural) . ' ';
            }
            if ($no_deleted) {
                $msg_no_deleted = __d('brownie', '%d %s no deleted.', $no_deleted, $plural) . ' ';
            }

            if ($deleted) {
                if ($no_deleted)
                    $flashStatus = 'flash_notice';
                else
                    $flashStatus = 'flash_success';
            } else {
                $flashStatus = 'flash_error';
            }
            $this->Session->setFlash($msg_deleted . $msg_no_deleted, $flashStatus);
        }

        $redir = env('HTTP_REFERER');
        if (empty($redir)) {
            $redir = array('action' => 'index', $model);
        }
        $this->redirect($redir);
    }

    //############ AFTER SAVE ###############
    public function _afterSaveRedirect() {
        switch ($this->request->data[$this->Model->name]['after_save']) {
            case 'referer':
                if ($this->request->data[$this->Model->name]['referer']) {
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->redirect(array('controller' => 'brownie', 'action' => 'index'));
                }
                break;
            case 'edit':
                $this->redirect(array('action' => 'edit', $this->Model->id, 'after_save' => 'edit'));
                break;
            case 'add':
                $this->redirect(array('action' => 'add', 'after_save' => 'add'));
                break;
            case 'index':
                $this->redirect(array('action' => 'index'));
                break;
            case 'parent':
                if ($parent = $this->Model->brwConfig['parent']) {
                    $foreignKey = $this->Model->belongsTo[$parent]['foreignKey'];
                    if (!empty($this->request->data[$this->Model->alias][$foreignKey])) {
                        $idRedir = $this->request->data[$this->Model->alias][$foreignKey];
                    } else {
                        $record = $this->Model->findById($this->Model->id);
                        $idRedir = $record[$this->Model->alias][$foreignKey];
                    }
                    $this->redirect(array('action' => 'view', $parent, $idRedir));
                }
                $this->redirect(array('action' => 'index'));
                break;
            case 'view':
                $this->redirect(array('action' => 'view', $this->Model->id));
                break;
            case 'home':
                $this->redirect(array('controller' => 'brownie', 'action' => 'index'));
                break;
        }
    }

}
