<?php
class CouponsController extends BrownieAppController
{
    public $components = array('Paginator');
    public $helpers = array('Brownie.i18n');
    public $Model;
    public $uses = array('Brownie.Content');

    public function beforeFilter() {
        parent::beforeFilter();

        $model = 'Coupon';
        $this->Model = ClassRegistry::init($model);
        $this->loadModel($model);

        $this->loadModel('Category');
        $this->loadModel('RetailerType');
        $this->loadModel('Retailer');

        $this->set('model', $model);
    }

    public function index() {
        
        $joins = array(
            array(
                'table' => 'retailers',
                'alias' => 'Retailer',
                'conditions' => array('Retailer.id = Coupon.retailer_id'),
                'type' => 'LEFT'
            ),            
        );

        $coupons = $this->paginate = array(
            'fields' => array('Retailer.id', 'Retailer.name','Coupon.*'),
            'joins' => $joins,
            'order' => array('Coupon.id' => 'desc'),
            'conditions'=>array('Coupon.isDeleted'=>'0')
        );

        $data = $this->paginate($this->Model); //pr($data);die;
        $this->set('datas', $data);
    }

    // ########## ADD #######################
    public function add() {
        $this->set('retailers', $this->Retailer->find('all', array(
            'fields' => array('Retailer.id', 'Retailer.name'),
            'order'=>array('Retailer.name')
        )));
        
        $Model = $this->Model->name; //echo ($Model);die; 
        if ($this->request->is('post')) {
            //pr($this->request->data);  die; 
            
            if ($this->Model->save($this->request->data)) {

                $msg = ($this->Model->brwConfig['names']['gender'] == 1) ?
                        __d('brownie', 'The %s has been saved [male]', __($this->Model->brwConfig['names']['singular'])) :
                        __d('brownie', 'The %s has been saved [female]', __($this->Model->brwConfig['names']['singular']));

                $this->Session->setFlash($msg, 'flash_success');

                if (!empty($this->request->data[$this->Model->name]['after_save'])) {
                    $this->_afterSaveRedirect();
                }
            }

            $msg = ($this->Model->brwConfig['names']['gender'] == 1) ?
                    __d('brownie', 'The %s could not be saved. Please, check the error messages.[male]', __($this->Model->brwConfig['names']['singular'])) :
                    __d('brownie', 'The %s could not be saved. Please, check the error messages.[female]', __($this->Model->brwConfig['names']['singular']));

            $this->Session->setFlash($msg, 'flash_error');
        }
    }
    
    //############ EDIT ###############
    public function edit($id = null) {
        $this->set('retailers', $this->Retailer->find('all', array(
            'fields' => array('Retailer.id', 'Retailer.name'),
            'conditions'=>array('Retailer.isDeleted'=>0),
            'order'=>array('Retailer.name'=>'asc')
        )));

        $data = $this->Model->find('first', array(
            'fields'=>array('Coupon.*','SubCategory.id,name'),
            'conditions' => array('Coupon.id' => $id,'Coupon.isDeleted'=>0),
            'joins'=>array(
                array(
                    'table'=>'sub_categories',
                    'alias'=>'SubCategory',
                    'conditions'=>array('SubCategory.id = Coupon.subCateId'),
                    'type'=>'left'
                )
            )
        ));
        
        $Model = $this->Model->name; //echo ($Model);die; 
        if ($this->request->is(array('post', 'put'))) {
            //pr($this->request->data);  die;            
            
            $this->Model->id = $id;
            if ($this->Model->save($this->request->data)) {

                $msg = ($this->Model->brwConfig['names']['gender'] == 1) ?
                        __d('brownie', 'The %s has been saved [male]', __($this->Model->brwConfig['names']['singular'])) :
                        __d('brownie', 'The %s has been saved [female]', __($this->Model->brwConfig['names']['singular']));

                $this->Session->setFlash($msg, 'flash_success');

                if (!empty($this->request->data[$this->Model->name]['after_save'])) {
                    $this->_afterSaveRedirect();
                }
            }

            $msg = ($this->Model->brwConfig['names']['gender'] == 1) ?
                    __d('brownie', 'The %s could not be saved. Please, check the error messages.[male]', __($this->Model->brwConfig['names']['singular'])) :
                    __d('brownie', 'The %s could not be saved. Please, check the error messages.[female]', __($this->Model->brwConfig['names']['singular']));

            $this->Session->setFlash($msg, 'flash_error');
        }

        if (!$this->request->data) {
            $this->request->data = $data;
        }
        $this->set('data',$data);
    }

    //########## VIEW #######################
    public function view($id = null) {
        $model = $this->Model->name;

        $neighbors = $this->Model->find(
                'neighbors', array('field' => 'id', 'value' => $id, 'conditions'=>array('isDeleted'=>0))
        );
        $this->set('neighbors', $neighbors);
        
                
        $joins = array(
            array(
                'table' => 'retailers',
                'alias' => 'Retailer',
                'conditions' => array('Retailer.id = Coupon.retailer_id'),
                'type' => 'LEFT'
            ),
             array(
                    'table'=>'sub_categories',
                    'alias'=>'SubCategory',
                    'conditions'=>array('SubCategory.id = Coupon.subCateId'),
                    'type'=>'left'
                )
        );
        
        $data = $this->Model->find('first', array(
            'fields' => array('Retailer.name', $model.'.*','SubCategory.name'),
            'joins' => $joins,
            'conditions' => array($model.'.id'=>$id, $model.'.isDeleted'=>0)
        ));
        $this->set('datas', $data);
    }

    

    public function delete($id=null, $model=null) {
        $model = $this->Model->name; 
        $record = $this->Model->findById($id);
        if (empty($record)) {
            throw new NotFoundException('Record does not exists');
        }
        $home = array('plugin' => 'brownie', 'controller' => 'brownie', 'action' => 'index', 'brw' => false);
        $redirect = $this->referer($home);
        //$deleted = $this->Content->remove($this->Model, $id);
        //die(pr($this->Model->name));
        
        $this->Model->id = $id;
        $this->request->data[$model]['isDeleted'] = 1;

        //if (!$deleted) {
        if (!$this->Model->save($this->request->data)) {
            $this->Session->setFlash(__d('brownie', 'Unable to delete'), 'flash_error');
            $this->redirect($redirect);
        } else {
            $this->Session->setFlash(__d('brownie', 'Successful delete'), 'flash_success');
            $afterDelete = empty($this->params['named']['after_delete']) ? null : $this->params['named']['after_delete'];
            if ($afterDelete == 'parent') {
                $parentModel = $this->Model->brwConfig['parent'];
                if (!$parentModel) {
                    $afterDelete = 'index';
                } else {
                    $foreignKey = $this->Model->belongsTo[$parentModel]['foreignKey'];
                    $redirect = array(
                        'plugin' => 'brownie', 'controller' => 'contents',
                        'action' => 'view', $parentModel, $record[$model][$foreignKey]
                    );
                }
            }
            if ($afterDelete == 'index') {
                if ($this->Model->brwConfig['actions']['index']) {
                    $redirect = array(
                        'plugin' => 'brownie', 'controller' => 'contents',
                        'action' => 'index', $model
                    );
                } else {
                    $redirect = $home;
                }
            }
            $this->redirect($redirect);
        }
    }

    public function delete_multiple($model = null) {
        $model = $this->Model->name;
        $plural = $this->Model->brwConfig['names']['plural'];
        if (empty($this->request->data[$model]['id'])) {
            $msg = __d('brownie', 'No %s selected to delete', $plural);
            $this->Session->setFlash($msg, 'flash_notice');
        } else {
            $deleted = $no_deleted = 0;
            $dataArray = $this->request->data[$model]['id'];
            
            foreach ($dataArray as $id) {
                
                $this->Model->id = $id;
                $this->request->data[$model]['isDeleted'] = 1;

                //if ($this->Model->save($this->request->data)) {
                if ($this->Content->remove($this->Model, $id)) {
                    $deleted++;
                } else {
                    $no_deleted++;
                }
            }
            $msg_deleted = $msg_no_deleted = '';
            if ($deleted) {
                $msg_deleted = __d('brownie', '%d %s deleted.', $deleted, $plural) . ' ';
            }
            if ($no_deleted) {
                $msg_no_deleted = __d('brownie', '%d %s no deleted.', $no_deleted, $plural) . ' ';
            }

            if ($deleted) {
                if ($no_deleted)
                    $flashStatus = 'flash_notice';
                else
                    $flashStatus = 'flash_success';
            } else {
                $flashStatus = 'flash_error';
            }
            $this->Session->setFlash($msg_deleted . $msg_no_deleted, $flashStatus);
        }

        $redir = env('HTTP_REFERER');
        if (empty($redir)) {
            $redir = array('action' => 'index', $model);
        }
        $this->redirect($redir);
    }

    //############ AFTER SAVE ###############
    public function _afterSaveRedirect() {
        switch ($this->request->data[$this->Model->name]['after_save']) {
            case 'referer':
                if ($this->request->data[$this->Model->name]['referer']) {
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->redirect(array('controller' => 'brownie', 'action' => 'index'));
                }
                break;
            case 'edit':
                $this->redirect(array('action' => 'edit', $this->Model->id, 'after_save' => 'edit'));
                break;
            case 'add':
                $this->redirect(array('action' => 'add', 'after_save' => 'add'));
                break;
            case 'index':
                $this->redirect(array('action' => 'index'));
                break;
            case 'parent':
                if ($parent = $this->Model->brwConfig['parent']) {
                    $foreignKey = $this->Model->belongsTo[$parent]['foreignKey'];
                    if (!empty($this->request->data[$this->Model->alias][$foreignKey])) {
                        $idRedir = $this->request->data[$this->Model->alias][$foreignKey];
                    } else {
                        $record = $this->Model->findById($this->Model->id);
                        $idRedir = $record[$this->Model->alias][$foreignKey];
                    }
                    $this->redirect(array('action' => 'view', $parent, $idRedir));
                }
                $this->redirect(array('action' => 'index'));
                break;
            case 'view':
                $this->redirect(array('action' => 'view', $this->Model->id));
                break;
            case 'home':
                $this->redirect(array('controller' => 'brownie', 'action' => 'index'));
                break;
        }
    }
    
    
    public function csv(){
        //pr($_FILES);die;
        if($_FILES['file']['error'] == 0){
            
            if($_FILES['file']['type'] != 'application/vnd.ms-excel'){
                $this->Session->setFlash('Invalid file selected!','default',array('class'=>'flash flash_error'));
            }else{                
                 //Import uploaded file to Database

                $handle = fopen($_FILES['file']['tmp_name'], "r");
                $c=0; $array = array();
                while (($data = fgetcsv($handle)) !== FALSE) {
                   
                    if($c > 0){  //pr($data);die;
                        $retailer = $this->Retailer->findByName($data[0]);
                        if(!empty($retailer)){
                            $this->request->data['Coupon']['retailer_id'] = $retailer['Retailer']['id'];
                        }
                        $this->request->data['Coupon']['title'] = $data[1];
                        $this->request->data['Coupon']['description'] = $data[2];
                        $this->request->data['Coupon']['code'] = $data[3];
                        $this->request->data['Coupon']['expire'] = date('Y-m-d', strtotime($data[4]));
                        //$this->request->data['Coupon']['subCateId'] = $data[5];
                        $this->request->data['Coupon']['cashback'] = str_replace('%','',$data[6]);
                        $this->request->data['Coupon']['created'] = date('Y-m-d', strtotime($data[7]));
                        $this->Coupon->create();
                        $this->Coupon->save($this->request->data);
                        
                    }
                    $c++;
                }
                
                fclose($handle);
                
                $this->Session->setFlash('File imported successfully!','default',array('class'=>'flash flash_success'));
            }            
        }    
        $this->redirect(array('controller'=>'coupons','action'=>'index'));
    }
    
    public function xml(){
        //pr($_FILES['file']['tmp_name']);die;
        if($_FILES['file']['error'] == 0){
            if($_FILES['file']['type'] != 'text/xml'){
                $this->Session->setFlash('Invalid file selected!','default',array('class'=>'flash flash_error'));
            }else{
                
                $xml = simplexml_load_file($_FILES['file']['tmp_name']) or die("ERROR: Cannot create SimpleXML object");
                $c=0;
                //pr(count($xml->Record)); //die;
                foreach($xml->Record as $k => $array){
                    //pr($array);
                    $data = array();
                    foreach($array->Row->attributes() as $a => $b) {
                        //echo $a,'=>"',$b,"\"\n";
                        $b = (array)$b;
                        $data[$a] = $b[0];
                    }                    
                    //pr($data);
                    if($c > 0){ 
                       
                        $retailer = $this->Retailer->findByName($data['A']);
                        if(!empty($retailer)){
                            $this->request->data['Coupon']['retailer_id'] = $retailer['Retailer']['id'];
                        }
                        $this->request->data['Coupon']['title'] = $data['B'];
                        $this->request->data['Coupon']['description'] = $data['C'];
                        $this->request->data['Coupon']['code'] = $data['D'];
                        $expire = @explode('/',$data['E']);
                        $data['E'] = $expire[2].'-'.$expire[1].'-'.$expire[0];
                        $this->request->data['Coupon']['expire'] = $data['E'];
                        //$this->request->data['Coupon']['subCateId'] = $data['F];
                        $data['G'] = $data['G']*100;
                        $this->request->data['Coupon']['cashback'] = str_replace('%','',$data['G']);
                        $created = @explode('/',$data['H']);
                        $data['H'] = $created[2].'-'.$created[1].'-'.$created[0];
                        $this->request->data['Coupon']['created'] = date('Y-m-d', strtotime($data['H']));
                        $this->Coupon->create();
                        $this->Coupon->save($this->request->data);
                        
                    }
                    $c++;
                    unset($data);
                }                              
                
                $this->Session->setFlash('File imported successfully!','default',array('class'=>'flash flash_success'));
            }
                        
            //$this->redirect(array('controller'=>'coupons','action'=>'index'));
        }
        $this->redirect(array('controller'=>'coupons','action'=>'index'));
    }

}
