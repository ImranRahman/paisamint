<?php

class SubcategoryController extends BrownieAppController {

    public $components = array('Paginator');
    public $helpers = array('Brownie.i18n');
    public $Model;
    public $uses = array('Brownie.Content','RetailerCategory');

    //public $paginate = array();
    //public $data = array();

    public function beforeFilter() {
        parent::beforeFilter();

        $model = 'SubCategory';
        $this->Model = ClassRegistry::init($model);

        $this->loadModel('Category');
        $this->loadModel('SubCategory');
        $this->loadModel('Retailer');
        

        $this->set('model', $model);
    }

    public function index() {

        $this->set('categories', $this->Category->find('all', array(
                    'fields' => array('Category.id', 'Category.name')
        )));

        $joins = array(
            array(
                'table' => 'categories',
                'alias' => 'Category',
                'conditions' => array('Category.id = SubCategory.category_id'),
                'type' => 'LEFT'
            )
        );

        $subcategories = $this->paginate = array(
            'fields' => array('Category.id', 'Category.name', 'SubCategory.*'),
            'joins' => $joins,
            'order' => array('SubCategory.id' => 'desc'),
            'conditions'=>array('SubCategory.isDeleted'=>'0')
        );

        $data = $this->paginate('SubCategory'); //pr($data);die;
        $this->set('subcategories', $data);
    }

    public function add() {
        $this->set('categories', $this->Category->find('all', array(
                    'fields' => array('Category.id', 'Category.name')
        )));

        if ($this->request->is('post')) {
//            pr($this->request->data);  die;
            if ($this->SubCategory->save($this->request->data)) {
                $msg = ($this->Model->brwConfig['names']['gender'] == 1) ?
                        __d('brownie', 'The %s has been saved [male]', __($this->Model->brwConfig['names']['singular'])) : __d('brownie', 'The %s has been saved [female]', __($this->Model->brwConfig['names']['singular']));

                $this->Session->setFlash($msg, 'flash_success');

                if (!empty($this->request->data[$this->Model->name]['after_save'])) {
                    $this->_afterSaveRedirect();
                }
            }

            $msg = ($this->Model->brwConfig['names']['gender'] == 1) ?
                    __d('brownie', 'The %s could not be saved. Please, check the error messages.[male]', __($this->Model->brwConfig['names']['singular'])) :
                    __d('brownie', 'The %s could not be saved. Please, check the error messages.[female]', __($this->Model->brwConfig['names']['singular']));
            $this->Session->setFlash($msg, 'flash_error');
        }
    }

    public function view($id) {


        $neighbors = $this->SubCategory->find(
                'neighbors', array('field' => 'id', 'value' => $id,'conditions'=>array('SubCategory.isDeleted'=>0))                
        );
        
        $this->set('neighbors', $neighbors);

        $joins = array(
            array(
                'table' => 'categories',
                'alias' => 'Category',
                'conditions' => array('Category.id = SubCategory.category_id'),
                'type' => 'INNER'
            )
        );

        $data = $this->SubCategory->find('first', array(
            'fields' => array('Category.id', 'Category.name', 'SubCategory.*'),
            'joins' => $joins,
            'conditions' => array('SubCategory.id' => $id)
        ));
        $this->set('datas', $data);
    }

    public function edit($id = null) { 
        $this->set('categories', $this->Category->find('all', array(
                    'fields' => array('Category.id', 'Category.name')
        )));

        $joins = array(
            array(
                'table' => 'categories',
                'alias' => 'Category',
                'conditions' => array('Category.id = SubCategory.category_id'),
                'type' => 'INNER'
        ));
        $data = $this->SubCategory->find('first', array(
            'fields' => array('Category.id', 'Category.name', 'SubCategory.*'),
            'joins' => $joins,
            'conditions' => array('SubCategory.id' => $id)
        ));

        if ($this->request->is(array('post', 'put'))) {
            //pr($this->request->data);  die;
            $this->SubCategory->id = $id;
            if ($this->SubCategory->save($this->request->data)) {
                $msg = ($this->Model->brwConfig['names']['gender'] == 1) ?
                        __d('brownie', 'The %s has been saved [male]', __($this->Model->brwConfig['names']['singular'])) : __d('brownie', 'The %s has been saved [female]', __($this->Model->brwConfig['names']['singular']));

                $this->Session->setFlash($msg, 'flash_success');

                if (!empty($this->request->data[$this->Model->name]['after_save'])) {
                    $this->_afterSaveRedirect();
                }
            }

            $msg = ($this->Model->brwConfig['names']['gender'] == 1) ?
                    __d('brownie', 'The %s could not be saved. Please, check the error messages.[male]', __($this->Model->brwConfig['names']['singular'])) :
                    __d('brownie', 'The %s could not be saved. Please, check the error messages.[female]', __($this->Model->brwConfig['names']['singular']));
            $this->Session->setFlash($msg, 'flash_error');
        }

        if (!$this->request->data) {
            $this->request->data = $data;
        }
    }

    public function delete($model = null, $id) {
        $model = $modelName;
        $record = $this->Model->findById($id);
        if (empty($record)) {
            throw new NotFoundException('Record does not exists');
        }
        $home = array('plugin' => 'brownie', 'controller' => 'brownie', 'action' => 'index', 'brw' => false);
        $redirect = $this->referer($home);
        //$deleted = $this->Content->remove($this->Model, $id);
        //die(pr($this->Model->name));
        $modelName = $this->Model->name;
        $this->Model->id = $id;
        $this->request->data[$modelName]['isDeleted'] = 1;

        //if (!$deleted) {
        if (!$this->Model->save($this->request->data)) {
            $this->Session->setFlash(__d('brownie', 'Unable to delete'), 'flash_error');
            $this->redirect($redirect);
        } else {
            $this->Session->setFlash(__d('brownie', 'Successful delete'), 'flash_success');
            $afterDelete = empty($this->params['named']['after_delete']) ? null : $this->params['named']['after_delete'];
            if ($afterDelete == 'parent') {
                $parentModel = $this->Model->brwConfig['parent'];
                if (!$parentModel) {
                    $afterDelete = 'index';
                } else {
                    $foreignKey = $this->Model->belongsTo[$parentModel]['foreignKey'];
                    $redirect = array(
                        'plugin' => 'brownie', 'controller' => 'contents',
                        'action' => 'view', $parentModel, $record[$model][$foreignKey]
                    );
                }
            }
            if ($afterDelete == 'index') {
                if ($this->Model->brwConfig['actions']['index']) {
                    $redirect = array(
                        'plugin' => 'brownie', 'controller' => 'contents',
                        'action' => 'index', $model
                    );
                } else {
                    $redirect = $home;
                }
            }
            $this->redirect($redirect);
        }
    }

    public function delete_multiple($model = null) {
        $plural = $this->Model->brwConfig['names']['plural'];
        $modelName = $this->Model->name;
        $model = $modelName;
        if (empty($this->request->data[$modelName]['id'])) {
            $msg = __d('brownie', 'No %s selected to delete', $plural);
            $this->Session->setFlash($msg, 'flash_notice');
        } else {
            $deleted = $no_deleted = 0;
            //die(pr($this->request->data[$modelName]['id']));
            $dataArray = $this->request->data[$modelName]['id'];
            unset($this->request->data[$modelName]['id']);
            foreach ($dataArray as $id) {
                
                $this->Model->id = $id;
                $this->request->data[$modelName]['isDeleted'] = 1;

                if ($this->Model->save($this->request->data)) {
                    //if ($this->Content->remove($this->Model, $id)) {
                    $deleted++;
                } else {
                    $no_deleted++;
                }
                
            }
            $msg_deleted = $msg_no_deleted = '';
            if ($deleted) {
                $msg_deleted = __d('brownie', '%d %s deleted.', $deleted, $plural) . ' ';
            }
            if ($no_deleted) {
                $msg_no_deleted = __d('brownie', '%d %s no deleted.', $no_deleted, $plural) . ' ';
            }

            if ($deleted) {
                if ($no_deleted)
                    $flashStatus = 'flash_notice';
                else
                    $flashStatus = 'flash_success';
            } else {
                $flashStatus = 'flash_error';
            }
            $this->Session->setFlash($msg_deleted . $msg_no_deleted, $flashStatus);
        }

        $redir = env('HTTP_REFERER');
        if (empty($redir)) {
            $redir = array('action' => 'index', $model);
        }
        $this->redirect($redir);
    }

    public function _afterSaveRedirect() {
        switch ($this->request->data[$this->Model->name]['after_save']) {
            case 'referer':
                if ($this->request->data[$this->Model->name]['referer']) {
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->redirect(array('controller' => 'brownie', 'action' => 'index'));
                }
                break;
            case 'edit':
                $this->redirect(array('action' => 'edit', $this->Model->id, 'after_save' => 'edit'));
                break;
            case 'add':
                $this->redirect(array('action' => 'add', 'after_save' => 'add'));
                break;
            case 'index':
                $this->redirect(array('action' => 'index'));
                break;
            case 'parent':
                if ($parent = $this->Model->brwConfig['parent']) {
                    $foreignKey = $this->Model->belongsTo[$parent]['foreignKey'];
                    if (!empty($this->request->data[$this->Model->alias][$foreignKey])) {
                        $idRedir = $this->request->data[$this->Model->alias][$foreignKey];
                    } else {
                        $record = $this->Model->findById($this->Model->id);
                        $idRedir = $record[$this->Model->alias][$foreignKey];
                    }
                    $this->redirect(array('action' => 'view', $parent, $idRedir));
                }
                $this->redirect(array('action' => 'index'));
                break;
            case 'view':
                $this->redirect(array('action' => 'view', $this->Model->id));
                break;
            case 'home':
                $this->redirect(array('controller' => 'brownie', 'action' => 'index'));
                break;
        }
    }
    
    
    public function getCategory(){
        //pr($this->request->data);die;
        $ret_id = $this->request->data['ret_id'];
        $retCates = $this->RetailerCategory->find('list', array(
            'fields'=>array('RetailerCategory.category_id'),
            'conditions'=>array('RetailerCategory.retailer_id'=>$ret_id)
        ));
        //pr($retCates);die;
        if(!empty($retCates)){
            $cates = $this->Category->find('list', array(
                'conditions'=>array('Category.id IN '=>$retCates)
            ));
            //pr($cates);die;
            $data = '<option value="">--Select--</option>';
            foreach($cates as $key => $cate){
                $data .= '<optgroup label="'.$cate.'">';
                $subCates = $this->SubCategory->find('all', array(
                    'conditions'=>array('SubCategory.category_id'=>$key)
                ));
                foreach($subCates as $subCate){
                    $data .= '<option value="'.$subCate['SubCategory']['id'].'">'.$subCate['SubCategory']['name'].'</option>';
                }
                $data .= '</optgroup>';
            }
            echo $data;
        }
        die;
    }

}
