<?php
class TransactionsController extends BrownieAppController
{
   public $components = array('Paginator');
    public $helpers = array('Brownie.i18n');
    public $Model;
    public $uses = array('Brownie.Content','User');

    public function beforeFilter() {
        parent::beforeFilter();

        $model = 'Transaction';
        $this->Model = ClassRegistry::init($model);
        $this->loadModel($model);
       
        $this->set('model', $model);
    } 
    
    public function index() {
        $joins = array(
            array(
                'table' => 'users',
                'alias' => 'User',
                'conditions' => array('User.id = Transaction.user_id'),
                'type' => 'LEFT'
            ),            
        );

        $coupons = $this->paginate = array(
            'fields' => array('User.id', 'User.name','Transaction.*'),
            'joins' => $joins,
            'order' => array('Transaction.id' => 'desc'),
            'conditions'=>array('Transaction.isDeleted'=>'0')
        );

        $data = $this->paginate($this->Model); //pr($data);die;
        $this->set('datas', $data);
    }
    
    
    //############ EDIT ###############
    public function edit($id = null) {
        $this->set('users', $this->User->find('all', array(
            'fields' => array('User.id', 'User.name'),
            'conditions'=>array('User.isDeleted'=>0),
            'order'=>array('User.name'=>'asc')
        )));

        $data = $this->Model->find('first', array(
            'fields'=>array('Transaction.*','User.id,name'),
            'conditions' => array('Transaction.id' => $id,'Transaction.isDeleted'=>0),
            'joins'=>array(
                array(
                    'table'=>'users',
                    'alias'=>'User',
                    'conditions'=>array('User.id = Transaction.user_id'),
                    'type'=>'left'
                )
            )
        ));
        
        $Model = $this->Model->name; //echo ($Model);die; 
        if ($this->request->is(array('post', 'put'))) {
            //pr($this->request->data);  die;            
            
            $this->Model->id = $id;
            if ($this->Model->save($this->request->data)) {

                $msg = ($this->Model->brwConfig['names']['gender'] == 1) ?
                        __d('brownie', 'The %s has been saved [male]', __($this->Model->brwConfig['names']['singular'])) :
                        __d('brownie', 'The %s has been saved [female]', __($this->Model->brwConfig['names']['singular']));

                $this->Session->setFlash($msg, 'flash_success');

                if (!empty($this->request->data[$this->Model->name]['after_save'])) {
                    $this->_afterSaveRedirect();
                }
            }

            $msg = ($this->Model->brwConfig['names']['gender'] == 1) ?
                    __d('brownie', 'The %s could not be saved. Please, check the error messages.[male]', __($this->Model->brwConfig['names']['singular'])) :
                    __d('brownie', 'The %s could not be saved. Please, check the error messages.[female]', __($this->Model->brwConfig['names']['singular']));

            $this->Session->setFlash($msg, 'flash_error');
        }

        if (!$this->request->data) {
            $this->request->data = $data;
        }
        $this->set('data',$data);
    }
    
    
    //############ AFTER SAVE ###############
    public function _afterSaveRedirect() {
        switch ($this->request->data[$this->Model->name]['after_save']) {
            case 'referer':
                if ($this->request->data[$this->Model->name]['referer']) {
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->redirect(array('controller' => 'brownie', 'action' => 'index'));
                }
                break;
            case 'edit':
                $this->redirect(array('action' => 'edit', $this->Model->id, 'after_save' => 'edit'));
                break;
            case 'add':
                $this->redirect(array('action' => 'add', 'after_save' => 'add'));
                break;
            case 'index':
                $this->redirect(array('action' => 'index'));
                break;
            case 'parent':
                if ($parent = $this->Model->brwConfig['parent']) {
                    $foreignKey = $this->Model->belongsTo[$parent]['foreignKey'];
                    if (!empty($this->request->data[$this->Model->alias][$foreignKey])) {
                        $idRedir = $this->request->data[$this->Model->alias][$foreignKey];
                    } else {
                        $record = $this->Model->findById($this->Model->id);
                        $idRedir = $record[$this->Model->alias][$foreignKey];
                    }
                    $this->redirect(array('action' => 'view', $parent, $idRedir));
                }
                $this->redirect(array('action' => 'index'));
                break;
            case 'view':
                $this->redirect(array('action' => 'view', $this->Model->id));
                break;
            case 'home':
                $this->redirect(array('controller' => 'brownie', 'action' => 'index'));
                break;
        }
    }
    
    
}