<?php

App::uses('CakeEmail', 'Network/Email');

class SubscribesController extends BrownieAppController {

    public $components = array('Paginator');
    public $helpers = array('Brownie.i18n');
    public $Model;
    public $uses = array('Brownie.Content', 'User');

    public function beforeFilter() {
        parent::beforeFilter();

        $model = 'Subscribe';
        $this->Model = ClassRegistry::init($model);
        $this->loadModel($model);

        $this->set('model', $model);
    }

    public function index() {

        $coupons = $this->paginate = array(
            'order' => array($this->Model->name . '.id' => 'desc'),
            'conditions' => array($this->Model->name . '.isDeleted' => '0')
        );

        $data = $this->paginate($this->Model); //pr($data);die;
        $this->set('datas', $data);
    }

    // ########## ADD #######################
    public function add() {

        $Model = $this->Model->name; //echo ($Model);die; 
        if ($this->request->is('post')) {
            //pr($this->request->data);  die; 

            if ($this->Model->save($this->request->data)) {

                $msg = ($this->Model->brwConfig['names']['gender'] == 1) ?
                        __d('brownie', 'The %s has been saved [male]', __($this->Model->brwConfig['names']['singular'])) :
                        __d('brownie', 'The %s has been saved [female]', __($this->Model->brwConfig['names']['singular']));

                $this->Session->setFlash($msg, 'flash_success');

                if (!empty($this->request->data[$this->Model->name]['after_save'])) {
                    $this->_afterSaveRedirect();
                }
            }

            $msg = ($this->Model->brwConfig['names']['gender'] == 1) ?
                    __d('brownie', 'The %s could not be saved. Please, check the error messages.[male]', __($this->Model->brwConfig['names']['singular'])) :
                    __d('brownie', 'The %s could not be saved. Please, check the error messages.[female]', __($this->Model->brwConfig['names']['singular']));

            $this->Session->setFlash($msg, 'flash_error');
        }
    }

    //############ EDIT ###############
    public function edit($id = null) {
        $Model = $this->Model->name;
        $data = $this->Model->find('first', array(
            'conditions' => array($Model . '.id' => $id, $Model . '.isDeleted' => 0)
        ));

        //echo ($Model);die; 
        if ($this->request->is(array('post', 'put'))) {
            //pr($this->request->data);  die;            

            $this->Model->id = $id;
            if ($this->Model->save($this->request->data)) {

                $msg = ($this->Model->brwConfig['names']['gender'] == 1) ?
                        __d('brownie', 'The %s has been saved [male]', __($this->Model->brwConfig['names']['singular'])) :
                        __d('brownie', 'The %s has been saved [female]', __($this->Model->brwConfig['names']['singular']));

                $this->Session->setFlash($msg, 'flash_success');

                if (!empty($this->request->data[$this->Model->name]['after_save'])) {
                    $this->_afterSaveRedirect();
                }
            }

            $msg = ($this->Model->brwConfig['names']['gender'] == 1) ?
                    __d('brownie', 'The %s could not be saved. Please, check the error messages.[male]', __($this->Model->brwConfig['names']['singular'])) :
                    __d('brownie', 'The %s could not be saved. Please, check the error messages.[female]', __($this->Model->brwConfig['names']['singular']));

            $this->Session->setFlash($msg, 'flash_error');
        }

        if (!$this->request->data) {
            $this->request->data = $data;
        }
        $this->set('data', $data);
    }

    public function delete($id = null, $model = null) {
        $model = $this->Model->name;
        $record = $this->Model->findById($id);
        if (empty($record)) {
            throw new NotFoundException('Record does not exists');
        }
        $home = array('plugin' => 'brownie', 'controller' => 'brownie', 'action' => 'index', 'brw' => false);
        $redirect = $this->referer($home);
        $deleted = $this->Content->remove($this->Model, $id);
        //die(pr($this->Model->name));

        $this->Model->id = $id;
        $this->request->data[$model]['isDeleted'] = 1;

        if (!$deleted) {
            //if (!$this->Model->save($this->request->data)) {
            $this->Session->setFlash(__d('brownie', 'Unable to delete'), 'flash_error');
            $this->redirect($redirect);
        } else {
            $this->Session->setFlash(__d('brownie', 'Successful delete'), 'flash_success');
            $afterDelete = empty($this->params['named']['after_delete']) ? null : $this->params['named']['after_delete'];
            if ($afterDelete == 'parent') {
                $parentModel = $this->Model->brwConfig['parent'];
                if (!$parentModel) {
                    $afterDelete = 'index';
                } else {
                    $foreignKey = $this->Model->belongsTo[$parentModel]['foreignKey'];
                    $redirect = array(
                        'plugin' => 'brownie', 'controller' => 'contents',
                        'action' => 'view', $parentModel, $record[$model][$foreignKey]
                    );
                }
            }
            if ($afterDelete == 'index') {
                if ($this->Model->brwConfig['actions']['index']) {
                    $redirect = array(
                        'plugin' => 'brownie', 'controller' => 'contents',
                        'action' => 'index', $model
                    );
                } else {
                    $redirect = $home;
                }
            }
            $this->redirect($redirect);
        }
    }

    public function delete_multiple($model = null) {
        $model = $this->Model->name;
        $plural = $this->Model->brwConfig['names']['plural'];
        if (empty($this->request->data[$model]['id'])) {
            $msg = __d('brownie', 'No %s selected to delete', $plural);
            $this->Session->setFlash($msg, 'flash_notice');
        } else {
            $deleted = $no_deleted = 0;
            $dataArray = $this->request->data[$model]['id'];

            foreach ($dataArray as $id) {

                $this->Model->id = $id;
                $this->request->data[$model]['isDeleted'] = 1;

                //if ($this->Model->save($this->request->data)) {
                if ($this->Content->remove($this->Model, $id)) {
                    $deleted++;
                } else {
                    $no_deleted++;
                }
            }
            $msg_deleted = $msg_no_deleted = '';
            if ($deleted) {
                $msg_deleted = __d('brownie', '%d %s deleted.', $deleted, $plural) . ' ';
            }
            if ($no_deleted) {
                $msg_no_deleted = __d('brownie', '%d %s no deleted.', $no_deleted, $plural) . ' ';
            }

            if ($deleted) {
                if ($no_deleted)
                    $flashStatus = 'flash_notice';
                else
                    $flashStatus = 'flash_success';
            } else {
                $flashStatus = 'flash_error';
            }
            $this->Session->setFlash($msg_deleted . $msg_no_deleted, $flashStatus);
        }

        $redir = env('HTTP_REFERER');
        if (empty($redir)) {
            $redir = array('action' => 'index', $model);
        }
        $this->redirect($redir);
    }

    //############ AFTER SAVE ###############
    public function _afterSaveRedirect() {
        switch ($this->request->data[$this->Model->name]['after_save']) {
            case 'referer':
                if ($this->request->data[$this->Model->name]['referer']) {
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->redirect(array('controller' => 'brownie', 'action' => 'index'));
                }
                break;
            case 'edit':
                $this->redirect(array('action' => 'edit', $this->Model->id, 'after_save' => 'edit'));
                break;
            case 'add':
                $this->redirect(array('action' => 'add', 'after_save' => 'add'));
                break;
            case 'index':
                $this->redirect(array('action' => 'index'));
                break;
            case 'parent':
                if ($parent = $this->Model->brwConfig['parent']) {
                    $foreignKey = $this->Model->belongsTo[$parent]['foreignKey'];
                    if (!empty($this->request->data[$this->Model->alias][$foreignKey])) {
                        $idRedir = $this->request->data[$this->Model->alias][$foreignKey];
                    } else {
                        $record = $this->Model->findById($this->Model->id);
                        $idRedir = $record[$this->Model->alias][$foreignKey];
                    }
                    $this->redirect(array('action' => 'view', $parent, $idRedir));
                }
                $this->redirect(array('action' => 'index'));
                break;
            case 'view':
                $this->redirect(array('action' => 'view', $this->Model->id));
                break;
            case 'home':
                $this->redirect(array('controller' => 'brownie', 'action' => 'index'));
                break;
        }
    }

    public function send_emails() {

        if ($this->request->is('post')) {
            //pr($this->request->data);//die;
            /* ... ger users .. */
            if ($this->request->data['Subscribe']['sendto'] == 1) {
                $Model = 'Subscribe';
            } else {
                $Model = 'User';
            }

            $users = $this->$Model->find('all', array(
                'fields' => array($Model . '.email,id'),
                'conditions' => array($Model . '.isDeleted' => 0, $Model . '.status' => 1)
            ));
            //pr($users);die;
            
            foreach($users as $user){
                //pr($user);
                if(filter_var($user[$Model]['email'], FILTER_VALIDATE_EMAIL)){
                    $vars = array(
                        'email' => $user[$Model]['email'],
                        'message' => $this->request->data['Subscribe']['content'],
                        'id' => $user[$Model]['id'],
                        'baseUrl' => BASE_URL,
                    );

                    $Email = new CakeEmail('smtp');
                    $Email->viewVars($vars);
    
                    $Email->template('newsletter')
                        ->emailFormat('html')
                        ->subject($this->request->data['Subscribe']['subject'])
                        ->to($user[$Model]['email'])
                        ->from(SITE_EMAIL)
                        ->send();
                }
            }
            $this->Session->setFlash('Email has been sent.', 'flash_success');
           // $this->redirect(array('controller'=>'subscribes','action'=>'send_emails'));
        }
        
    }

}
