<?php
//pr($retailerCate);
$Controller = $this->params['controller'];
?>

<div id="<?php echo $model;?>_view" class="view">
    <?php if(empty($datas)){ 
            echo '<p class="flash flash_error">There is no '.$model.' found.</p>';
    }else { ?>
    <div class="clearfix">
        <h1><?php echo $model;?></h1>
        
        <div class="actions-view">
            <ul class="actions neighbors">
                <?php
                    if (!empty($neighbors['prev'])) {
                            echo '
                            <li class="prev">
                                    ' . $this->Html->link('Previous',
                                    array('action' => 'view', $neighbors['prev'][$model]['id']) + $this->params['named'],
                                    array('title' => __d('brownie', 'Previous'))).'
                            </li>';
                    }
                    if (!empty($neighbors['next'])) {
                            echo '
                            <li class="next">
                                    ' . $this->Html->link(__d('brownie', 'Next'),
                                    array('action' => 'view', $neighbors['next'][$model]['id']) + $this->params['named'],
                                    array('title' => __d('brownie', 'Next'))).'
                            </li>';
                    }
                    ?>
                    <?php
                    if (!empty($this->params['named']['back_to'])) {
                            $backToUrl = array('plugin' => 'brownie', 'controller' => 'contents');
                            $named = $this->params['named'];
                            $back_to = $named['back_to'];
                            unset($named['back_to']);
                            switch($back_to) {
                                    case 'index':
                                            $backToUrl += array('action' => 'index', $model) + $named;
                                    break;
                            }
                            echo '
                            <li class="back">
                                    ' . $this->Html->link(__d('brownie', 'Back'), $backToUrl, array('title' => __d('brownie', 'Back'))) . '
                            <li>';
                    } else{
                        echo '
                            <li class="back">
                                    ' . $this->Html->link('Back', array('controller'=>$Controller,'action'=>'index'), array('title' => __d('brownie', 'Back'))) . '
                            <li>';
                    }
                    ?>
                
            </ul>
            <ul class="actions">
                <li class="add">
                    <?php echo $this->Html->link('Add', array('controller'=>$Controller, 'action'=>'add', 'after_save:view')); ?>
                <li class="edit">
                    <?php echo $this->Html->link('Edit', array('controller'=>$Controller, 'action'=>'edit', $datas[$model]['id'])); ?>
                </li>
                <li class="delete">
                <?php echo $this->Html->link('Delete', array('controller'=>$Controller, 'action'=>'delete', $datas[$model]['id'],'after_delete:parent'), array('confirm'=>'Are you sure you want to delete this ?')); ?>
                </li>
                <li class="index">
                    <?php echo $this->Html->link('List all', array('controller'=>$Controller, 'action'=>'index')); ?>
                </li>
            </ul>
        </div>

        <table class="view">

            <tbody>                
                <tr>
                    <td class="label">Id</td>
                    <td class="fcktxt"><?php echo $datas[$model]['id'];?></td>
                </tr>
                <tr>
                    <td class="label">Retailer Type</td>
                    <td class="fcktxt"><?php echo $datas['RetailerType']['name'];?></td>
                </tr> 
                <tr>
                    <td class="label">Category</td>
                    <td class="fcktxt"><?php
                    $ret_cate = array();
                    foreach($retailerCate as $retCate){
                        array_push($ret_cate, $retCate['Category']['name']);
                    }
                    echo @implode(', ', $ret_cate);
                    ?></td>
                </tr>
                <tr>
                    <td class="label">Name</td>
                    <td class="fcktxt"><?php echo $datas[$model]['name'];?></td>
                </tr>
                <tr>
                    <td class="label">Description</td>
                    <td class="fcktxt"><?php echo $datas[$model]['description'];?></td>
                </tr>
                <tr>
                    <td class="label">Phone</td>
                    <td class="fcktxt"><?php echo $datas[$model]['phone'];?></td>
                </tr>
                
                <tr>
                    <td class="label">Url</td>
                    <td class="fcktxt"><?php echo $datas[$model]['url'];?></td>
                </tr>
                
                <?php 
                //pr($datas['RetailerParam']);
                ?>
                <tr>
                    <td class="label">Url parameter</td>
                    <td class="fcktxt">
                        <table border="0" style="border:none;">
                            <tr>
                                <td style="border:none;"><strong><u>Name</u></strong></td>
                                <td style="border:none;"><strong><u>Value</u></strong></td>
                            </tr>
                            <?php foreach($datas['RetailerParam'] as $Params){ ?>
                            <tr>
                                <td style="border:none;"><?php echo $Params['name'];?></td>
                                <td style="border:none;"><?php echo $Params['value'];?></td>
                            </tr>
                            <?php }?>
                        </table>
                    </td>
                </tr>
                
                
                
                <tr>
                    <td class="label">Status</td>
                    <td class="fcktxt"><?php echo ($datas[$model]['status']==1)?'Active':'Inactive';?></td>
                </tr>
                <tr>
                    <td class="label">Created</td>
                    <td class="fcktxt"><?php echo date('m/d/Y h:i', strtotime($datas[$model]['created']));?></td>
                </tr>
                               
            </tbody>
        </table>
    </div>
        <?php } ?>
    <div class="brw-images index">
    </div>
    <div class="brw-files index">
    </div>

</div>