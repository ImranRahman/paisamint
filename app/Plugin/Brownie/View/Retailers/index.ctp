<?php 
//pr($datas);
//pr($this->params);
$Controller = $this->params['controller'];
$url = array('controller' => $Controller, 'action' => 'delete_multiple');

$paginator = $this->Paginator;
?>

<div id="Category_index" class="model-index">

    <div class="index clearfix">
        <h2><?php echo __(ucfirst($Controller));?></h2>
        <div class="actions">
            <ul>
                <li class="add">
                    <?php echo $this->Html->link('Add '.$model, array('action'=>'add'));?>
                </li>
            </ul>
        </div>
    </div>

    <?php if(empty($datas)){ ?>
    <p class="norecords">There are no <?php echo ucfirst($Controller);?></p>
    <?php }else{ ?>
    
    <?php echo $this->Form->create($model, array('url'=>$url, 'id'=>'deleteMultiple')); ?>

    <table id="index">
        <tbody>
            <tr class="list">
                <th class="delete_multiple">
                    <input type="checkbox" title="Select/Unselect all" id="deleteCheckAll" style="visibility: visible;">
                </th>
                <th class="id number"><?php echo $paginator->sort('id','Id');?></th>
                <th class="name string"><a href="#"><?php echo __('Logo');?></a></th>
                <th><a href="#"><?php echo __('Retailer Type');?></a></th>
                <th class="name string"><?php echo $paginator->sort('name','Name');?></a></th>
                <!--<th><a href="#"><?php echo __('Category');?></a></th>-->
                <th><a href="#"><?php echo __('Description');?></a></th>
                <th class="actions"><?php echo __('Actions');?></th>
            </tr>

            <?php foreach($datas as $data){ ?>

            <tr class="list">
                <td class="delete_multiple">
                    <input type="checkbox" value="<?php echo $data[$model]['id'];?>" name="data[<?php echo $model;?>][id][]">
                </td>
                
                <td class="id number field">
                    <?php echo $data[$model]['id'];?>
                </td>
                
                <td class="name string field">
                    <?php 
                    if($data[$model]['logo']!='' && file_exists(RETAILER_THUMB.$data[$model]['logo'])){
                    ?>
                    <img src="<?php echo RETAILER_THUMB_URL.$data[$model]['logo'];?>">
                    <?php }else{                     
                      echo $this->Html->image('noimage.jpg'); 
                    }?>
                </td>
                
                <td class="name string field">
                    <?php echo $data['RetailerType']['name'];?>
                </td>
                
                <td class="name string field">
                    <?php echo $data[$model]['name'];?>
                </td>
                
<!--                <td class="name string field">
                    <?php echo $data['Category']['name'];?>
                </td>-->
                
                <td class="name string field">
                    <?php echo $data[$model]['description'];?>
                </td>
                
                <td class="actions">
                    <ul class="actions">
                        <li class="view"><?php echo $this->Html->link("View", array('action'=>'view',$data[$model]['id']));?></li>
                        <li class="edit"><?php echo $this->Html->link("Edit", array('action'=>'edit',$data[$model]['id']));?></li>
                        <li class="delete"><?php echo $this->Form->postLink( "Delete", array('action' => 'delete', $data[$model]['id']), array('escape' => false, 'confirm' => __('Are you sure you want to delete this ?'))); ?>
                            
                        </li>
                    </ul>
                </td> 
            </tr>
            <?php } ?>
        </tbody>
    </table>
        <!--<div class="submit"><button><span>Delete selected</span></button></div>-->
    <?php $options = array('label'=>'Delete selected','class'=>'delete-selected'); ?>
    <?php echo $this->Form->end($options);?>
    
<?php //echo $this->element('sql_dump');?>

<?php 
// pagination section
$brwConfig['names']['plural'] = 'Show';
$brwConfig['paginate']['limit'] = 20;
echo $this->element('pagination', array('model' => $model, 'brwConfig' => $brwConfig));
?>
    
<?php }/*empty else*/ ?>    
</div>