<?php

$Controller = $this->params['controller'];
$Action = $this->params['action'];
$url = array('controller' => $Controller, 'action' => $Action);
?>
<style>
    #paramName{ width: 45%; float: left;}
    #paramValue{ width: 45%; float: right; margin-top: -57px;}
    .del{ width: 100%; text-align: right;}
</style>
<div class="form">
    <?php 
        
    echo $this->Form->create($model, array('type' => 'file', 'action' => $Action, 'autocomplete' => 'off', 'url' => $url, 'novalidate' => true, 'inputDefaults' => array('separator' => ' '))); ?>

    <fieldset>
        <legend><?php echo __("Add ".$model);?></legend>

            <?php 
            $retailerTypesOption = array();
            $retailerTypesOption[''] = 'Select Retailer Type';
            
            foreach($RetailerTypes as $RetailerType){
                $retailerTypesOption[$RetailerType['RetailerType']['id']] = $RetailerType['RetailerType']['name']; 
            }
            
            echo $this->Form->input('retailer_type_id', array('options'=>$retailerTypesOption,'class'=>'select'));
            
            $options = array();
            $options[''] = 'Select Category';
            foreach($categories as $cate){
                $options[$cate['Category']['id']] = $cate['Category']['name']; 
            }
            
            echo $this->Form->input('category_id', array('options'=>$options,'class'=>'select','multiple'=>'multiple'));
            echo __('Press Ctrl key to select multiple');
            
            echo $this->Form->input('name');
            
            echo $this->Form->input('logo_image', array('type'=>'file'));
            
            echo $this->Form->input('description');
            
            echo $this->Form->input('phone');
            
            echo $this->Form->input('url', array('placeholder'=>'http://example.com'));
            ?>

        <div class="retailerParam">
            <div class="input text">
                <label for="params">Url Parameters:</label>
                <div class="input text" id="paramName">
                    <label for="params name">Name:</label>
                    <input type="text" name="data[RetailerParam][name][]" placeholder="Parameter Name" />
                </div>
                <div class="input text" id="paramValue">
                    <label for="params value">Value:</label>
                    <input type="text" name="data[RetailerParam][value][]" placeholder="Parameter Value" />
                </div>
            </div>
        </div>

        <div class="input text" style="text-align:right;">
            <a href="#" id='addParam' title="Add Parameter">
            <?php echo $this->Html->image('agregar2_ico.png');?>
            </a>            
        </div>

    </fieldset>

    <fieldset>
            <?php 
            $saveOption = array(
                'referer'=>'Back to where I was',
                'view'=>'View saved '.$model,
                'add'=>'Add another '.$model,
                'index'=>'Go to the index of all '.$Controller,
                'edit'=>'Continue editing this '.$model,
                'home'=>'Go home',
            )
            ?>
        <div class="input select">
            <!--<label for="ContentAfterSave">After save</label>
            <select id="ContentAfterSave" separator=" " name="data[SubCategory][after_save]">
                <option selected="selected" value="referer">Back to where I was</option>
                <option value="view">View saved Category</option>
                <option value="add">Add another Category</option>
                <option value="index">Go to the index of all Categories</option>
                <option value="edit">Continue editing this Category</option>
                <option value="home">Go home</option>
            </select>-->
                <?php echo $this->Form->input('after_save', array('options'=>$saveOption, 'separator'=> " ")); ?>
        </div>
    </fieldset>

<?php echo $this->Form->hidden('referer', array('value'=> Router::url(array('controller' => $Controller, 'action' => 'index'))));?>
    <div class="submit">
        <input type="submit" value="<?php echo __d('brownie', 'Save') ?>" />

        <?php echo $this->Html->link('Cancel', array('controller'=>$Controller,'action'=>'index'), array('class'=>'cancel'));?>
    </div>

<?php echo $this->Form->end(); ?>

</div>

<script>
$(document).ready(function(){
    var i = 1;
   $('#addParam').click(function(e){
        var row = '<div class="input text" id="cont'+i+'"><div class="input text" id="paramName"><label for="params name">Name:</label><input type="text" name="data[RetailerParam][name][]" placeholder="Parameter Name" /></div>';
        
        row += '<div class="input text" id="paramValue"><label for="params value">Value:</label><input type="text" name="data[RetailerParam][value][]" placeholder="Parameter Value" /></div>';
        
        row += '<div class="del"><a href="javascript:void(0);" onclick=delRow('+i+');><img src="<?php echo BASE_URL;?>img/delete.png" /></a></div></div>';
        
        $('.retailerParam').append(row);     
        e.preventDefault();
        i++;
   });
});

function delRow(id){
    if(confirm('You are about to remove ?')){
        $('#cont'+id).remove();
    }
}
</script>