<?php ?>
<div class="form">
    <?php 
    $url = array('controller' => 'subcategory', 'action' => 'add');
    
    echo $this->Form->create('SubCategory', array(
	'type' => 'file', 'action' => 'add', 'autocomplete' => 'off', 'url' => $url, 'novalidate' => true,
	'inputDefaults' => array('separator' => ' ')
)); ?>
        <fieldset>
            <legend>Add Sub Category</legend>
            
            <?php 
            $options = array();
            $options[''] = 'Select Category';
            foreach($categories as $cate){
                $options[$cate['Category']['id']] = $cate['Category']['name']; 
            }
            
            echo $this->Form->input('category_id', array('options'=>$options,'class'=>'select'));
            echo $this->Form->input('name');
            ?>
            
            
<!--            <div id="brwCategoryName" class="input text">
                <label for="CategoryName">Name</label>
                <input type="text" id="CategoryName" maxlength="255" separator=" " name="data[Category][name]">
            </div>-->
            
        </fieldset>

        <fieldset>
            <div class="input select"><label for="ContentAfterSave">After save</label>
                <select id="ContentAfterSave" separator=" " name="data[SubCategory][after_save]">
                    <option selected="selected" value="referer">Back to where I was</option>
                    <option value="view">View saved Category</option>
                    <option value="add">Add another Category</option>
                    <option value="index">Go to the index of all Categories</option>
                    <option value="edit">Continue editing this Category</option>
                    <option value="home">Go home</option>
                </select>
            </div>
        </fieldset>
        
<!--        <input type="hidden" id="ContentReferer" value="http://localhost/cakephp264/admin/contents/index/Category" name="data[SubCategory][referer]">
        <div class="submit">
            <input type="submit" value="Save">
            <a class="cancel" href="/cakephp264/admin/brownie/index">Cancel</a>
        </div>

    </form>-->
    


<?php echo $this->Form->hidden('referer', array('value'=> Router::url(array('controller' => 'subcategory', 'action' => 'index'))));?>
<div class="submit">
	<input type="submit" value="<?php echo __d('brownie', 'Save') ?>" />
	<a href="<?php echo Router::url(array('controller' => 'subcategory', 'action' => 'index')); ?>" class="cancel">Cancel</a>
</div>

<?php echo $this->Form->end(); ?>
    
</div>
