<?php
//pr($neighbors);
$Controller = $this->params['controller'];
?>

<div id="Category_view" class="view">
    <div class="clearfix">
        <h1>Sub Category</h1>
        <div class="actions-view">
            <ul class="actions neighbors">
                <?php
                    if (!empty($neighbors['prev'])) {
                            echo '
                            <li class="prev">
                                    ' . $this->Html->link('Previous',
                                    array('action' => 'view', $neighbors['prev'][$model]['id']) + $this->params['named'],
                                    array('title' => __d('brownie', 'Previous'))).'
                            </li>';
                    }
                    if (!empty($neighbors['next'])) {
                            echo '
                            <li class="next">
                                    ' . $this->Html->link(__d('brownie', 'Next'),
                                    array('action' => 'view', $neighbors['next'][$model]['id']) + $this->params['named'],
                                    array('title' => __d('brownie', 'Next'))).'
                            </li>';
                    }
                    ?>
                    <?php
                    if (!empty($this->params['named']['back_to'])) {
                            $backToUrl = array('plugin' => 'brownie', 'controller' => 'contents');
                            $named = $this->params['named'];
                            $back_to = $named['back_to'];
                            unset($named['back_to']);
                            switch($back_to) {
                                    case 'index':
                                            $backToUrl += array('action' => 'index', $model) + $named;
                                    break;
                            }
                            echo '
                            <li class="back">
                                    ' . $this->Html->link(__d('brownie', 'Back'), $backToUrl, array('title' => __d('brownie', 'Back'))) . '
                            <li>';
                    } else{
                        echo '
                            <li class="back">
                                    ' . $this->Html->link('Back', array('controller'=>$Controller,'action'=>'index'), array('title' => __d('brownie', 'Back'))) . '
                            <li>';
                    }
                    ?>
                
            </ul>
            <ul class="actions">
                <li class="add">
                    <!--<a title="Add" href="/cakephp264/admin/contents/edit/Category/after_save:view">Add</a>	</li>-->            <?php echo $this->Html->link('Add', array('controller'=>$Controller, 'action'=>'add', 'after_save:view')); ?>
                <li class="edit">
                    <!--<a title="Edit" href="/cakephp264/admin/contents/edit/Category/1">Edit</a>-->	
                    <?php echo $this->Html->link('Edit', array('controller'=>$Controller, 'action'=>'edit', $datas['SubCategory']['id'])); ?>
                </li>
                <li class="delete">
                <?php echo $this->Html->link('Delete', array('controller'=>$Controller, 'action'=>'delete', $datas['SubCategory']['id'],'after_delete:parent'), array('confirm'=>'Are you sure you want to delete this ?')); ?>
                </li>
                <li class="index">
                    <!--<a title="List all" href="/cakephp264/admin/contents/index/Category">List all</a>	</li>-->
                    <?php echo $this->Html->link('List all', array('controller'=>$Controller, 'action'=>'index')); ?>
            </ul>
        </div>

        <table class="view">

            <tbody>
                <tr>
                    <td class="label">Id</td>
                    <td class="fcktxt"><?php echo $datas['SubCategory']['id'];?></td>
                </tr>
                <tr>
                    <td class="label">Name</td>
                    <td class="fcktxt"><?php echo $datas['SubCategory']['name'];?></td>
                </tr>
                <tr>
                    <td class="label">Category Name</td>
                    <td class="fcktxt"><?php echo $datas['Category']['name'];?></td>
                </tr>
                               
            </tbody>
        </table>
    </div>

    <div class="brw-images index">
    </div>
    <div class="brw-files index">
    </div>

</div>