<?php 
//pr($subcategories);
//pr($this->params);
$paginator = $this->Paginator;
$url = array('controller' => 'subcategory', 'action' => 'delete_multiple');
?>
<div id="Category_index" class="model-index">

    <div class="index clearfix">
        <h2>Sub Categories</h2>
        <div class="actions">
            <ul>
                <li class="add"><?php echo $this->Html->link('Add Subcategory', array('action'=>'add'));?></li>
            </ul>
        </div>
    </div>

    <?php if(empty($subcategories)){ ?>
    <p class="norecords">There are no <?php echo ucfirst($this->params['controller']);?></p>
    <?php }else{ ?>
    <!--<form accept-charset="utf-8" method="post" id="deleteMultiple" action="/cakephp264/admin/contents/delete_multiple/Category">-->
    <?php echo $this->Form->create('SubCategory', array('url'=>$url, 'id'=>'deleteMultiple')); ?>

    <table id="index">
        <tbody>
            <tr>
                <th class="delete_multiple">
                    <input type="checkbox" title="Select/Unselect all" id="deleteCheckAll" style="visibility: visible;">
                </th>
                <th class="id number"><?php echo $paginator->sort('id','Id');?></th>
                <th class="name string"><?php echo $paginator->sort('id','name');?></th>
                <th><a href="#">Category</a></th>
                <th class="actions">Actions</th>
            </tr>

            <?php foreach($subcategories as $subcat){ ?>

            <tr class="list">
                <td class="delete_multiple">
                    <input type="checkbox" value="<?php echo $subcat['SubCategory']['id'];?>" name="data[<?php echo $model;?>][id][]">
                    
                </td>
                <td class="id number field">
                    <?php echo $subcat['SubCategory']['id'];?>
                </td>
                <td class="name string field">
                    <?php echo $subcat['SubCategory']['name'];?>
                </td>
                <td class="name string field">
                    <?php echo $subcat['Category']['name'];?>
                </td>
                <td class="actions">
                    <ul class="actions">
                        <li class="view"><?php echo $this->Html->link("View", array('action'=>'view',$subcat['SubCategory']['id']));?></li>
                        <li class="edit"><?php echo $this->Html->link("Edit", array('action'=>'edit',$subcat['SubCategory']['id']));?></li>
                        <li class="delete"><?php echo $this->Form->postLink( "Delete", array('action' => 'delete', $subcat['SubCategory']['id']), array('escape' => false, 'confirm' => __('Are you sure you want to delete this ?'))); ?>
                            
                        </li>
                    </ul>
                </td> 
            </tr>
            <?php } ?>
        </tbody>
    </table>
        <!--<div class="submit"><button><span>Delete selected</span></button></div>-->
        <?php $options = array('label'=>'Delete selected','class'=>'delete-selected'); ?>
        <?php echo $this->Form->end($options);?>
    
<!--        <div class="pagination clearfix"><div class="paging_counter">
                <p class="counter">Page 1 of 1, showing 1 Categories out of 1 total, starting on record 1, ending on 1</p></div></div>-->

<?php //echo $this->element('sql_dump');?>

<?php // pagination section
$brwConfig['names']['plural'] = 'Show';
$brwConfig['paginate']['limit'] = 20;
echo $this->element('pagination', array('model' => 'SubCategory', 'brwConfig' => $brwConfig));
/*    echo "<div class='paging'>";
 
        // the 'first' page button
        echo $paginator->first("First");
         
        // 'prev' page button, 
        // we can check using the paginator hasPrev() method if there's a previous page
        // save with the 'next' page button
        if($paginator->hasPrev()){
            echo $paginator->prev("Prev");
        }
         
        // the 'number' page buttons
        echo $paginator->numbers(array('modulus' => 2));
         
        // for the 'next' button
        if($paginator->hasNext()){
            echo $paginator->next("Next");
        }
         
        // the 'last' page button
        echo $paginator->last("Last");
     
    echo "</div>"; */
    ?>

<?php }/*empty else*/ ?>  
</div>