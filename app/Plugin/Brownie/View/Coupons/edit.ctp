<?php

$Controller = $this->params['controller'];
$Action = $this->params['action'];
$url = array('controller' => $Controller, 'action' => $Action);
?>

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<div class="form">
    <?php 
    if(empty($data)){
        echo '<p class="flash  flash_error">There is no '.$model.' found.</p>';
    }else{    
    
    echo $this->Form->create($model, array('type' => 'file', 'autocomplete' => 'off', 'novalidate' => true, 'inputDefaults' => array('separator' => ' '))); ?>
    <fieldset>
        <legend><?php echo __("Edit ".$model);?></legend>

            <?php 
            //pr($data);            
            $options = array();
            $options[''] = 'Select Retailer';
            foreach($retailers as $retailer){
                $options[$retailer['Retailer']['id']] = $retailer['Retailer']['name']; 
            }
            
            echo $this->Form->input('retailer_id', array('options'=>$options,'class'=>'select'));
            
            $option = array(''=>'--Select--');
            if(!empty($data['SubCategory']['id'])){
                $option[$data['SubCategory']['id']] = $data['SubCategory']['name'];
            }
            echo $this->Form->input('subCateId',array('options'=>$option,'label'=>'Sub Category','class'=>'select'));
            
            echo $this->Form->input('title');
            
            echo $this->Form->input('description');
            
            echo $this->Form->input('cashback', array('placeholder'=>'10','label'=>'Cashback(%)'));
            
            echo $this->Form->input('code'); 
            
            echo $this->Form->input('expire', array('type'=>'text','id'=>'expDate','label'=>'Expiry Date')); 
            
            echo $this->Form->input('url', array('lable'=>'Tracking Url'));
                       
            $option2 = array('0'=>'Inactive','1'=>'Active');
            echo $this->Form->input('status', array('options'=>$option2, 'class'=>'select'));
            
            
            ?>

    </fieldset>

    <fieldset>
            <?php 
            $saveOption = array(
                'referer'=>'Back to where I was',
                'view'=>'View saved '.$model,
                'add'=>'Add another '.$model,
                'index'=>'Go to the index of all '.$Controller,
                'edit'=>'Continue editing this '.$model,
                'home'=>'Go home',
            )
            ?>
        <div class="input select">
            <!--<label for="ContentAfterSave">After save</label>
            <select id="ContentAfterSave" separator=" " name="data[SubCategory][after_save]">
                <option selected="selected" value="referer">Back to where I was</option>
                <option value="view">View saved Category</option>
                <option value="add">Add another Category</option>
                <option value="index">Go to the index of all Categories</option>
                <option value="edit">Continue editing this Category</option>
                <option value="home">Go home</option>
            </select>-->
                <?php echo $this->Form->input('after_save', array('options'=>$saveOption, 'separator'=> " ")); ?>
        </div>
    </fieldset>

<?php echo $this->Form->hidden('referer', array('value'=> Router::url(array('controller' => $Controller, 'action' => 'index'))));?>
    <div class="submit">
        <input type="submit" value="<?php echo __d('brownie', 'Save') ?>" />

        <?php echo $this->Html->link('Cancel', array('controller'=>$Controller,'action'=>'index'), array('class'=>'cancel'));?>
    </div>

<?php echo $this->Form->end(); ?>

    <?php } ?>
</div>

<script>
    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: "<?php echo BASE_URL;?>admin/Subcategory/getCategory",
            data: {'ret_id': <?php echo $data['Coupon']['retailer_id'];?>},
            success: function (res) {
                $('#CouponSubCateId').html(res);
                $('#CouponSubCateId').val(<?php echo $data['SubCategory']['id'];?>)
            }
        });

        $('#CouponRetailerId').change(function (e) {
            var val = $(this).val();
            //alert(val);
            if (val != '') {
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASE_URL;?>admin/Subcategory/getCategory",
                    data: {'ret_id': val},
                    success: function (res) {
                        $('#CouponSubCateId').html(res);
                    }
                });
            } else {
                $('#CouponSubCateId').html('');
            }
        });
        
        // DATEPICKER 
         $( "#expDate" ).datepicker({
            minDate: 2,
            dateFormat: 'yy-mm-dd'
         });
    })
</script>