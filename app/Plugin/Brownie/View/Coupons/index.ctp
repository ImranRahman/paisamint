<?php 
//pr($datas); 
//pr($this->params);
$Controller = $this->params['controller'];
$url = array('controller' => $Controller, 'action' => 'delete_multiple');

$paginator = $this->Paginator;
?>

<style>
    .csv, .xml{ display: none; padding: 15px;}
    .input{ margin-bottom: 10px;}
    .error{ color: red;}
</style>
<div id="Category_index" class="model-index">

    <div class="index clearfix">
        <h2><?php echo __(ucfirst($Controller));?></h2>
        <div class="actions">
            <ul>
                <li class="add">
                    <?php echo $this->Html->link('Add '.$model, array('action'=>'add'));?>
                </li>
                <li class="add_csv">
                    <?php echo $this->Html->link('Import CSV', array('action'=>'#'), array('id'=>'csv'));?>
                </li>
                <li class="add_xml">
                    <?php echo $this->Html->link('Import XML', array(), array('id'=>'xml'));?>
                </li>
            </ul>
        </div>
    </div>

    <div class="csv">
        <form method="post" action="<?php echo BASE_URL_ADMIN;?>coupons/csv" enctype="multipart/form-data">
            <label for="csv">Select CSV File</label>
            <input type="file" name="file" id="csv-file" class="input">
            <input type="submit" id="csv-btn" value="Upload">
        </form>
    </div>
    <div class="xml">
        <form method="post" action="<?php echo BASE_URL_ADMIN;?>coupons/xml" enctype="multipart/form-data">
            <label for="xml">Select XML File</label>
            <input type="file" name="file" id="xml-file" class="input">
            <input type="submit" id="xml-btn" value="Upload">
        </form>
    </div>
    
    
    <?php if(empty($datas)){ ?>
    <p class="norecords">There are no <?php echo ucfirst($Controller);?></p>
    <?php }else{ ?>
    
    <?php echo $this->Form->create($model, array('url'=>$url, 'id'=>'deleteMultiple')); ?>

    <table id="index">
        <tbody>
            <tr>
                <th class="delete_multiple">
                    <input type="checkbox" title="Select/Unselect all" id="deleteCheckAll" style="visibility: visible;">
                </th>
                <th class="id number"><?php echo $paginator->sort('id','Id');?></th>                  <th><a href="#"><?php echo __('Retailer');?></a></th>
                <th class="name string"><?php echo $paginator->sort('title','Coupon Title');?></th>                
                <th class="name string"><?php echo $paginator->sort('cashback','Cashback');?></th>
                <th class="name string"><?php echo $paginator->sort('code','Coupon Code');?></th>
                <th class="actions"><?php echo __('Actions');?></th>
            </tr>

            <?php foreach($datas as $data){ ?>

            <tr class="list">
                <td class="delete_multiple">
                    <input type="checkbox" value="<?php echo $data[$model]['id'];?>" name="data[<?php echo $model;?>][id][]">
                </td>
                
                <td class="id number field">
                    <?php echo $data[$model]['id'];?>
                </td>
                
                <td class="name string field">
                    <?php echo $data['Retailer']['name'];?>
                </td>
                
                <td class="name string field">
                    <?php echo $data[$model]['title'];?>
                </td>
                
                <td class="name string field">
                    <?php echo $data[$model]['cashback'].'%';?>
                </td>
                
                <td class="name string field">
                    <?php echo $data[$model]['code'];?>
                </td>
                
                <td class="actions">
                    <ul class="actions">
                        <li class="view"><?php echo $this->Html->link("View", array('action'=>'view',$data[$model]['id']));?></li>
                        <li class="edit"><?php echo $this->Html->link("Edit", array('action'=>'edit',$data[$model]['id']));?></li>
                        <li class="delete"><?php echo $this->Form->postLink( "Delete", array('action' => 'delete', $data[$model]['id']), array('escape' => false, 'confirm' => __('Are you sure you want to delete this ?'))); ?>
                            
                        </li>
                    </ul>
                </td> 
            </tr>
            <?php } ?>
        </tbody>
    </table>
        <!--<div class="submit"><button><span>Delete selected</span></button></div>-->
    <?php $options = array('label'=>'Delete selected','class'=>'delete-selected'); ?>
    <?php echo $this->Form->end($options);?>
    
<?php //echo $this->element('sql_dump');?>

<?php 
// pagination section
$brwConfig['names']['plural'] = 'Show';
$brwConfig['paginate']['limit'] = 20;
echo $this->element('pagination', array('model' => $model, 'brwConfig' => $brwConfig));
?>
    
<?php }/*empty else*/ ?>    
</div>

<script>
    $(document).ready(function(){
       $('#csv').click(function(e){
           $('.xml').hide();
           $('.csv').toggle();
           e.preventDefault();
       });
       
       
       $('#xml').click(function(e){
           $('.csv').hide();
           $('.xml').toggle();
           e.preventDefault();
       });
       
       
       $('#csv-btn').click(function(){
          $('.error').remove();
          if($('#csv-file').val() == ''){
              $('#csv-file').after('<span class="error">Please select file!<br></span>');
              return false;
          } 
       });
       $('#xml-btn').click(function(){
          $('.error').remove();
          if($('#xml-file').val() == ''){
              $('#xml-file').after('<span class="error">Please select file!<br></span>');
              return false;
          } 
       });
    });    
</script>