<?php 
$Controller = $this->params['controller'];
$Action = $this->params['action'];
$url = array('controller' => $Controller, 'action' => $Action);
?>

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<div class="form">
    <?php 
        
    echo $this->Form->create($model, array('type' => 'file', 'action' => $Action, 'autocomplete' => 'off', 'url' => $url, 'novalidate' => true, 'inputDefaults' => array('separator' => ' '))); ?>
    
    
        <fieldset>
            <legend><?php echo __("Add ".$model);?></legend>
            
            <?php 
                        
            $options = array();
            $options[''] = 'Select Retailer';
            foreach($retailers as $retailer){
                $options[$retailer['Retailer']['id']] = $retailer['Retailer']['name']; 
            }
            
            echo $this->Form->input('retailer_id', array('options'=>$options,'class'=>'select'));
            
            $option = array(''=>'--Select--');
            echo $this->Form->input('subCateId',array('options'=>$option,'label'=>'Sub Category','class'=>'select'));
             
            echo $this->Form->input('title');
            
            echo $this->Form->input('description');
            
            echo $this->Form->input('cashback', array('placeholder'=>'10','label'=>'Cashback(%)'));
            
            echo $this->Form->input('code');
            
            echo $this->Form->input('expire', array('type'=>'text','id'=>'expDate','label'=>'Expiry Date'));
            
            echo $this->Form->input('url', array('lable'=>'Tracking Url'));
            ?>
           
        </fieldset>

        <fieldset>
            <?php 
            $saveOption = array(
                'referer'=>'Back to where I was',
                'view'=>'View saved '.$model,
                'add'=>'Add another '.$model,
                'index'=>'Go to the index of all '.$Controller,
                'edit'=>'Continue editing this '.$model,
                'home'=>'Go home',
            )
            ?>
            <div class="input select">                
                <?php echo $this->Form->input('after_save', array('options'=>$saveOption, 'separator'=> " ")); ?>
            </div>
        </fieldset>
        
<?php echo $this->Form->hidden('referer', array('value'=> Router::url(array('controller' => $Controller, 'action' => 'index'))));?>
<div class="submit">
	<input type="submit" value="<?php echo __d('brownie', 'Save') ?>" />

        <?php echo $this->Html->link('Cancel', array('controller'=>$Controller,'action'=>'index'), array('class'=>'cancel'));?>
</div>

<?php echo $this->Form->end(); ?>
    
</div>

<script>
    $(document).ready(function(){
        
        $('#CouponRetailerId').change(function(e){
            var val = $(this).val();
            //alert(val);
            if(val != ''){
                $.ajax({
                   type:"POST",
                   url:"<?php echo BASE_URL;?>admin/Subcategory/getCategory",
                   data:{ 'ret_id':val},
                   success:function(res){
                        $('#CouponSubCateId').html(res);
                    }
                });
            }else{
                $('#CouponSubCateId').html('');
            }
        });
    
        // DATEPICKER 
         $( "#expDate" ).datepicker({
            minDate: 2,
            dateFormat: 'yy-mm-dd'
         });
    });
</script>
