<?php
$Controller = $this->params['controller'];
$Action = $this->params['action'];
$url = array('controller' => $Controller, 'action' => $Action);
//pr($this->request->data);
?>

<style>
#cke_1_contents{ height: 600px !important;} 
</style>

<div class="form">
    <?php 
    if(empty($data)){
        echo '<p class="flash  flash_error">There is no '.$model.' found.</p>';
    }else{    
    
    echo $this->Form->create($model, array('type' => 'file', 'autocomplete' => 'off', 'novalidate' => true, 'inputDefaults' => array('separator' => ' '))); ?>
    <fieldset>
        <legend><?php echo __("Edit ".$model);?></legend>

            <?php 
            
            echo $this->Form->input('', array('disabled'=>'disabled', 'value'=>$data[$model]['title'],'type'=>'text'));
            
            echo $this->Form->input('content', array('class'=>'richEditor'));
            
            $options = array('1'=>'Active','2'=>'Inactive');
            echo $this->Form->input('status', array('options'=>$options,'class'=>'select')); 
                                           
            
            ?>

    </fieldset>

    <fieldset style="display: none;">
            <?php 
            $saveOption = array(
                'referer'=>'Back to where I was',
                'view'=>'View saved '.$model,
                'add'=>'Add another '.$model,
                'index'=>'Go to the index of all '.$Controller,
                'edit'=>'Continue editing this '.$model,
                'home'=>'Go home',
            )
            ?>
        <div class="input select">
            <?php echo $this->Form->input('after_save', array('options'=>$saveOption, 'separator'=> " ")); ?>
        </div>
    </fieldset>

<?php echo $this->Form->hidden('referer', array('value'=> Router::url(array('controller' => $Controller, 'action' => 'index'))));?>
    <div class="submit">
        <input type="submit" value="<?php echo __d('brownie', 'Save') ?>" />

        <?php echo $this->Html->link('Cancel', array('controller'=>$Controller,'action'=>'index'), array('class'=>'cancel'));?>
    </div>

<?php echo $this->Form->end(); ?>

    <?php } ?>
</div>

<script>
    $(document).ready(function(){
        // DATEPICKER 
         $( "#crdate" ).datepicker({
            minDate: 2,
            dateFormat: 'yy-mm-dd'
         });
    });
</script>