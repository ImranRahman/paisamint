<?php 
$Controller = $this->params['controller'];
$url = array('controller' => $Controller, 'action' => 'delete_multiple');

$paginator = $this->Paginator;
?>

<div id="Category_index" class="model-index">

    <div class="index clearfix">
        <h2><?php echo __(ucfirst($Controller));?></h2>
        <div class="actions">
            <ul>
<!--                <li class="add">
                    <?php //echo $this->Html->link('Add '.$model, array('action'=>'add'));?>
                </li>-->                
            </ul>
        </div>
    </div>
      
    
    <?php if(empty($datas)){ ?>
    <p class="norecords">There are no <?php echo ucfirst($Controller);?></p>
    <?php }else{ $sn=0; ?>
    
    <?php echo $this->Form->create($model, array('url'=>$url, 'id'=>'deleteMultiple')); ?>

    <table id="index">
        <tbody>
            <tr>
<!--                <th class="delete_multiple" width="5%">
                    <input type="checkbox" title="Select/Unselect all" id="deleteCheckAll" style="visibility: visible;">
                </th>-->
                <th class="id number" width="5%">
                    <?php echo '<a href="#">S.N.</a>';?>
                </th>                     
                
                <th class="name string" width="60%">
                    <?php echo $paginator->sort('title','Page Title');?>
                </th>                
                
                <th class="name string" width="20%">
                    <?php echo $paginator->sort('status','Status');?>
                </th>
                <th class="actions" width="15%"><?php echo __('Actions');?></th>
            </tr>

            <?php foreach($datas as $data){ ?>

            <tr class="list">

                
                <td class="id number field">
                    <?php echo ++$sn;?>
                </td>
                
                
                
                <td class="name string field">
                    <?php echo ucwords($data[$model]['title']);?>
                </td>
                                
                
                <td class="name string field">
                    <?php echo ($data[$model]['status']==1)?'Active':'Inactive';?>
                </td>
                
                
                
                <td class="actions">
                    <ul class="actions">
                        
                        <li class="edit"><?php echo $this->Html->link("View & Edit", array('action'=>'edit',$data[$model]['id']));?></li>                           
                    </ul>
                </td> 
            </tr>
            <?php } ?>
        </tbody>
    </table>
        <!--<div class="submit"><button><span>Delete selected</span></button></div>-->
    <?php $options = array('label'=>'Delete selected','class'=>'delete-selected'); ?>
    <?php echo $this->Form->end();?>
    
<?php //echo $this->element('sql_dump');?>

<?php 
// pagination section
$brwConfig['names']['plural'] = 'Show';
$brwConfig['paginate']['limit'] = 20;
echo $this->element('pagination', array('model' => $model, 'brwConfig' => $brwConfig));
?>
    
<?php }/*empty else*/ ?>    
</div>

<script>
    $(document).ready(function(){
       $('#csv').click(function(e){
           $('.xml').hide();
           $('.csv').toggle();
           e.preventDefault();
       });
       
       
       $('#xml').click(function(e){
           $('.csv').hide();
           $('.xml').toggle();
           e.preventDefault();
       });
       
       
       $('#csv-btn').click(function(){
          $('.error').remove();
          if($('#csv-file').val() == ''){
              $('#csv-file').after('<span class="error">Please select file!<br></span>');
              return false;
          } 
       });
       $('#xml-btn').click(function(){
          $('.error').remove();
          if($('#xml-file').val() == ''){
              $('#xml-file').after('<span class="error">Please select file!<br></span>');
              return false;
          } 
       });
    });    
</script>