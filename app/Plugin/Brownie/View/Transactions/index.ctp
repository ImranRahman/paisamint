<?php 
$Controller = $this->params['controller'];
$url = array('controller' => $Controller, 'action' => 'delete_multiple');

$paginator = $this->Paginator;
?>

<div id="Category_index" class="model-index">

    <div class="index clearfix">
        <h2><?php echo __(ucfirst($Controller));?></h2>
        <div class="actions">
            <ul>
<!--                <li class="add">
                    <?php echo $this->Html->link('Add '.$model, array('action'=>'add'));?>
                </li>-->                
            </ul>
        </div>
    </div>
      
    
    <?php if(empty($datas)){ ?>
    <p class="norecords">There are no <?php echo ucfirst($Controller);?></p>
    <?php }else{ ?>
    
    <?php echo $this->Form->create($model, array('url'=>$url, 'id'=>'deleteMultiple')); ?>

    <table id="index">
        <tbody>
            <tr>
<!--                <th class="delete_multiple" width="5%">
                    <input type="checkbox" title="Select/Unselect all" id="deleteCheckAll" style="visibility: visible;">
                </th>-->
                <th class="id number" width="5%">
                    <?php echo $paginator->sort('id','Id');?>
                </th>                     
                <th class="name string" width="18%">
                    <a href="#"><?php echo __('User');?></a>
                </th>
                <th class="name string" width="17%">
                    <?php echo $paginator->sort('order_id','Order Id');?>
                </th>                
                <th class="name string" width="15%">
                    <?php echo $paginator->sort('type','Transaction Type');?>
                </th>
                <th class="id number" width="10%">
                    <?php echo $paginator->sort('amount','Amount');?>
                </th>
                <th class="name string" width="10%">
                    <?php echo $paginator->sort('status','Status');?>
                </th>
                <th class="name string" width="15%">
                    <?php echo $paginator->sort('created','Date & Time');?>
                </th>
                <th class="actions" width="10%"><?php echo __('Actions');?></th>
            </tr>

            <?php foreach($datas as $data){ ?>

            <tr class="list">
<!--                <td class="delete_multiple">
                    <input type="checkbox" value="<?php echo $data[$model]['id'];?>" name="data[<?php echo $model;?>][id][]">
                </td>-->
                
                <td class="id number field">
                    <?php echo $data[$model]['id'];?>
                </td>
                
                <td class="name string field">
                    <?php echo $data['User']['name'];?>
                </td>
                
                <td class="name string field">
                    <?php echo $data[$model]['order_id'];?>
                </td>
                
                <td class="name string field">
                    <?php echo $data[$model]['type'];?>
                </td>
                
                <td class="id number field">
                    <?php echo $data[$model]['amount'];?>
                </td>
                
                <td class="name string field">
                    <?php echo $data[$model]['status'];?>
                </td>
                
                <td class="name string field">
                    <?php echo date('m/d/Y h:i', strtotime($data[$model]['created']));?>
                </td>
                
                <td class="actions">
                    <ul class="actions">
                        
                        <li class="edit"><?php echo $this->Html->link("Edit", array('action'=>'edit',$data[$model]['id']));?></li>                        
                    </ul>
                </td> 
            </tr>
            <?php } ?>
        </tbody>
    </table>
        <!--<div class="submit"><button><span>Delete selected</span></button></div>-->
    <?php $options = array('label'=>'Delete selected','class'=>'delete-selected'); ?>
    <?php echo $this->Form->end();?>
    
<?php //echo $this->element('sql_dump');?>

<?php 
// pagination section
$brwConfig['names']['plural'] = 'Show';
$brwConfig['paginate']['limit'] = 20;
echo $this->element('pagination', array('model' => $model, 'brwConfig' => $brwConfig));
?>
    
<?php }/*empty else*/ ?>    
</div>

<script>
    $(document).ready(function(){
       $('#csv').click(function(e){
           $('.xml').hide();
           $('.csv').toggle();
           e.preventDefault();
       });
       
       
       $('#xml').click(function(e){
           $('.csv').hide();
           $('.xml').toggle();
           e.preventDefault();
       });
       
       
       $('#csv-btn').click(function(){
          $('.error').remove();
          if($('#csv-file').val() == ''){
              $('#csv-file').after('<span class="error">Please select file!<br></span>');
              return false;
          } 
       });
       $('#xml-btn').click(function(){
          $('.error').remove();
          if($('#xml-file').val() == ''){
              $('#xml-file').after('<span class="error">Please select file!<br></span>');
              return false;
          } 
       });
    });    
</script>