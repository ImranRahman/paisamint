<?php
$Controller = $this->params['controller'];
$Action = $this->params['action'];
$url = array('controller' => $Controller, 'action' => $Action);
//pr($this->request->data);
?>

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<div class="form">
    <?php 
    if(empty($data)){
        echo '<p class="flash  flash_error">There is no '.$model.' found.</p>';
    }else{    
    
    echo $this->Form->create($model, array('type' => 'file', 'autocomplete' => 'off', 'novalidate' => true, 'inputDefaults' => array('separator' => ' '))); ?>
    <fieldset>
        <legend><?php echo __("Edit ".$model);?></legend>

            <?php 
            //pr($data);            
            $options = array();
            $options[''] = 'Select User';
            foreach($users as $user){
                $options[$user['User']['id']] = $user['User']['name']; 
            }
            
            echo $this->Form->input('user_id', array('options'=>$options,'class'=>'select'));       
            echo $this->Form->input('order_id', array('type'=>'text'));
            
            $typeArr = array(
                'recharge'=>'Recharge',
                'cashback'=>"Cashback"
            );
            echo $this->Form->input('type',array('options'=>$typeArr,'class'=>'select'));
            
            echo $this->Form->input('amount');
            
            echo $this->Form->input('status'); 
            
            $date = date('Y-m-d h:i', strtotime($this->request->data['Transaction']['created']));
            echo $this->Form->input('created', array('type'=>'text','id'=>'crdate','label'=>'Date & Time', 'value'=>$date)); 
                       
            
            ?>

    </fieldset>

    <fieldset style="display: none;">
            <?php 
            $saveOption = array(
                'referer'=>'Back to where I was',
                'view'=>'View saved '.$model,
                'add'=>'Add another '.$model,
                'index'=>'Go to the index of all '.$Controller,
                'edit'=>'Continue editing this '.$model,
                'home'=>'Go home',
            )
            ?>
        <div class="input select">
            <?php echo $this->Form->input('after_save', array('options'=>$saveOption, 'separator'=> " ")); ?>
        </div>
    </fieldset>

<?php echo $this->Form->hidden('referer', array('value'=> Router::url(array('controller' => $Controller, 'action' => 'index'))));?>
    <div class="submit">
        <input type="submit" value="<?php echo __d('brownie', 'Save') ?>" />

        <?php echo $this->Html->link('Cancel', array('controller'=>$Controller,'action'=>'index'), array('class'=>'cancel'));?>
    </div>

<?php echo $this->Form->end(); ?>

    <?php } ?>
</div>

<script>
    $(document).ready(function(){
        // DATEPICKER 
         $( "#crdate" ).datepicker({
            minDate: 2,
            dateFormat: 'yy-mm-dd'
         });
    });
</script>