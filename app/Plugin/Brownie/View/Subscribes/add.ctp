<?php
$Controller = $this->params['controller'];
$Action = $this->params['action'];
$url = array('controller' => $Controller, 'action' => $Action);
//pr($this->request->data);
?>

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<div class="form">
    <?php 
       
    echo $this->Form->create($model, array('type' => 'file', 'autocomplete' => 'off', 'novalidate' => true, 'inputDefaults' => array('separator' => ' '))); ?>
    <fieldset>
        <legend><?php echo __("Add ".$model);?></legend>

            <?php 
            //pr($data);            
                   
            echo $this->Form->input('email');
            
            $statusArr = array(
                '1'=>'Verified',
                '0'=>"Not Verified"
            );
            echo $this->Form->input('status',array('options'=>$statusArr,'class'=>'select'));
            ?>

    </fieldset>

    <fieldset style="display: none;">
            <?php 
            $saveOption = array(
                'referer'=>'Back to where I was',
                'view'=>'View saved '.$model,
                'add'=>'Add another '.$model,
                'index'=>'Go to the index of all '.$Controller,
                'edit'=>'Continue editing this '.$model,
                'home'=>'Go home',
            )
            ?>
        <div class="input select">
            <?php echo $this->Form->input('after_save', array('options'=>$saveOption, 'separator'=> " ")); ?>
        </div>
    </fieldset>

<?php echo $this->Form->hidden('referer', array('value'=> Router::url(array('controller' => $Controller, 'action' => 'index'))));?>
    <div class="submit">
        <input type="submit" value="<?php echo __d('brownie', 'Save') ?>" />

        <?php echo $this->Html->link('Cancel', array('controller'=>$Controller,'action'=>'index'), array('class'=>'cancel'));?>
    </div>

<?php echo $this->Form->end(); ?>

</div>

<script>
    $(document).ready(function(){
        // DATEPICKER 
         $( "#crdate" ).datepicker({
            minDate: 2,
            dateFormat: 'yy-mm-dd'
         });
    });
</script>