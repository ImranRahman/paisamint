<?php

$Controller = $this->params['controller'];

?>

<style>
    /*.radio { float: left; margin-right: 20px; }*/
    .error{ color: red; font-size: 14px;}
</style>

<div class="form">
    <?php        
    echo $this->Form->create($model, array('type' => 'file', 'autocomplete' => 'off', 'novalidate' => true, 'inputDefaults' => array('separator' => ' '))); ?>
    <fieldset>
        <legend><?php echo __("Send Emails ");?></legend>

        <fieldset class="send-to-field">
            <legend><?php echo __("Send To:");?></legend>
            <div class="radio">
                <input type="radio" name="data[Subscribe][sendto]" value="1" id="subsuser1" class="checkRadio">
                <label for="subsuser1">Subscribed User</label>
            </div>
            <div class="radio">
                <input type="radio" name="data[Subscribe][sendto]" value="2" id="subsuser2" class="checkRadio">
                <label for="regis-user">Registered User</label>
            </div>
            <div class="send-to-error"></div>
        </fieldset>

        <?php 
        echo $this->Form->input('subject');
        
        echo $this->Form->input('content', array('class'=>'richEditor','type'=>'textarea'));
        //echo __('<strong>Note: Simply copy and paste html contents in source mode</strong>');
        ?>

    </fieldset>

    <?php 
    $option = array('id'=>'submit','label'=>'Send');
    echo $this->Form->end($option); ?>

</div>

<script>
    $(document).ready(function () {
        $('#submit').click(function () {
            $('.error').remove();
            var len = $('.checkRadio').length;
            var flag = 0;
            for (i = 1; i <= len; i++) {
                if ($('#subsuser' + i).prop("checked")) {
                    flag = 1;
                }
            }
            if (flag == 0) {
                $('.send-to-error').html('<span class="error">Please select a user type.</span>');
            }
            
            if($('#SubscribeSubject').val() == ''){
                $('#SubscribeSubject').after('<span class="error">Please enter email subject.</span>');
               flag = 0; 
            }
            
            if($('.cke_source').val() == ''){
                $('#SubscribeContent').after('<span class="error">Please enter email contents.</span>');
                flag = 0;
            }
            
            if(flag == 1){
                return true;
            }
            
            return false;
        });
    });
</script>

