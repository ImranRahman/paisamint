$(document).ready(function () {
    $('.searchBtn').click(function () {
        $('.error').remove();
        var val = $('.srch-form').val();
        if (val == "") {
            $('.main_search').after('<span class="error">Please enter keyword!</span>');
            $('.srch-form').focus();
            return false;
        }
        return true;
    });

    //.... Coupon Like Dislike JS ......
    $('.coup-likes').click(function (e) {
        var ID = $(this).attr('id');
        //console.log(ID);
        var baseUrl = $('#base-url').val();
        var user_sess = $('#user_session').val();

        if (user_sess) {
            $.ajax({
                type: "POST",
                url: baseUrl + "categories/setCouponLikes",
                data: {'id': ID},
                success: function (res) {
                    if (res != 'not done') {
                        var data = $.parseJSON(res);
                        if (data.status == 'like') {
                            $('#' + ID + ' i').css('color', '#0DE238');
                        } else {
                            $('#' + ID + ' i').css('color', 'red');
                        }
                        $('#like_' + data.id).removeClass('coup-likes');
                        $('#dislike_' + data.id).removeClass('coup-likes');
                        $('#' + ID + ' span').html(data.count);
                    }
                }
            });
        } else {
            $('#modal-launcher').click();
        }

        e.preventDefault();
    });

    // ######## Coupon Favorite JS ###########
    $('.coup-fav').click(function (e) {
        var ID = $(this).attr('id');
        console.log(ID);
        var baseUrl = $('#base-url').val();
        var user_sess = $('#user_session').val();

        if (user_sess) {
            $.ajax({
                type: "POST",
                url: baseUrl + "categories/setCouponFav",
                data: {'id': ID},
                success: function (res) {
                    if (res == 'Done') {
                        $('#' + ID + ' i').css('color', '#0DE238');
                    } else {
                        $('#' + ID + ' i').css('color', '');
                    }
                }
            });
        } else {
            $('#modal-launcher').click();
        }
        e.preventDefault();
    });

});


/*
 * ########## Get Coupon Code Popup Js ############
 */

function couponPopup(id) {
    var baseUrl = $('#base-url').val();
    var user_sess = $('#user_session').val();
    if (user_sess) {
        $.ajax({
            type: "POST",
            url: baseUrl + "homes/getCouponAjax",
            data: {'id': id},
            success: function (res) {
                var data = $.parseJSON(res);
                console.log(data['Retailer']['name']);
                $('.coupon-code').html(data['Coupon']['code']);
                $('.external').html(data['Retailer']['name']);
                $('.external').attr('href', baseUrl + 'coupons/load/' + data['Coupon']['id']);
                $('.coupon-title > h4').html(data['Coupon']['title']);
                $('.coupon-description').html(data['Coupon']['description']);
                $('.coupon-verified').html('Verified '+data['Coupon']['verified']);
                $('.coupon-end').html('Expires in '+data['Coupon']['expires']);

                $('#couponModal').modal('show');
            }
        });
    } else {
        $('#modal-launcher').click();
    }

}



/*
 * ------------ My Account Page JS ----------------------------
 */
//#### JS FOR SETTINGS TAB #####
$(document).ready(function () {
    $('.sett').click(function () {
        var ID = $(this).attr('id');
        $('.sett').removeClass('border active-step');
        $(this).addClass('border active-step');
        $('.formdiv').hide();
        $('#' + ID + '-div').show();
    });
});

//##### JS FOR RECHARGE ######
$(document).ready(function () {
    $('.otp-div-container').hide();
    $('#otp-btn').hide();
    $('#req-otp').hide();

    $('.btn-withdraw').click(function () {
        //$('#popup-withdraw').modal('show');
        $('#redeem_a').click();
    });

    $('.recharge-now').click(function (e) {
        var bal = $('#userBalance').val();
        bal = parseInt(bal);
        
        if(bal <= 10){
           $('#popup-balance').modal('show'); 
        }else{
            $('#popup-withdraw').modal('show');
        }
        e.preventDefault();
    });

    /*
     * .... Submit Recharge Form
     */
    $('#recharge-btn').click(function () {
        var operator = $('#operator').val();
        var servicenumber = $('#servicenumber').val();
        var amount = $('#amount').val();
        var flag = 0;

        $('#operator_error').html('');
        $('#mobno_error').html('');
        $('#amt_error').html('');

        if (operator == '') {
            $('#operator_error').html('Please select your operator!');
            flag = 1;
        }
        if (servicenumber == '') {
            $('#mobno_error').html('Please enter your number!');
            flag = 1;
        }
        if (servicenumber != '' && isNaN(servicenumber)) {
            $('#mobno_error').html('please enter Only number!');
            flag = 1;
        }
        if (servicenumber != '' && servicenumber.length != 10) {
            $('#mobno_error').html('please enter a 10 digit number!');
            flag = 1;
        }
        if (amount == '') {
            $('#amt_error').html('Please enter your amount!');
            flag = 1;
        }
        if (amount != '' && isNaN(amount)) {
            $('#amt_error').html('Please enter only decimal!');
            flag = 1;
        }

        if (flag == 0) {
            var baseUrl = $('#base-url').val();
            //$('#rech-form').submit();
            $.ajax({
                type: "POST",
                url: baseUrl + 'users/sendOTP',
                data: {'mobile':servicenumber},
                beforeSend: function (x) {

                },
                success: function (res) {
                    if(res == 'success'){                        
                        $('.rechargeDivContainer').hide();
                        $('#recharge-btn').hide();
                        $('.otp-div-container').show();
                        $('#otp-btn').show();
                        //$('#req-otp').show();
                    }
                }
            });
        }
    });

    /*
     * .... Submit OTP Form
     */
    $('#otp-btn').click(function (e) {
        var otpFlag = 0;
        $('#otpError').text('');
        $('#otpMsg').html('');
        var otp = $('#otp').val();
        if (otp == '') {
            $('#otpError').text('Please enter otp!');
            otpFlag = 1;
            return false;
        }
        if (otp.length != 6) {
            $('#otpError').text('Please enter 6 digits otp!');
            otpFlag = 1;
            return false;
        }
        
        if(otpFlag == 0){
            var operator = $('#operator').val();
            var servicenumber = $('#servicenumber').val();
            var amount = $('#amount').val();
            var otp = $('#otp').val();
            var user_id = $('#user_id').val();
            
            var baseUrl = $('#base-url').val();
            //$('#rech-form').submit();
            $.ajax({
                type: "POST",
                url: baseUrl + 'users/recharge',
                data: {'mobile':servicenumber,'operator':operator,'amount':amount,"otp":otp},
                beforeSend: function (x) {

                },
                success: function (res) {
                    //console.log(res);
                    if(res == 'time_exeeded'){
                        $('#otpMsg').html('<div class="alert alert-danger">Your OTP has been expired!</div>');
                    } else if(res == 'otp_error'){
                        $('#otpMsg').html('<div class="alert alert-danger">Incorrect OTP!</div>');
                    }else {
                        $('#otpMsg').html('<div class="alert alert-info">'+res+'</div>');
                    }
                }
            });
        }

    });
});


//##### JS for view all recharges in overview tab ######
$(function () {
    $('#viewall_recent').click(function () {
        var type = $(this).attr('type');
        if (type == 'viewall') {
            $('.recentFirst').hide();
            $('.recentAll').show();
            $(this).attr('type', 'viewone');
            $(this).html('Hide');
        } else {
            $('.recentAll').hide();
            $('.recentFirst').show();
            $(this).attr('type', 'viewall');
            $(this).html('View All');
        }
    });

    $('#paging a').click(function (e) {
        var href = $(this).attr('href');
        console.log(href);
        $.ajax({
            type: "POST",
            url: href,
            data: '',
            beforeSend: function (xhr) {
                $('#spin').show();
            },
            success: function (res) {
                $('body').html(res);
                $('#viewall_recent').trigger('click');
                $('.recentFirst').hide();
                $('.recentAll').show();
                $('#viewall_recent').attr('type', 'viewone');
                $('#viewall_recent').html('Hide');
                $('#spin').hide();
            }
        })

        e.preventDefault();
    });
});


/*
 * .... Js for referral tab ...................
 */
$(function () {
    $('.ref-back-Redeem-btn').click(function () {
        //$('.click_earn').position();
        $('html,body').animate({
            scrollTop: $(".click_earn").offset().top
        }, 'slow');
    });
});

function send_invite() {
    var baseUrl = $('#base-url').val();

    var val = $('#referFriend').val();
    $('#refError').html('');
    if (val == '') {
        $('#refError').html('<p class="alert alert-danger">Please enter an email address!</p>');
        $('#referFriend').focus();
        return false;
    }
    if (!checkemail(val)) {
        $('#refError').html('<p class="alert alert-danger">Please enter a valid email address!</p>');
        $('#referFriend').focus();
        return false;
    }

    $('.form-control-feedback').attr('disabled', 'disabled');
    $.ajax({
        type: 'POST',
        //url: "<?php echo BASE_URL;?>users/refer",
        url: baseUrl + "users/refer",
        data: {'referFriend': val},
        beforeSend: function (xhr) {
            $('#refSpin').show();
        },
        success: function (res) {
            $('#refSpin').hide();
            $('#refError').html(res);
            $('.form-control-feedback').removeAttr('disabled');
        }
    });
    //$('#ref-frnd-form').submit();            
}

function checkemail(email) {
    var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
    if (filter.test(email))
        return true;
    else {
        return false;
    }
}


//#### JS for Redeem tab #######
$(function () {
    $('.redeem-tab').click(function () {
        $('.redeem-tab').removeClass('border active-step');
        $(this).addClass('border active-step');
    });
});


/*
 * .... Auto Search js ........
 */
$(function () {
    $('.srch-form').keyup(function () {
        var baseUrl = $('#base-url').val();
        var text = $(this).val();
        if (text.length > 2) {
            console.log(text);
            $.ajax({
                type: 'POST',
                url: baseUrl + 'retailers/search',
                data: {'text': text},
                success: function (res) {
                    //if(res != 'not found'){
                    $('.serach-result-ul').html(res);
                    $('.serach-result-ul').show();
                    //}
                }
            });
        } else {
            $('.serach-result-ul').hide();
            $('.serach-result-ul').html('');
        }
    });

    $('.srch-form').keypress(function (e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            console.log($('ul.serach-result-ul').children('li').first().addClass('clickable'));
            var loc = $('.clickable a').attr('href');
            //console.log(loc);
            document.location.href = loc;
            return false;
        }
    });

//    $(window).click(function () {
//        $('.serach-result-ul').hide();
//        $('.serach-result-ul').html('');
//    });
});