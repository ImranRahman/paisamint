$(document).ready(function () {
    $('.searchBtn').click(function () {
        $('.error').remove();
        var val = $('.srch-form').val();
        if (val == "") {
            $('.main_search').after('<span class="error">Please enter keyword!</span>');
            $('.srch-form').focus();
            return false;
        }
        return true;
    });

    //.... Coupon Like Dislike JS ......
    $('.coup-likes').click(function (e) {
        var ID = $(this).attr('id');
        //console.log(ID);
        var baseUrl = $('#base-url').val();
        var user_sess = $('#user_session').val();

        if (user_sess) {            
            $.ajax({
                type: "POST",
                url: baseUrl + "categories/setCouponLikes",
                data: {'id': ID},
                success: function (res) { 
                    if(res !='not done'){
                        var data = $.parseJSON(res); 
                        if(data.status == 'like'){
                            $('#'+ID+' i').css('color','#0DE238');
                        }else{
                            $('#'+ID+' i').css('color','red');
                        }
                        $('#like_'+data.id).removeClass('coup-likes');
                        $('#dislike_'+data.id).removeClass('coup-likes');
                        $('#'+ID+' span').html(data.count);
                    }
                }
            });
        } else {
            $('#modal-launcher').click();
        }

        e.preventDefault();
    });

    // ######## Coupon Favorite JS ###########
    $('.coup-fav').click(function (e) {
        var ID = $(this).attr('id');
        console.log(ID);
        var baseUrl = $('#base-url').val();
        var user_sess = $('#user_session').val();
        
        if (user_sess) {
            $.ajax({
                type: "POST",
                url: baseUrl + "categories/setCouponFav",
                data: {'id': ID},
                success: function (res) {
                    if(res == 'Done'){
                        $('#'+ID+' i').css('color','#0DE238');
                    }else{
                       $('#'+ID+' i').css('color',''); 
                    }
                }
            });
        } else {
            $('#modal-launcher').click();
        }
        e.preventDefault();
    });

});



function couponPopup(id) {
    var baseUrl = $('#base-url').val();
    var user_sess = $('#user_session').val();
    if (user_sess) {
        $.ajax({
            type: "POST",
            url: baseUrl + "homes/getCouponAjax",
            data: {'id': id},
            success: function (res) {
                var data = $.parseJSON(res);
                console.log(data['Retailer']['name']);
                $('.coupon-code').html(data['Coupon']['code']);
                $('.external').html(data['Retailer']['name']);
                $('.external').attr('href', baseUrl + 'coupons/load/' + data['Coupon']['id']);
                $('.coupon-title > h4').html(data['Coupon']['title']);
                $('.coupon-description').html(data['Coupon']['description']);

                $('#couponModal').modal('show');
            }
        });
    } else {
        $('#modal-launcher').click();
    }

}
