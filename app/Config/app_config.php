<?php
define('BASE_URL','http://' . $_SERVER['HTTP_HOST'] . '/');
define('BASE_URL_ADMIN','http://' . $_SERVER['HTTP_HOST'] . '/admin/');

define('SITE_NAME', 'PaisaMint');
define('SITE_EMAIL', 'info@paisamint.com');


define('RETAILER_IMG', WWW_ROOT.'uploads'.DS.'retailers'.DS);
define('RETAILER_MID_THUMB', WWW_ROOT.'uploads'.DS.'retailers'.DS.'midThumb'.DS);
define('RETAILER_THUMB', WWW_ROOT.'uploads'.DS.'retailers'.DS.'thumb'.DS);

define('RETAILER_MID_THUMB_URL', BASE_URL.'uploads/retailers/midThumb/');
define('RETAILER_THUMB_URL', BASE_URL.'uploads/retailers/thumb/');

?>
