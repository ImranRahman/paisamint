<?php

class CategoriesController extends AppController {

    var $uses = array('Retailer', 'Category', 'Coupon', 'SubCategory', 'CouponLike', 'CouponFavorit', 'CouponComment');

    public function beforeFilter() {

        parent::beforeFilter();

        $this->Auth->allow('index', 'getCouponLikes', 'getCouponFav');

        $this->layout = 'custome';

        $model = 'Category';
        $this->Model = ClassRegistry::init($model);
        $this->set('model', $model);
    }

    public function index() {

        $joins = array(
            array(
                'table' => 'sub_categories',
                'alias' => 'SubCategory',
                'conditions' => array('Category.id = SubCategory.category_id', 'SubCategory.isDeleted' => 0),
                'type' => 'left',
                'order' => array('SubCategory.name' => 'asc')
            )
        );
        $categories = $this->Category->find('all', array(
            //'fields'=>array('Category.*','SubCategory.id,SubCategory.name'),
            'conditions' => array('Category.isDeleted' => 0),
            //'joins'=>$joins,
            'group' => 'Category.id',
            'order' => array('Category.name' => 'asc')
        ));
        //pr($categories);die;
        $this->set('categories', $categories);

        $cond = '';
        $params = $this->params['pass']; //die(pr($params));
        if (!empty($params)) {
            $cate = $this->SeoUrl->friendlyUrl($params[0]);
            $Cates = $this->Category->find('first', array(
                'conditions' => array('Category.name' => $cate)
            ));
            $cate_id = $Cates['Category']['id'];

            if (isset($params[1])) {
                $sub_cate = $this->SeoUrl->friendlyUrl($params[1]);
                $subCate = $this->SubCategory->find('first', array(
                    'conditions' => array('SubCategory.name' => $sub_cate)
                ));
                $sub_cate_id = $subCate['SubCategory']['id'];
                $cond = " Coupon.subCateId = '$sub_cate_id'";
            }
        }



        $now = date('Y-m-d');
        $joinCoupon = array(
            array(
                'table' => 'retailers',
                'alias' => 'Retailer',
                'conditions' => array('Retailer.id = Coupon.retailer_id'),
                'type' => 'LEFT'
            ),
        );
        $couponsQry = $this->paginate = array(
            'fields' => array('Retailer.logo, Retailer.name', 'Coupon.*'),
            'conditions' => array('Coupon.isDeleted' => 0, 'Coupon.status' => 1, "Coupon.expire >= " => $now, $cond),
            'limit' => '12',
            'order' => array('Coupon.id' => 'desc'),
            'joins' => $joinCoupon,
        );
        $coupons = $this->paginate('Coupon');
        $this->set('coupons', $coupons);
    }

    public function getCouponLikes($couponId = null) {
        $userId = $this->Session->read('user_data.id');
        $likes = $this->CouponLike->find('count', array(
            'conditions' => array('CouponLike.coupon_id' => $couponId, 'CouponLike.status' => 1)
        ));

        $disLikes = $this->CouponLike->find('count', array(
            'conditions' => array('CouponLike.coupon_id' => $couponId, 'CouponLike.status' => 2)
        ));

        $userLike = $this->CouponLike->find('first', array(
            'fields' => array('CouponLike.status'),
            'conditions' => array('CouponLike.coupon_id' => $couponId, 'CouponLike.user_id' => $userId)
        ));
        $userLike = (empty($userLike)) ? 0 : $userLike['CouponLike']['status'];
        $array = array(
            'likes' => $likes,
            'disLikes' => $disLikes,
            'userLike' => $userLike
        );
        return $array;
    }

    public function getCouponFav($couponId = null) {
        $userId = $this->Session->read('user_data.id');
        $fav = $this->CouponFavorit->find('first', array(
            'fields' => array('CouponFavorit.status'),
            'conditions' => array('CouponFavorit.coupon_id' => $couponId, 'CouponFavorit.status' => 1, 'CouponFavorit.user_id' => $userId)
        ));
        if (!empty($fav)) {
            return $fav['CouponFavorit']['status'];
        } else {
            return 0;
        }
    }

    public function setCouponLikes() {
        //pr($_POST);
        if (isset($_POST)) {
            $userId = $this->Session->read('user_data.id');
            $coupon = @explode('_', $_POST['id']);

            $data['CouponLike']['coupon_id'] = $coupon[1];
            $data['CouponLike']['user_id'] = $userId;
            $data['CouponLike']['status'] = ($coupon[0] == 'like') ? 1 : 2;

            $check = $this->CouponLike->find('first', array(
                'conditions' => array('CouponLike.coupon_id' => $coupon[1], 'CouponLike.status != ' => 0, 'CouponLike.user_id' => $userId)
            ));
            if (empty($check)) {
                $this->CouponLike->create();
                $this->CouponLike->save($data);

                $likes = $this->CouponLike->find('count', array(
                    'conditions' => array('CouponLike.coupon_id' => $coupon[1], 'CouponLike.status' => $data['CouponLike']['status'])
                ));

                $resp = array(
                    'status' => $coupon[0],
                    'id' => $coupon[1],
                    'count' => $likes
                );
                echo json_encode($resp);
            } else {
                echo 'not done';
            }
            die;
        }
        die;
    }

    public function setCouponFav() {
        //pr($_POST);
        if (isset($_POST)) {
            $userId = $this->Session->read('user_data.id');
            $favs = @explode('_', $_POST['id']);

            $fav = $this->CouponFavorit->find('first', array(                
                'conditions' => array('CouponFavorit.coupon_id' => $favs[1], 'CouponFavorit.user_id' => $userId)
            ));
            
            if(empty($fav)){
                $data['CouponFavorit']['coupon_id'] = $favs[1];
                $data['CouponFavorit']['user_id'] = $userId;
                $data['CouponFavorit']['status'] = 1;
                $this->CouponFavorit->create();
                $this->CouponFavorit->save($data);
                echo 'Done'; die;
            }else{
                $data['CouponFavorit']['status'] = ($fav['CouponFavorit']['status']==0)?1:0;
                $this->CouponFavorit->id = $fav['CouponFavorit']['id'];
                $this->CouponFavorit->save($data);
                if($data['CouponFavorit']['status'] == 1){
                    echo 'Done'; die;
                }
                else{
                    echo 'Not Done'; die;
                }
            }
            
        }

        die;
    }

}
