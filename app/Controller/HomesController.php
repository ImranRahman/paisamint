<?php

App::uses('CakeEmail', 'Network/Email');

class HomesController extends AppController {

    var $uses = array('User', 'Retailer', 'Category', 'Coupon', 'Subscribe');
    public $helpers = array('Time');

    public function beforeFilter() {

        parent::beforeFilter();

        $this->Auth->allow('index', 'moreCoupon', 'subscribe', 'Mailsubcsribe', 'getCouponAjax');
    }

    public function index() {

        $categories = $this->Category->find('all', array(
            'conditions' => array("Category.isDeleted='0'", "Category.status='1'"),
            'limit' => 4
        ));

        /*
         * .. find top retailers
         */
        $this->set('categories', $categories);
        $this->set('retailers', $this->Retailer->find('all', array(
                    'conditions' => array("Retailer.isDeleted = '0'", "Retailer.status = '1'"),
                    'order' => array('Retailer.id' => 'desc'),
                    'limit' => 12,
        )));


        /*
         * ... Find retailers by category
         */
        $retCate = array();
        foreach ($categories as $category) {
            $join = array(
                array(
                    'table' => 'retailer_categories',
                    'alias' => 'RetailerCategory',
                    'conditions' => array("RetailerCategory.retailer_id = Retailer.id", "RetailerCategory.category_id='" . $category['Category']['id'] . "'"),
                )
            );

            $retailer = $this->Retailer->find('all', array(
                'fields' => array('Retailer.logo,Retailer.name', 'RetailerCategory.category_id'),
                'conditions' => array("Retailer.isDeleted = '0'", "Retailer.status = '1'", ""),
                'order' => array('Retailer.id' => 'desc'),
                'limit' => 12,
                'joins' => $join,
            ));

            $retCate[$category['Category']['name']] = $retailer;
            unset($join);
        }

        $this->set('retsCate', $retCate);

        /*
         * ... Get Coupons here
         */
        $now = date('Y-m-d');
        $joinCoupon = array(
            array(
                'table' => 'retailers',
                'alias' => 'Retailer',
                'conditions' => array('Retailer.id = Coupon.retailer_id'),
                'type' => 'LEFT'
            )
        );
        $this->set('coupons', $this->Coupon->find('all', array(
                    'fields' => array('Retailer.logo, Retailer.name', 'Coupon.*'),
                    'conditions' => array('Coupon.isDeleted' => 0, 'Coupon.status' => 1, "Coupon.expire >= " => $now),
                    'limit' => '10',
                    'order' => array('Coupon.id' => 'desc'),
                    'joins' => $joinCoupon,
        )));
    }

    public function subscribe() {
        if ($this->request->is('post')) {
            if (empty($this->request->data['email'])) {
                echo 'Please enter your email address';
                die;
            } elseif (filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL) === false) {
                echo('"' . $this->request->data['email'] . '"' . " is not a valid email address");
                die;
            }

            $subscribe = $this->Subscribe->find('first', array(
                'conditions' => array(
                    'Subscribe.isDeleted' => 0,
                    'Subscribe.email' => $this->request->data['email']
                )
            ));
            if (empty($subscribe)) {
                // print_r($this->request->data);  exit;
                $data['Subscribe']['email'] = $this->request->data['email'];
                $this->Subscribe->save($data);
                $id = $this->Subscribe->getLastInsertId();

                $forgetCode = md5($id . time());
                $this->Subscribe->query("UPDATE subscribes set code = '$forgetCode' WHERE id = '" . $id . "'");
                $name = @explode('@', $this->request->data['email']);
                $vars = array(
                    'email' => $this->request->data['email'],
                    'id' => $id,
                    'baseUrl' => BASE_URL,
                    'code' => $forgetCode,
                    'name' => $name[0],
                );

                $Email = new CakeEmail('smtp');
                $Email->viewVars($vars);
                $Email->template('subscribe')
                        ->emailFormat('html')
                        ->subject('Mail Subscribe')
                        ->to($this->request->data['email'])
                        ->from(SITE_EMAIL)
                        ->send();

                echo 'Done';
                die;
            }
            echo 'You are already Subscribed';
        } die;
    }

    public function Mailsubcsribe($id = null, $code = null) {
        $this->Subscribe->updateAll(
                array('Subscribe.status' => 1, 'Subscribe.code' => "''"), array('Subscribe.id' => $id)
        );
        $array['heading'] = 'Congratulations';
        $array['message'] = '<div class="alert alert-success">Your mail is Subscribe Successfully.</div>';
        $this->set('data', $array);
    }

    public function moreCoupon() {
        $this->autoRender = false;
        //pr($this->request->data);
        $now = date('Y-m-d');
        if ($this->request->is('post')) {
            $joinCoupon = array(
                array(
                    'table' => 'retailers',
                    'alias' => 'Retailer',
                    'conditions' => array('Retailer.id = Coupon.retailer_id'),
                    'type' => 'LEFT'
                )
            );
            $coupons = $this->Coupon->find('all', array(
                'fields' => array('Retailer.logo, Retailer.name', 'Coupon.*'),
                'conditions' => array('Coupon.isDeleted' => 0, 'Coupon.status' => 1, "Coupon.expire >= " => $now),
                'limit' => '20',
                'offset' => $this->request->data['limit'],
                'order' => array('Coupon.id' => 'desc'),
                'joins' => $joinCoupon,
            ));

            if (empty($coupons)) {
                echo 'No Found';
            } else {
                $data = '';

                foreach ($coupons as $coupon) {
                    $data .= '<div class="col-md-2 col-xs-3  offer_box"> 
                        <div class="icon-info">
                            <i class="fa fa-info-circle new"></i>
                        </div>';

                    if ($coupon['Retailer']['logo'] != '' && file_exists(RETAILER_THUMB . $coupon['Retailer']['logo'])) {
                        $data .= '<a href="' . BASE_URL . 'retailers/' . $coupon['Retailer']['name'] . '"><img alt="Logo Here" src="' . RETAILER_THUMB_URL . $coupon['Retailer']['logo'] . '"></a>';
                    } else {
                        $data .= '<a href="' . BASE_URL . 'retailers/' . $coupon['Retailer']['name'] . '"><img alt="Logo Here" src="' . BASE_URL . 'img/noimage.jpg"></a>';
                    }

                    $data .= '<p style="line-height:1.35;">' . $coupon['Coupon']['title'] . '</p>';

                    $now = time(); // or your date as well
                    $your_date = strtotime($coupon['Coupon']['expire']);
                    $datediff = $your_date - $now;
                    $expiry = ceil($datediff / (60 * 60 * 24));
                    $day = ($expiry > 1) ? ' days' : ' day';

                    $data .= '<span><i class="fa fa-clock-o"></i>Expires in ' . $expiry . $day . '</span>
                        <h5 style="line-height:1.25; height: 70px;">+ Cashback upto <br><span style="font-size:24px; color: #08c; font-weight: bold; font-family: arial;">' . $coupon['Coupon']['cashback'] . '%</span></h5>
                        <button type="button" onclick="couponPopup(' . $coupon['Coupon']['id'] . ');" class="get">Get this Deal</button>
                    </div>';
                }

                echo $data;
            }
        }
        die;
    }

    public function getCouponAjax() {
        //pr($this->request->data);die;
        $cid = $this->request->data['id'];
        $joinCoupon = array(
            array(
                'table' => 'retailers',
                'alias' => 'Retailer',
                'conditions' => array('Retailer.id = Coupon.retailer_id'),
                'type' => 'LEFT'
            )
        );
        $coupon = $this->Coupon->find('first', array(
            'fields' => array('Retailer.*', 'Coupon.*'),
            'conditions' => array('Coupon.id' => $cid),
            'joins' => $joinCoupon,
        ));
        
        $coupon['Coupon']['verified'] = CakeTime::timeAgoInWords($coupon['Coupon']['created'], array(
                    'accuracy' => array('month' => 'month'),
                    'end' => '1 year'
        ));


        $now = time(); // or your date as well
        $your_date = strtotime($coupon['Coupon']['expire']);
        $datediff = $your_date - $now;
        $expiry = ceil($datediff / (60 * 60 * 24));
        $day = ($expiry > 1) ? ' days' : ' day';
        if ($expiry == 0) {
            $expiry = 'today';
            $day = '';
        }
        
        $coupon['Coupon']['expires'] = $expiry . $day;
        
        echo json_encode($coupon);
        die;
    }

}
