<?php

App::uses('AppController', 'Controller');

class RetailersController extends AppController {

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array('Retailer', 'Coupon', 'RetailerCategory');

    public function beforeFilter() {

        parent::beforeFilter();

        $model = 'Retailer';
        $this->Model = ClassRegistry::init($model);
        $this->set('model', $model);

        $this->Auth->allow('index', 'search');
    }

    public function index() {
        $this->layout = 'custome';
        if (isset($this->params['pass'][0])) {
            $params = $this->params['pass'][0]; //pr($params);

            $name = $this->SeoUrl->friendlyUrl($params);
            $this->set('paramName', ucwords($name));

            $retailer = $this->Retailer->find('first', array(
                'conditions' => array('Retailer.name' => $name)
            ));
            $this->set('retailer', $retailer);

            $now = date('Y-m-d');
            $retailerid = $retailer['Retailer']['id'];
            $coupons = $this->Coupon->find('all', array(
                'conditions' => array('Coupon.retailer_id' => $retailerid, 'Coupon.isDeleted' => 0, "Coupon.expire >= " => $now)
            ));
            $this->set('coupons', $coupons);

            /* Find retailer category */
            $retailerCate = $this->RetailerCategory->find('list', array(
                'fields' => array('RetailerCategory.category_id'),
                'conditions' => array('RetailerCategory.retailer_id = ' . $retailerid)
            ));
            if (empty($retailerCate)) {
                $retailerCate = 0;
            } else {
                $retailerCate = implode(',', $retailerCate);
            }
            //pr($retailerCate);die;
            /* -- Find related Retailers */
            $join = array(
                array(
                    'table' => 'retailer_categories',
                    'alias' => 'RetailerCategory',
                    'conditions' => array('RetailerCategory.retailer_id = Retailer.id'),
                    'type' => 'left'
                )
            );
            $relatedRetailers = $this->Retailer->find('all', array(
                'fields' => array('Retailer.name', 'RetailerCategory.retailer_id,RetailerCategory.category_id'),
                'conditions' => array('RetailerCategory.category_id IN ( ' . $retailerCate . ')', 'Retailer.id != ' => $retailerid, 'Retailer.isDeleted' => 0),
                'joins' => $join,
                'group' => 'Retailer.id',
                'order' => array('Retailer.name' => 'asc')
            ));
            //pr($relatedRetailers);die;
            $this->set('relatedRetailers', $relatedRetailers);
        } else {
            /*
             * .....Find all Retailers
             */
            $resJoin = array(
                array(
                    'table' => 'coupons',
                    'alias' => 'Coupon',
                    'type' => 'LEFT',
                    'conditions' => array('Coupon.retailer_id = Retailer.id'),
                )
            );
            $all_retailers = $this->Retailer->find('all', array(
                'conditions' => array('Retailer.isDeleted' => 0),
                'order' => array('Retailer.id' => 'desc'),
            ));
            
            $now = date('Y-m-d');
            foreach ($all_retailers as $k => $all) {
                
                $retailerid = $all['Retailer']['id'];
                $coupons = $this->Coupon->find('count', array(
                    'conditions' => array('Coupon.retailer_id' => $retailerid, 'Coupon.isDeleted' => 0, "Coupon.expire >= " => $now)
                ));
                //pr($coupons); 
                $all_retailers[$k]['Coupon'] = $coupons;
            }

            $this->set('retailers', $all_retailers);
            $this->render('stores');
        }
    }

    public function search() {
        //pr($_POST);
        $search = $_POST['text'];
        $now = date('Y-m-d');

        $retailers = $this->Retailer->find('all', array(
            'fields' => array('Retailer.id,name,logo,description'),
            'conditions' => array('Retailer.name LIKE ' => "%$search%", 'Retailer.isDeleted' => 0),
            'order' => array('Retailer.name' => 'asc'),
                )
        );
        $data = '';
        if (empty($retailers)) {
            echo '<li class="alert-danger">No Result Found</li>';
        } else {
            foreach ($retailers as $retailer) {
                $name = $this->SeoUrl->seoUrl($retailer['Retailer']['name']);
                $data .= '<li><a href="' . BASE_URL . 'retailers/' . $name . '">';
                $data .= '<img src="' . RETAILER_THUMB_URL . $retailer['Retailer']['logo'] . '">';
                //$data .=  '<span>'.ucwords($retailer['Retailer']['name']).'</span>';
                $data .= '<span>' . $this->SeoUrl->wordWrap($retailer['Retailer']['description'], 50) . '</span>';
                $data .= '</a></li>';
            }
            echo $data;
        }

        die;
    }

}
