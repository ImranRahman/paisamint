<?php

//Ram@123456%# payoom pass
@session_start();

App::uses('CakeEmail', 'Network/Email');

class UsersController extends AppController {

    public $helpers = array('time');
    public $uses = array('User', 'SocialProfile', 'Transaction');
    public $components = array('Hybridauth');

    public function beforeFilter() {

        parent::beforeFilter();

        $this->Auth->allow('register', 'ajaxLogin', 'ajaxRegister', 'forgetPassword', 'activate', 'index', 'forget', 'updateProfile', 'social_login', 'social_endpoint', 'test');
    }

    public function index() {

//        if ($this->Session->check('user_data')) {
//            $this->redirect(array('controller' => 'users', 'action' => 'account'));
//        } else {
//            $this->redirect(array('controller' => 'homes', 'action' => 'index'));
//        }
        $this->redirect(array('controller' => 'homes', 'action' => 'index'));
    }

    public function login() {
        $this->autoRender = false;
        //pr($this->Session->read('user_data'));die;
        if ($this->Session->check('user_data')) {
            $this->redirect(array('action' => 'index'));
        }

        if ($this->request->is('post')) {
            //pr($this->request->data); die;

            if ($this->Auth->login()) {

                $this->Session->write("user_data", $this->Auth->User());
                return $this->redirect($this->Auth->redirect(array('action' => 'index')));
            }
            $this->Session->setFlash(__('Opps! Change somethings are wrong and try again'));
        } else {
            $this->redirect(array('controller' => 'homes', 'action' => 'index'));
        }
    }

    public function register() {
        $this->autoRender = false;

        if ($this->request->is('post')) {

            if ($this->User->save($this->request->data)) {

                $this->Session->setFlash('Register successfully.', 'default', array('class' => 'success'));
                $this->redirect(array('controller' => 'homes', 'action' => 'index'));
            }
            $this->Session->setFlash('Unable to register! Try again.', 'default', array('class' => 'cake-error'));
        }
    }

    public function logout() {
        $this->Session->delete('user_data');
        $this->Hybridauth->logout();
        return $this->redirect($this->Auth->logout());
    }

    public function ajaxRegister() {
        $this->autoRender = false;
        $errors = array();
        if ($this->request->is('post')) {
            //pr($this->request->data['User']['referral']); die;
            if ($this->request->data['User']['referral'] != '') {
                // Decrypt referral user id
                $enckey = 'ramjanam@resolweb';
                $secret64Dec = base64_decode($this->request->data['User']['referral']);
                $ref_id = Security::cipher($secret64Dec, $enckey);
                //echo $ref_id; die;
                $this->request->data['User']['ref_id'] = $ref_id;
            }
            unset($this->request->data['User']['referral']);

            $this->User->create();
            if ($this->User->save($this->request->data)) {

                $userId = $this->User->getLastInsertId();
                $this->updateUserData($userId);

                if (isset($ref_id)) {
                    $this->updateReferral($userId, $ref_id);
                }
                echo 'Done';
                die;
            }
            $errors = $this->User->validationErrors;
            //pr($errors); die;
            echo json_encode($errors);
            die;
        }
    }

    public function updateUserData($userId) {

        $this->User->id = $userId;
        $this->request->data['User']['activateCode'] = md5($userId);
        $this->User->save($this->request->data);

        $user = $this->User->findById($userId);

        $vars = array(
            'email' => $user['User']['email'],
            'code' => $user['User']['activateCode'],
            'id' => $user['User']['id'],
            'baseUrl' => BASE_URL,
        );

        $Email = new CakeEmail('smtp');
        $Email->viewVars($vars);

        $Email->template('register')
                ->emailFormat('html')
                ->subject('Registration Verify')
                ->to($user['User']['email'])
                ->from('info@paisamint.com')
                ->send();
    }

    public function updateReferral($userId, $ref_id) {

        $this->User->id = $userId;
        $this->request->data['User']['ref_id'] = $ref_id;
        $this->User->save($this->request->data);

        $user = $this->User->findById($userId);
        $refUser = $this->User->findById($ref_id);

        $vars = array(
            'email' => $user['User']['email'],
            'name' => $user['User']['name'],
            'baseUrl' => BASE_URL,
            'refEmail' => $refUser['User']['email'],
            'refName' => $refUser['User']['name'],
        );

        $Email = new CakeEmail('smtp');
        $Email->viewVars($vars);

        $Email->template('ref_registered')
                ->emailFormat('html')
                ->subject('Referral User')
                ->to($refUser['User']['email'])
                ->from(SITE_EMAIL)
                ->send();
    }

    public function ajaxLogin() {

        if ($this->request->is('post')) {
            //pr($this->request->data); die;

            if ($this->Auth->login()) {

                if ($this->Auth->User('status') == 1 && $this->Auth->User('isDeleted') == 0) {

                    $this->Session->write("user_data", $this->Auth->User());
                    //return $this->redirect($this->Auth->redirect(array('action' => 'index')));
                    echo 'Done';
                    die;
                } else {
                    $this->Auth->logout();
                    echo __('Opps! The user is blocked or does not exists.');
                    die;
                }
            }
            echo __('Opps! the email or password is invalid.');
            die;
        }
    }

    public function forgetPassword() {
        if ($this->request->is('post')) {
            if (empty($this->request->data['email'])) {
                echo 'Please enter your email address.';
                die;
            } elseif (filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL) === false) {
                echo($this->request->data['email'] . " is not a valid email address");
                die;
            }

            $user = $this->User->findByEmail($this->request->data['email']);
            if (empty($user)) {
                echo("Opps! <b>" . $this->request->data['email'] . "</b> is not exists.");
                die;
            }

            $forgetCode = md5($user['User']['id'] . time());
            $this->User->query("UPDATE users set forgetCode = '$forgetCode' WHERE id = '" . $user['User']['id'] . "'");

            $name = @explode('@', $user['User']['email']);

            $vars = array(
                'email' => $user['User']['email'],
                'id' => $user['User']['id'],
                'baseUrl' => BASE_URL,
                'code' => $forgetCode,
                'name' => $name[0],
            );

            $Email = new CakeEmail('smtp');
            $Email->viewVars($vars);

            $Email->template('forget_password')
                    ->emailFormat('html')
                    ->subject('Forget Password')
                    ->to($user['User']['email'])
                    ->from('info@paisamint.com')
                    ->send();

            echo 'Done';
            die;
        }
        die;
    }

    public function activate($userId = null, $code = null) {

        $user = $this->User->find('first', array(
            'conditions' => array('id' => $userId, 'activateCode' => $code)
                )
        );
        if (empty($user)) {

            $array = array(
                'heading' => 'Error occured!',
                'message' => 'Error in activation account! your account may be activated or somethins mishmatched <br>Please try after some time.'
            );

            $this->set('data', $array);
        } else {

            //$this->User->id = $userId;
            //$this->request->data['User']['activateCode'] = '';
            //$this->request->data['User']['status'] = '1';
            //$this->User->save($this->request->data);
            $this->User->query("UPDATE users set activateCode = '', status = '1' WHERE id = '$userId'");

            $vars = array(
                'email' => $user['User']['email'],
                'id' => $user['User']['id'],
                'baseUrl' => BASE_URL,
            );

            $Email = new CakeEmail('smtp');
            $Email->viewVars($vars);

            $Email->template('activated')
                    ->emailFormat('html')
                    ->subject('Account Verified')
                    ->to($user['User']['email'])
                    ->from('info@paisamint.com')
                    ->send();

            $array = array(
                'heading' => 'Congratulations!',
                'message' => 'Your account is activated successfully! Now you can access your acount.'
            );

            $this->set('data', $array);
        }
    }

    public function forget($id = null, $code = null) {
        $user = $this->User->find('first', array(
            'conditions' => array('id' => $id, 'forgetCode' => $code)
                )
        );
        if (empty($user)) {
            $array = array(
                'heading' => 'Error occured!',
                'message' => 'Sorry! you can\'t reset your password any more right now.<br>'
                . 'If you want to reset your passworsd then click on sign in and forget password link.',
                'id' => $id,
                'code' => $code
            );
        } else {
            $array = array(
                'heading' => 'Reset your password',
                'message' => '',
                'id' => $id,
                'code' => $code
            );

            if ($this->request->is(array('post', 'put'))) { //pr($this->request->data);die;
                if (empty($this->request->data['User']['password'])) {
                    $array['message'] = '<div class="error">Please enter your Password!</div>';
                } elseif ($this->request->data['User']['password'] != $this->request->data['User']['confpassword']) {
                    $array['message'] = '<div class="error">Password entered did not matched</div>';
                } else {

                    unset($this->request->data['User']['confpassword']);

                    $hashPass = AuthComponent::password($this->request->data['User']['password']);

                    $this->User->updateAll(
                            array('User.password' => "'$hashPass'", 'User.forgetCode' => "''"), array('User.id' => $user['User']['id'])
                    );

                    $array['heading'] = 'Congratulations';
                    $array['message'] = '<div class="success">Password reset successfully.</div>';
                }
            }
        }
        $this->set('data', $array);
    }

    public function account() {
        $this->layout = 'custome';

        //pr($this->Session->read('user_data.id'));die;
        $userId = $this->Session->read('user_data.id');
        try {
            $this->set("user", $this->User->find('first', array(
                        'conditions' => array('User.id' => $userId, 'User.status' => '1')
            )));

            $this->set('transRecent', $this->Transaction->find('first', array(
                        'conditions' => array('Transaction.user_id' => $userId),
                        'order' => array('id' => 'desc'),
                        'limit' => 1
            )));
            $all_trans = $this->paginate = array(
                'conditions' => array('Transaction.user_id' => $userId),
                'order' => array('id' => 'desc'),
                'limit' => 20
            );
            $transAll = $this->paginate('Transaction');
            $this->set('transAll', $transAll);

            $enckey = 'ramjanam@resolweb';
            $secret = Security::cipher($userId, $enckey);
            $secret64Enc = base64_encode($secret);
            $this->set('shareId', $secret64Enc);
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
    }

    public function updateProfile() {
        $this->layout = 'custome';

        $user = $this->User->findById($this->Session->read('user_data.id'));

        if ($this->request->is(array('post', 'put'))) {
            //pr($this->request->data);die;
            $this->User->id = $this->Session->read('user_data.id');
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash("Your Profile updated successfully.", "default", array('class' => 'alert alert-success'));
                $this->redirect(array('controller' => 'users', 'action' => 'account'));
            }
            $this->Session->setFlash(__("<p>An error occured Please see below errors:</p>"), 'default', array('class' => 'alert alert-danger'));
        }

        $this->set('user', $user);
        $this->render('account');
    }

    public function updatePassword() {
        $this->autoRender = false;

        $user = $this->User->findById($this->Session->read('user_data.id'));
        $errors = array();

        if ($this->request->is(array('post', 'put'))) { 

            if (empty($this->request->data['User']['currpassword'])) {
                $errors[] = 'Please enter your Current Password!';
            }
            else {
                $hashPass = AuthComponent::password($this->request->data['User']['currpassword']);
                if ($hashPass != $user['User']['password']) {
                    $errors[] = "Current password is incorrect";
                    echo json_encode($errors);
                    return 1;
                }                
            }


            if (empty($this->request->data['User']['password'])) {
                $errors[] = 'Please enter New Password!';
            } 

            if (!empty($this->request->data['User']['password'])) {
                if ($this->request->data['User']['password'] != $this->request->data['User']['confpassword']) {
                    $errors[] = "Password didn't match";
                }
            }

            if (!count($errors)) {
                $hashPass = AuthComponent::password($this->request->data['User']['currpassword']);
                if ($hashPass == $user['User']['password']) {
                    $this->User->id = $this->Session->read('user_data.id');
                    if ($this->User->save($this->request->data)) {
                        echo "done";
                        return 1;
                    }
                }
            }
        }

        //$this->set('user', $user);        
        echo json_encode($errors);
        exit;
        
    }

    /*
     * .... OTP Function
     */

    public function sendOTP() {
        //pr($_POST);die;
        $user = $this->Auth->User(); //pr($user);
        $otp = rand(111111, 999999);
        
        $this->User->id = $user['id'];
        $array = array();
        $array['User']['otp'] = $otp;
        $array['User']['otp_time'] = time();
        $this->User->save($array);

        $mobile = $_POST['mobile'];
        $msg = "Your one time password is: $otp. valid for 5 min. only.";
        
        $smsapi = Configure::read('smsapi'); //pr($smsapi); 
        
        $ch = curl_init();
        $timeout = 100; // set to zero for no timeout
        $myurl = 'http://bhashsms.com/api/sendmsg.php?user='.$smsapi['user'].'&pass='.$smsapi['pass'].'&sender='.$smsapi['sender'].'&phone='.$mobile.'&text='.$msg.'&priority='.$smsapi['Priority'].'&stype='.$smsapi['smstype'].'';
        //echo $myurl;die;         

        curl_setopt($ch, CURLOPT_URL, $myurl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $file_contents = curl_exec($ch);
        $curl_error = curl_errno($ch);
        curl_close($ch);

        $maindata = explode(",", $file_contents);
        //pr($maindata);die;
        
        echo 'success';

        die;
    }

    public function recharge() {
        $this->autoRender = false;
        if (isset($_POST)) {
            //pr($_POST); die;
            
            $userData = $this->Auth->User();
            $checkUser = $this->User->findById($userData['id']);
            //pr($checkUser);die;
            $currTime = strtotime('-5 minutes'.date('Y-m-d h:i:s'));
            $otpTime = $checkUser['User']['otp_time'];
//            echo date('Y-m-d h:i:s',$currTime);
//            echo date('Y-m-d h:i:s',$otpTime); 
//            echo '<br>'.$currTime;
//            echo '<br>'.$otpTime;
//            die;
            
            if($currTime > $otpTime ){
                echo 'time_exeeded'; die;
                
            } elseif($checkUser['User']['otp'] != $_POST['otp']){
                echo 'otp_error'; die;
            }else {
            
                $flag = 0;
                $operator = $_POST['operator'];
                $servicenumber = $_POST['mobile'];
                $amount = $_POST['amount'];

                if ($operator == '') {
                    $flag = 1;
                    $this->set('operator_error', 'Please select your operator');
                }
                if ($servicenumber == '') {
                    $flag = 1;
                    $this->set('mobno_error', 'Please enter your number');
                }
                if ($servicenumber != '' && !is_numeric($servicenumber)) {
                    $flag = 1;
                    $this->set('mobno_error', 'Please enter number only');
                }
                if ($amount == '') {
                    $flag = 1;
                    $this->set('amt_error', 'Please enter amount');
                }
                if ($amount != '' && !is_numeric($amount)) {
                    $flag = 1;
                    $this->set('amt_error', 'Please enter decimal only');
                }

                if ($flag == 0) {

                    $user = $this->User->findById($userData['id']); //pr($_POST);die;
                    //echo $user['User']['balance']; echo $amount;die;
                    if ($user['User']['balance'] >= $amount) {
                        $uniqueorderid = uniqid();
                        $jolo = Configure::read('jolo');
                        $joloUser = $jolo['user'];
                        $myjoloappkey = $jolo['apikey']; //your jolo appkey
                        $mode = $jolo['mode']; //set 1 for live recharge, set 0 for demo recharge
                        //doing recharge now by hitting jolo api
                        $ch = curl_init();
                        $timeout = 100; // set to zero for no timeout
                        $myurl = "http://joloapi.com/api/recharge.php?mode=$mode&userid=$joloUser&key=$myjoloappkey&operator=$operator&service=$servicenumber&amount=$amount&orderid=$uniqueorderid";

                        curl_setopt($ch, CURLOPT_URL, $myurl);
                        curl_setopt($ch, CURLOPT_HEADER, 0);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                        $file_contents = curl_exec($ch);
                        $curl_error = curl_errno($ch);
                        curl_close($ch);

                        $maindata = explode(",", $file_contents);
                        //pr($maindata);die;
                        $transactionid = $maindata[0];
                        $status = $maindata[1];
                        if ($status == 'SUCCESS') {
                            $operator = $maindata[2];
                            $service = $maindata[3];
                            $amount = $maindata[4];
                        } else {
                            $status = 'FAILED';
                        }

                        /* ... Manage Transaction History */
                        $data['Transaction']['user_id'] = $user['User']['id'];
                        $data['Transaction']['order_id'] = $uniqueorderid;
                        $data['Transaction']['type'] = 'recharge';
                        $data['Transaction']['amount'] = $amount;
                        $data['Transaction']['status'] = $status;
                        $data['Transaction']['response'] = $file_contents;
                        $this->Transaction->create();
                        $this->Transaction->save($data);


                        if ($status == 'SUCCESS') {
                            /* ... Update User Balance */
                            $this->User->id = $user['User']['id'];
                            $remBalance = $user['User']['balance'] - $amount;
                            $userData['User']['balance'] = $remBalance;
                            $this->User->save($userData);

                            $successMsg = "Recharge successful!<br> &nbsp;&nbsp;Your transaction id is : $maindata[0]";
                            //--$this->Session->setFlash($successMsg, 'default', array('class' => 'alert alert-success'));
                            echo $successMsg; die;
                        } else {
                            //--$this->Session->setFlash("Your transaction has been failed!", 'default', array('class' => 'alert alert-danger'));
                            echo 'Your transaction has been failed!'; die;
                        }
                    } else {
                        //--$this->Session->setFlash("You don't have sufficiant balance!", 'default', array('class' => 'alert alert-danger'));
                        echo "You don't have sufficiant balance!"; die;
                    }
                }
                //echo 'success'; die;
            }
        }
        die;
        //$this->redirect(array('controller' => 'users', 'action' => 'account'));
    }

    /* ... Refer a Friend */

    public function refer() {
        $userData = $this->Auth->User(); //pr($userData['email']);die;
        if (isset($_POST)) {
            if (filter_var($_POST['referFriend'], FILTER_VALIDATE_EMAIL) === false) {
                echo '<p class="error">Please enter a valid email address!</p>';
                die;
            }

            //pr($_POST);die;
            /*
             * Encrypt and base64 encode 
             */
            $enckey = 'ramjanam@resolweb';
            $secret = Security::cipher($userData['id'], $enckey);
            $secret64Enc = base64_encode($secret);
            //echo $secret64Enc;            
            $toName = strstr($_POST['referFriend'], '@', true);
            $toName = preg_replace('/[\.\-]+/', ' ', $toName);
            //echo ucwords($toName); die;


            $vars = array(
                'toemail' => $_POST['referFriend'],
                'code' => $secret64Enc,
                'baseUrl' => BASE_URL,
                'fromName' => ucwords($userData['name']),
                'toName' => $toName,
            );

            $Email = new CakeEmail('smtp');
            $Email->viewVars($vars);

            $Email->template('referral')
                    ->emailFormat('html')
                    ->subject('Refrral Invitation')
                    ->to($_POST['referFriend'])
                    ->from($userData['email'])
                    ->send();

            echo '<p class="alert alert-success">Your invitation email has been sent.</p>';
            die;
            //$this->redirect(array('controller'=>'users','action'=>'account'));
            // Decrypt your text
            //        $secret64Dec = base64_decode($secret64Enc);
            //        $nosecret = Security::cipher($secret64Dec, $enckey);
            //        echo $nosecret;
        }
        die;
    }

    /*
     * Social Login Section Start Here
     */

    public function social_login($provider) {
        if ($this->Hybridauth->connect($provider)) {
            $this->_successfulHybridauth($provider, $this->Hybridauth->user_profile);
        } else {
            // error
            $this->Session->setFlash($this->Hybridauth->error);
            //$this->redirect($this->Auth->loginAction);
            $this->redirect(array('controller' => 'users', 'action' => 'index'));
        }
    }

    private function _successfulHybridauth($provider, $incomingProfile) {

        // #1 - check if user already authenticated using this provider before
        $this->SocialProfile->recursive = -1;
        $existingProfile = $this->SocialProfile->find('first', array(
            'conditions' => array('social_network_id' => $incomingProfile['SocialProfile']['social_network_id'], 'social_network_name' => $provider)
        ));

        if ($existingProfile) {
            // #2 - if an existing profile is available, then we set the user as connected and log them in
            $user = $this->User->find('first', array(
                'conditions' => array('id' => $existingProfile['SocialProfile']['user_id'])
            ));

            $this->_doSocialLogin($user, true);
        } else {

            // New profile.
            if ($this->Auth->loggedIn()) {
                // user is already logged-in , attach profile to logged in user.
                // create social profile linked to current user
                $incomingProfile['SocialProfile']['user_id'] = $this->Auth->user('id');
                $this->SocialProfile->save($incomingProfile);

                $this->Session->write("user_data", $this->Auth->User());
                $this->Session->setFlash('Your ' . $incomingProfile['SocialProfile']['social_network_name'] . ' account is now linked to your account.', 'default', array('class' => 'alert alert-success'));
                //$this->redirect($this->Auth->redirectUrl());
                $this->redirect(array('controller' => 'users', 'action' => 'index'));
            } else {
                // no-one logged and no profile, must be a registration.
                $user = $this->User->createFromSocialProfile($incomingProfile);

                $incomingProfile['SocialProfile']['user_id'] = $user['User']['id'];
                $this->SocialProfile->save($incomingProfile);

                // log in with the newly created user
                $this->_doSocialLogin($user);
            }
        }
    }

    private function _doSocialLogin($user, $returning = false) {

        if ($this->Auth->login($user['User'])) {
            $this->Session->write("user_data", $this->Auth->User());
            if ($returning) {
                $this->Session->setFlash(__('Welcome back, ' . $this->Auth->user('username')), 'default', array('class' => 'alert alert-success'));
            } else {
                $this->Session->setFlash(__('Welcome to our community, ' . $this->Auth->user('username')), 'default', array('class' => 'alert alert-success'));
            }
            //$this->redirect($this->Auth->loginRedirect);
            $this->redirect(array('controller' => 'users', 'action' => 'index'));
        } else {
            $this->Session->setFlash(__('Unknown Error could not verify the user: ' . $this->Auth->user('username')), 'default', array('class' => 'alert alert-danger'));
        }
    }

    public function social_endpoint($provider) {
        $this->Hybridauth->processEndpoint();
    }

}
