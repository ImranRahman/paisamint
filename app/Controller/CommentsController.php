<?php
class CommentsController extends AppController
{
    var $uses = array('User', 'Retailer','Category','Coupon','SubCategory','CouponComment','RetailerCategory');
    
    public function beforeFilter() {

        parent::beforeFilter();

        $this->Auth->allow('index','getCouponComment','coupon');
        
        $this->layout = 'custome';
        
        $model = 'Category';
        $this->Model = ClassRegistry::init($model);
        $this->set('model', $model);
    }
    
    public function index(){
        
    }
    
    public function coupon($id = null){
        $this->layout = 'custome';
        
        if($this->request->is('post')){
            //pr($this->request->data);die;
            
            $this->request->data['CouponComment']['status'] = 0;
            $this->request->data['CouponComment']['coupon_id'] = $id;
            $this->CouponComment->create();
            if($this->CouponComment->save($this->request->data)){
                $this->Session->setFlash('Your comment posted successfully and is under review!', 'default', array('class'=>'alert alert-success'));
                $this->redirect(array('controller'=>'comments','action'=>'coupon',$id));
            }
        }
        
        
        $now = date('Y-m-d');                
        $coupons = $this->Coupon->find('first', array(
            'conditions' => array('Coupon.id' => $id,'Coupon.isDeleted'=>0,"Coupon.expire >= "=>$now)
        ));        
        $this->set('coupons', $coupons);
         //die(pr($coupons));
        $retailer = $this->Retailer->find('first', array(
            'conditions' => array('Retailer.id' => $coupons['Coupon']['retailer_id'])
        ));
        $this->set('retailer', $retailer);
        //die(pr($retailer));
        $name = $retailer['Retailer']['name']; 
        $this->set('paramName', ucwords($name));
        
        
        $join = array(
            array(
                'table'=>'users',
                'alias'=>'User',
                'conditions'=>array('CouponComment.user_id = User.id'),
                
            )
        );
        $comment = $this->paginate = array(
            'fields'=>array('User.name', 'CouponComment.*'),
            'conditions'=>array('CouponComment.coupon_id'=>$id, 'CouponComment.status'=>1),
            'joins'=>$join,
            'limit'=>10
        );
        $comments = $this->paginate('CouponComment'); //pr($comments);die;
        $this->set('comments', $comments);
    }
    
    
    
    
    
    public function getCouponComment($couponId = null){
        $userId = $this->Session->read('user_data.id');
        
        $comments = $this->CouponComment->find('first', array(
            'fields' => array('CouponComment.status'),
            'conditions' => array('CouponComment.coupon_id'=>$couponId, 'CouponComment.user_id'=>$userId)
        ));
        
        if(!empty($comments)){
            return $comments['CouponComment']['status'];
        }else{
            return 0;
        }
    }
    
}