<?php
class CatalogsController extends AppController
{
    var $uses = array('Retailer', 'Category', 'Coupon');
    
    public function beforeFilter() {

        parent::beforeFilter();

        $this->Auth->allow('index');
    }

    public function index() {
        
        $catalog = $this->params->query;
        //pr($catalog['search']);
        $search = isset($catalog['search'])?$catalog['search']:'';
        $this->set('search', $search);
        
        $now = date('Y-m-d');
        $joinCoupon = array(
            array(
                'table' => 'retailers',
                'alias' => 'Retailer',
                'conditions' => array('Retailer.id = Coupon.retailer_id'),
                'type' => 'LEFT'
            ),            
            array(
                'table' => 'retailer_categories',
                'alias' => 'RetailerCategory',
                'conditions' => array('Retailer.id = RetailerCategory.retailer_id'),
                'type' => 'LEFT'
            ),
            array(
                'table' => 'categories',
                'alias' => 'Category',
                'conditions' => array('Category.id = RetailerCategory.category_id'),
                'type' => 'LEFT'
            ),
        );
        $this->set('coupons', $this->Coupon->find('all', array(
            'fields' => array('Retailer.logo, Retailer.name', 'Coupon.*', 'Category.name'),
            'conditions' => array('Coupon.isDeleted' => 0, 'Coupon.status' => 1,"Coupon.expire >= "=>$now, "OR"=>array(
                "Coupon.title LIKE '%$search%'", "Retailer.name LIKE '%$search%'", "Category.name LIKE '%$search%'"
            )),
            'joins' => $joinCoupon,
            'limit' => '20',
            'group' => 'Coupon.id',
            'order' => array('Coupon.id' => 'desc'),
                    
        )));
    }
    
    
    public function getMore(){
        $now = date('Y-m-d');
        $joinCoupon = array(
            array(
                'table' => 'retailers',
                'alias' => 'Retailer',
                'conditions' => array('Retailer.id = Coupon.retailer_id'),
                'type' => 'LEFT'
            ),            
            array(
                'table' => 'retailer_categories',
                'alias' => 'RetailerCategory',
                'conditions' => array('Retailer.id = RetailerCategory.retailer_id'),
                'type' => 'LEFT'
            ),
            array(
                'table' => 'categories',
                'alias' => 'Category',
                'conditions' => array('Category.id = RetailerCategory.category_id'),
                'type' => 'LEFT'
            ),
        );
        $coupons = $this->Coupon->find('all', array(
            'fields' => array('Retailer.logo, Retailer.name', 'Coupon.*', 'Category.name'),
            'conditions' => array('Coupon.isDeleted' => 0, 'Coupon.status' => 1,"Coupon.expire >= "=>$now, "OR"=>array(
                "Coupon.title LIKE '%$search%'", "Retailer.name LIKE '%$search%'", "Category.name LIKE '%$search%'"
            )),
            'joins' => $joinCoupon,
            'limit' => '20',
            'group' => 'Coupon.id',
            'order' => array('Coupon.id' => 'desc'),
                    
        ));
        
        $this->response($coupons);
        
    }
    
    
    public function response($data = null){
        
        
    }
    
}
